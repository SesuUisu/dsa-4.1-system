import { existsSync } from 'node:fs'
import { mkdir, open, writeFile } from 'node:fs/promises'
import yaml from 'js-yaml'

const fileName = process.argv[2]

const createFileName = (name, id) => {
  const escapedName = name.replaceAll(/[ÄÖÜäöüß()\s\/]/g, '_')
  return `${escapedName}_${id}.yml`
}

if (!fileName) {
  console.error(
    "no fileName specified, please call 'npm run pack-convert -- <fileName>'"
  )
  process.exit(1)
}

;(async () => {
  const filePath = `./src/packs/${fileName}.db`
  const file = await open(filePath)
  const dirName = `./src/packs/${fileName}`

  if (!existsSync(dirName)) {
    await mkdir(dirName)
  }

  for await (const line of file.readLines()) {
    const lineContent = JSON.parse(line)
    const json = {}
    const keys = ['_id', 'effects', 'flags', 'img', 'name', 'type']

    keys.forEach((key) => (json[key] = lineContent[key]))
    json.permission = lineContent.ownership
    json._key = `!items!${lineContent._id}`

    json.data = lineContent.system

    const doc = yaml.dump(json)

    const yamlFileName = createFileName(lineContent.name, lineContent._id)

    await writeFile(`./src/packs/${fileName}/${yamlFileName}`, doc)

    console.log(yamlFileName)
  }
})()
