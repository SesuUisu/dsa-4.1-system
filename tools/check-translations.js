import { readFile } from 'node:fs/promises'

function diff(a, b) {
  return a.filter((value) => !b.includes(value))
}

const languages = ['de', 'en']
const referenceLanguage = 'de'

const keysPerLang = {}

for (const lang of languages) {
  const json = await readFile(`./src/lang/${lang}.json`, 'utf8')
  const data = JSON.parse(json)
  const keys = Object.keys(data)
  keysPerLang[lang] = keys
}

const referenceKeys = keysPerLang[referenceLanguage]

for (const lang of languages) {
  if (lang === referenceLanguage) continue
  const keys = keysPerLang[lang]
  const missingKeys = diff(referenceKeys, keys)
  if (missingKeys.length > 0) {
    console.error(`Missing keys in ${lang}:\n${missingKeys.join('\n')}`)
    process.exit(1)
  }
}
