import { when } from 'jest-when'
import type { RuleDescriptor } from '../../src/module/ruleset/rule.js'
import { DescribeRule } from '../../src/module/ruleset/rule.js'
import { CreateRule } from '../../src/module/ruleset/rule.js'
import { Rule } from '../../src/module/ruleset/rule.js'
import type { RuleConfigManager } from '../../src/module/ruleset/rule.js'
import type { Ruleset } from '../../src/module/ruleset/ruleset.js'
import { createMockRuleset } from './rules/helpers.js'
import { TestAction } from './test-classes.js'
import { CreateActionIdentifier } from '../../src/module/ruleset/rule-components.js'

describe('Rule', function () {
  const ruleId = 'testRule'

  it('should be disabled by default', function () {
    const rule = new Rule(ruleId)
    expect(rule.isEnabled).toEqual(false)
  })

  test.each([true, false])(
    'can be enabled on load',
    function (isEnabled: boolean) {
      const rule = new Rule(ruleId, {
        enabled: isEnabled,
      })
      expect(rule.isEnabled).toEqual(isEnabled)
    }
  )

  test.each([true, false])(
    'should be describable and loadable into a ruleset if enabled',
    function (isEnabled: boolean) {
      const rule = new Rule(ruleId, {
        enabled: isEnabled,
      })
      const ruleset = createMockRuleset()

      rule.describe((ruleset: Ruleset) => {
        ruleset.registerAction(new TestAction(CreateActionIdentifier('test')))
      })

      rule.loadInto(ruleset)

      if (isEnabled) {
        expect(ruleset.registerAction).toHaveBeenCalled()
      } else {
        expect(ruleset.registerAction).toHaveBeenCalledTimes(0)
      }
    }
  )

  test.each([
    [true, true],
    [true, false],
    [false, true],
    [false, false],
  ])(
    'should be able to create a config in the config manager',
    function (isChangeable: boolean, isEnabled: boolean) {
      const systemId = 'dsa-41'
      const i18nID = 'DSA'
      const rule = new Rule(ruleId, {
        changeable: isChangeable,
        enabled: isEnabled,
      })
      const configManager = {
        register: vi.fn(),
      } as unknown as vi.Mocked<RuleConfigManager>
      rule.createConfig(configManager)

      expect(configManager.register).toHaveBeenCalledWith(
        systemId,
        ruleId,
        expect.objectContaining({
          name: `${i18nID}.settings.${ruleId}`,
          hint: `${i18nID}.settings.${ruleId}Hint`,
          scope: 'world',
          config: isChangeable,
          type: Boolean,
          default: isEnabled,
        })
      )
    }
  )

  test.each([true, false])(
    'should be able to load its config from the config manager',
    function (isEnabled) {
      const systemId = 'dsa-41'
      const rule = new Rule(ruleId)

      const configManager = {
        get: vi.fn(),
      } as unknown as vi.Mocked<RuleConfigManager>

      when(configManager.get)
        .calledWith(systemId, ruleId)
        .mockReturnValue(isEnabled)
      rule.loadConfig(configManager)
      expect(rule.isEnabled).toEqual(isEnabled)
    }
  )
})

describe('Rule Creator', function () {
  const ruleId = 'testRule'
  const systemId = 'dsa-41'
  const i18nID = 'DSA'

  it('should create a rule based on description', function () {
    const isChangeable = false
    const isEnabled = true
    const loaderCallback = vi.fn()
    const ruleDescription: RuleDescriptor = {
      ruleId,
      config: {
        enabled: isEnabled,
        changeable: isChangeable,
      },
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      loader: (_ruleset: Ruleset) => {
        loaderCallback()
      },
    }
    const rule: Rule = CreateRule(ruleDescription)

    const configManager = {
      register: vi.fn(),
    } as unknown as vi.Mocked<RuleConfigManager>

    rule.createConfig(configManager)
    rule.loadInto({} as Ruleset)

    expect(configManager.register).toHaveBeenCalledWith(
      systemId,
      ruleId,
      expect.objectContaining({
        name: `${i18nID}.settings.${ruleId}`,
        hint: `${i18nID}.settings.${ruleId}Hint`,
        scope: 'world',
        config: isChangeable,
        type: Boolean,
        default: isEnabled,
      })
    )

    expect(loaderCallback).toHaveBeenCalled()
  })
})

describe('Rule Describer', function () {
  const ruleId = 'testRule'

  it('should create a rule description', function () {
    const ruleConfig = {
      enabled: true,
      changeable: false,
    }
    const loaderCallback = vi.fn()
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const ruleLoader = (_ruleset: Ruleset) => {
      loaderCallback()
    }
    const ruleDescription = DescribeRule(ruleId, ruleConfig, ruleLoader)

    expect(ruleDescription.ruleId).toEqual(ruleId)
    expect(ruleDescription.config).toEqual(ruleConfig)
    ruleDescription.loader({} as Ruleset)
    expect(loaderCallback).toHaveBeenCalled()
  })
})
