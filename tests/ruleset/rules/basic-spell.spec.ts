import type { BaseCharacter } from '../../../src/module/model/character.js'

import { createTestRuleset } from './helpers.js'
import {
  BasicSpellRule,
  ComputeSpell,
  ComputeSpellOptions,
} from '../../../src/module/ruleset/rules/basic-spell.js'
import { SkillDescriptor } from '../../../src/module/model/properties.js'

describe('BasicSpellRule', function () {
  const character = {} as BaseCharacter

  const ruleset = createTestRuleset()

  ruleset.add(BasicSpellRule)
  ruleset.compileRules()

  const skill: SkillDescriptor = {
    name: 'Ignifaxius',
    identifier: 'spell-ignifaxius',
    skillType: 'spell',
  }

  beforeEach(() => {
    character.spell = vi.fn().mockReturnValue({
      modifications: ['duration', 'range'],
      targetClasses: ['test'],
      castTime: {
        duration: 1,
        unit: 'action',
      },
      range: '3 Schritt',
      effectTime: '1 Minute',
      magicResistance: 0,
      astralCost: '1 AsP',
      requiresUphold: false,
    })
  })

  test.each([
    [
      ['duration', 'range'],
      ['test'],
      [
        'changeTechnique',
        'changeCentralTechnique',
        'increaseRange',
        'reduceRange',
        'doubleDuration',
        'halfDuration',
        'changeToFixedDuration',
      ],
    ],
    [
      ['multipleTargets'],
      ['person'],
      [
        'changeTechnique',
        'changeCentralTechnique',
        'multipleEnemies',
        'voluntarily',
      ],
    ],
    [
      ['multipleTargets'],
      ['creature'],
      [
        'changeTechnique',
        'changeCentralTechnique',
        'multipleEnemies',
        'voluntarily',
      ],
    ],
    [
      ['multipleTargets'],
      ['person', 'voluntarily'],
      [
        'changeTechnique',
        'changeCentralTechnique',
        'multipleCompanions',
        'unvoluntarily',
      ],
    ],
    [
      ['multipleTargets'],
      ['creature', 'voluntarily'],
      [
        'changeTechnique',
        'changeCentralTechnique',
        'multipleCompanions',
        'unvoluntarily',
      ],
    ],
  ])(
    'if the spell has modifications %p and target classes %p, the expected spomods are %p',
    async function (modifications, targetClasses, expectedSpomods) {
      character.spell = vi.fn().mockReturnValue({
        modifications,
        targetClasses,
      })

      const result = await ruleset.compute(ComputeSpellOptions, {
        character,
        skill,
      })

      expect(result).toEqual({
        options: {
          character,
          skill,
        },
        spellVariants: [],
        spontaneousModifications: expectedSpomods,
      })
    }
  )

  it('should have no modifiers in computed spell without options and only the by the spell provided data', async function () {
    const result = await ruleset.compute(ComputeSpell, {
      character,
      skill,
    })

    expect(result).toEqual(
      expect.objectContaining({
        modifiers: new Map(),
        targetClasses: ['test'],
        castTime: {
          duration: 1,
          unit: 'action',
        },
        range: {
          value: '3 Schritt',
          stepModifier: 0,
        },
        effectTime: {
          value: '1 Minute',
          multiplier: 1,
        },
        magicResistance: 0,
        astralCost: '1 AsP',
        requiresUphold: false,
      })
    )
  })

  it('should translate spomods into modifers', async function () {
    const spontaneousModifications = new Map()
    spontaneousModifications.set('changeTechnique', {
      name: 'changeTechnique',
      count: 1,
    })
    spontaneousModifications.set('doubleDuration', {
      name: 'doubleDuration',
      count: 1,
    })

    const result = await ruleset.compute(ComputeSpell, {
      character,
      skill,
      spontaneousModifications,
    })

    const expectedModifiers = new Map()
    expectedModifiers.set('changeTechnique', {
      name: 'changeTechnique',
      multiplier: 1,
      class: 'spontaneousModification',
      modifierType: 'other',
      mod: 7,
    })
    expectedModifiers.set('doubleDuration', {
      name: 'doubleDuration',
      multiplier: 1,
      class: 'spontaneousModification',
      modifierType: 'other',
      mod: 7,
    })

    expect(result).toEqual(
      expect.objectContaining({
        modifiers: expectedModifiers,
      })
    )
  })

  it.each([['person'], ['creature']])(
    'should translate target spomods to target classes',
    async function (targetClass) {
      character.spell = vi.fn().mockReturnValue({
        modifications: ['target'],
        targetClasses: [targetClass],
      })

      const spontaneousModifications = new Map()
      spontaneousModifications.set('voluntarily', {
        name: 'voluntarily',
        count: 1,
      })

      const result = await ruleset.compute(ComputeSpell, {
        character,
        skill,
        spontaneousModifications,
      })

      const expectedModifiers = new Map()
      expectedModifiers.set('voluntarily', {
        name: 'voluntarily',
        multiplier: 1,
        class: 'spontaneousModification',
        modifierType: 'other',
        mod: 7,
      })

      expect(result).toEqual(
        expect.objectContaining({
          modifiers: expectedModifiers,
          targetClasses: [targetClass, 'voluntarily'],
        })
      )
    }
  )

  it.each([['person'], ['creature']])(
    'should translate target spomods to target classes',
    async function (targetClass) {
      character.spell = vi.fn().mockReturnValue({
        modifications: ['target'],
        targetClasses: [targetClass, 'voluntarily'],
      })

      const spontaneousModifications = new Map()
      spontaneousModifications.set('unvoluntarily', {
        name: 'unvoluntarily',
        count: 1,
      })

      const result = await ruleset.compute(ComputeSpell, {
        character,
        skill,
        spontaneousModifications,
      })

      const expectedModifiers = new Map()
      expectedModifiers.set('unvoluntarily', {
        name: 'unvoluntarily',
        multiplier: 1,
        class: 'spontaneousModification',
        modifierType: 'other',
        mod: 7,
      })

      expect(result).toEqual(
        expect.objectContaining({
          modifiers: expectedModifiers,
          targetClasses: [targetClass],
        })
      )
    }
  )

  it.each([
    ['person', 'multiplePersons'],
    ['creature', 'multipleCreatures'],
  ])(
    'should translate target spomods to target classes',
    async function (targetClass, expectedTargetClass) {
      character.spell = vi.fn().mockReturnValue({
        modifications: ['target'],
        targetClasses: [targetClass, 'voluntarily'],
      })

      const spontaneousModifications = new Map()
      spontaneousModifications.set('multipleCompanions', {
        name: 'multipleCompanions',
        count: 1,
      })

      const result = await ruleset.compute(ComputeSpell, {
        character,
        skill,
        spontaneousModifications,
      })

      const expectedModifiers = new Map()
      expectedModifiers.set('multipleCompanions', {
        name: 'multipleCompanions',
        multiplier: 1,
        class: 'spontaneousModification',
        modifierType: 'other',
        mod: 7,
      })

      expect(result).toEqual(
        expect.objectContaining({
          modifiers: expectedModifiers,
          targetClasses: expect.arrayContaining([
            expectedTargetClass,
            'voluntarily',
          ]),
        })
      )
    }
  )

  it.each([
    ['person', 'multiplePersons'],
    ['creature', 'multipleCreatures'],
  ])(
    'should translate target spomods to target classes',
    async function (targetClass, expectedTargetClass) {
      character.spell = vi.fn().mockReturnValue({
        modifications: ['target'],
        targetClasses: [targetClass],
      })

      const spontaneousModifications = new Map()
      spontaneousModifications.set('multipleEnemies', {
        name: 'multipleEnemies',
        count: 1,
      })

      const result = await ruleset.compute(ComputeSpell, {
        character,
        skill,
        spontaneousModifications,
      })

      const expectedModifiers = new Map()
      expectedModifiers.set('multipleEnemies', {
        name: 'multipleEnemies',
        multiplier: 1,
        class: 'spontaneousModification',
        modifierType: 'other',
        mod: 7,
      })

      expect(result).toEqual(
        expect.objectContaining({
          modifiers: expectedModifiers,
          targetClasses: expect.arrayContaining([expectedTargetClass]),
        })
      )
    }
  )

  it.each([
    {
      spoMod: 'halfCastTime',
      multiplier: 1,
      expectedDuration: 3,
      expectedAdditionalDuration: 0,
    },
    {
      spoMod: 'halfCastTime',
      multiplier: 2,
      expectedDuration: 2,
      expectedAdditionalDuration: 0,
    },
    {
      spoMod: 'halfCastTime',
      multiplier: 3,
      expectedDuration: 1,
      expectedAdditionalDuration: 0,
    },
    {
      spoMod: 'halfCastTime',
      multiplier: 4,
      expectedDuration: 1,
      expectedAdditionalDuration: 0,
    },
    {
      spoMod: 'doubleCastTime',
      multiplier: 1,
      expectedDuration: 10,
      expectedAdditionalDuration: 0,
    },
    {
      spoMod: 'doubleCastTime',
      multiplier: 2,
      expectedDuration: 20,
      expectedAdditionalDuration: 0,
    },
    {
      spoMod: 'changeTechnique',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 3,
    },
    {
      spoMod: 'changeCentralTechnique',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 3,
    },
    {
      spoMod: 'forceEffect',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedAstralCost: '1 AsP + 1 AsP',
    },
    {
      spoMod: 'reduceCost',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedAstralCost: '1 AsP - 10% AsP',
    },
    {
      spoMod: 'unvoluntarily',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedMagicResistance: 2,
    },
    {
      spoMod: 'voluntarily',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedMagicResistance: 0.5,
    },
    {
      spoMod: 'multipleCompanions',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
    },
    {
      spoMod: 'multipleEnemies',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedMagicResistance: 'max',
    },
    {
      spoMod: 'increaseRange',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedRangeStepModifier: 1,
    },
    {
      spoMod: 'reduceRange',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedRangeStepModifier: -1,
    },
    {
      spoMod: 'doubleDuration',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedEffectTimeModifier: 2,
    },
    {
      spoMod: 'doubleDuration',
      multiplier: 2,
      expectedDuration: 5,
      expectedAdditionalDuration: 2,
      expectedEffectTimeModifier: 4,
    },
    {
      spoMod: 'halfDuration',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
      expectedEffectTimeModifier: 0.5,
    },
    {
      spoMod: 'halfDuration',
      multiplier: 2,
      expectedDuration: 5,
      expectedAdditionalDuration: 2,
      expectedEffectTimeModifier: 0.25,
    },
    {
      spoMod: 'changeToFixedDuration',
      multiplier: 1,
      expectedDuration: 5,
      expectedAdditionalDuration: 1,
    },
  ])(
    'should apply spomods to cast time, astral cost, range, ...',
    async function ({
      spoMod,
      multiplier,
      expectedDuration,
      expectedAdditionalDuration,
      expectedAstralCost,
      expectedRangeStepModifier,
      expectedEffectTimeModifier,
      expectedMagicResistance,
    }) {
      if (expectedAstralCost === undefined) {
        expectedAstralCost = '1 AsP'
      }
      if (expectedRangeStepModifier === undefined) {
        expectedRangeStepModifier = 0
      }
      if (expectedEffectTimeModifier === undefined) {
        expectedEffectTimeModifier = 1
      }
      if (expectedMagicResistance === undefined) {
        expectedMagicResistance = 1
      }
      character.spell = vi.fn().mockReturnValue({
        modifications: ['target'],
        targetClasses: ['person'],
        castTime: {
          duration: 5,
          unit: 'action',
        },
        astralCost: '1 AsP',
        range: '3 Schritt',
        effectTime: '1 Minute',
        magicResistance: 1,
      })

      const spontaneousModifications = new Map()
      spontaneousModifications.set(spoMod, {
        name: spoMod,
        count: multiplier,
      })

      const result = await ruleset.compute(ComputeSpell, {
        character,
        skill,
        spontaneousModifications,
      })

      const expectedModifiers = new Map()
      expectedModifiers.set(spoMod, {
        name: spoMod,
        multiplier,
        class: 'spontaneousModification',
        modifierType: 'other',
        mod: 7,
      })

      expect(result).toEqual(
        expect.objectContaining({
          castTime: {
            duration: expectedDuration,
            unit: 'action',
            additionalCastTime: {
              duration: expectedAdditionalDuration,
              unit: 'action',
              info: '',
            },
          },
          astralCost: expectedAstralCost,
          range: {
            value: '3 Schritt',
            stepModifier: expectedRangeStepModifier,
          },
          effectTime: {
            value: '1 Minute',
            multiplier: expectedEffectTimeModifier,
          },
          magicResistance: expectedMagicResistance,
        })
      )
    }
  )

  it('should change astral cost based on spell variant and add as modifier', async function () {
    const spellVariants = new Map([
      [
        'test',
        {
          astralCost: '1 AsP + 1 AsP',
          name: 'test',
          modificator: 1,
          description: 'test',
        },
      ],
    ])
    const result = await ruleset.compute(ComputeSpell, {
      character,
      skill,
      spellVariants,
    })

    expect(result).toEqual(
      expect.objectContaining({
        astralCost: '1 AsP + 1 AsP',
      })
    )
    expect(result.modifiers.get('test')).toEqual({
      name: 'test',
      class: 'variant',
      modifierType: 'other',
      mod: 1,
      nameIsLocalized: true,
    })
  })

  it('should determine possible spell variants', async function () {
    character.spell = vi.fn().mockReturnValue({
      modifications: ['target'],
      value: 1,
      spellVariants: [
        { name: 'test', modificator: 1, description: 'test', minimalValue: 1 },
        {
          name: 'test2',
          modificator: 1,
          description: 'test2',
          minimalValue: 2,
        },
        { name: 'test3', modificator: 1, description: 'test3' },
      ],
    })

    const result = await ruleset.compute(ComputeSpellOptions, {
      character,
      skill,
    })

    expect(result.spellVariants.map((variant) => variant.name)).toEqual([
      'test',
      'test3',
    ])
  })
})
