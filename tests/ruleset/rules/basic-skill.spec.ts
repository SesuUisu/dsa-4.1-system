import { when } from 'jest-when'
import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import {
  TestAction,
  TestEffect,
  TestEffectListenedResult,
} from '../test-classes.js'
import {
  RollSkill,
  RollSkillToChatEffect,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import {
  BasicSkillRule,
  SourceAction,
  FormulaAction,
  TalentAction,
} from '../../../src/module/ruleset/rules/basic-skill.js'
import type {
  NamedAttribute,
  Rollable,
  SkillDescriptor,
  Spell,
  Talent,
  Source,
  TestAttributes,
  Formula,
} from '../../../src/module/model/properties.js'
import { SpellAction } from '../../../src/module/ruleset/rules/basic-skill.js'
import { createTestRuleset } from './helpers.js'
import { AttributeName } from '../../../src/module/model/character-data.js'

describe('BasicSkillRule', function () {
  const ruleset = createTestRuleset()

  const executeHook = vi.fn().mockReturnValue({})
  const skillRollAction = new TestAction(RollSkill, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.registerEffect<TestEffectListenedResult, TestEffect>(
    new TestEffect(RollSkillToChatEffect)
  )

  ruleset.add(BasicSkillRule)
  ruleset.compileRules()

  const talentDescriptor: SkillDescriptor = {
    name: 'Swimming',
    identifier: 'talent-swimming',
    skillType: 'talent',
  }
  const spellDescriptor: SkillDescriptor = {
    name: 'Ignifaxius',
    identifier: 'spell-ignifaxius',
    skillType: 'spell',
  }
  const sourceDescriptor: SkillDescriptor = {
    name: 'Carafei',
    identifier: 'source-carafei',
    skillType: 'source',
  }
  const formulaDescriptor: SkillDescriptor = {
    name: 'Attributo',
    identifier: 'formula-attributo',
    skillType: 'formula',
  }

  const attributeRollMock = vi.fn()
  const courage = {
    name: 'courage',
    value: 10,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const cleverness = {
    name: 'cleverness',
    value: 11,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const agility = {
    name: 'agility',
    value: 12,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData,
  ] = [
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: cleverness.name,
      value: cleverness.value,
    },
    {
      name: agility.name,
      value: agility.value,
    },
  ]
  const skillValue = 8
  const testAttributes: TestAttributes = testAttributeData.map(
    (attributeData) => attributeData.name as AttributeName
  ) as TestAttributes

  const character = {
    data: {} as CharacterDataAccessor,
  } as BaseCharacter

  const talent = {} as Talent
  const spell = {} as Spell
  const source = {} as Source
  const formula = {} as Formula

  talent.value = skillValue
  talent.testAttributes = testAttributes
  spell.value = skillValue
  spell.testAttributes = testAttributes
  source.value = skillValue
  source.testAttributes = testAttributes
  formula.value = skillValue
  formula.testAttributes = testAttributes
  character.talent = vi.fn()
  when(character.talent)
    .calledWith(talentDescriptor.identifier)
    .mockReturnValue(talent)
  character.spell = vi.fn()
  when(character.spell)
    .calledWith(spellDescriptor.identifier)
    .mockReturnValue(spell)
  character.source = vi.fn()
  when(character.source)
    .calledWith(sourceDescriptor.identifier)
    .mockReturnValue(source)
  character.formula = vi.fn()
  when(character.formula)
    .calledWith(formulaDescriptor.identifier)
    .mockReturnValue(formula)
  character.attribute = vi.fn()
  when(character.attribute).calledWith(courage.name).mockReturnValue(courage)
  when(character.attribute)
    .calledWith(cleverness.name)
    .mockReturnValue(cleverness)
  when(character.attribute).calledWith(agility.name).mockReturnValue(agility)

  it('should provide basic talent roll action', async function () {
    const mod = 5
    await ruleset.execute(TalentAction, {
      character,
      mod,
      skill: talentDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: talentDescriptor.name,
        skillType: talentDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })

  it('should provide basic spell roll action', async function () {
    const mod = 5
    await ruleset.execute(SpellAction, {
      character,
      mod,
      skill: spellDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: spellDescriptor.name,
        skillType: spellDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })

  it('should provide basic source roll action', async function () {
    const mod = 5
    await ruleset.execute(SourceAction, {
      character,
      mod,
      skill: sourceDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: sourceDescriptor.name,
        skillType: sourceDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })

  it('should provide basic formula roll action', async function () {
    const mod = 5
    await ruleset.execute(FormulaAction, {
      character,
      mod,
      skill: formulaDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: formulaDescriptor.name,
        skillType: formulaDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })
})
