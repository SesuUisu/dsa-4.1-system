type FakeActorData = {
  attack?: number
  rangedAttack?: number
  parry?: number
  dodge?: number
  talents?: any
}

export class FakeActor {
  _attack: number
  _rangedAttack: number
  _parry: number
  _dodge: number
  _talents: any

  constructor(options: FakeActorData = {}) {
    this._attack = options.attack || 0
    this._rangedAttack = options.rangedAttack || 0
    this._parry = options.parry || 0
    this._dodge = options.dodge || 0
    this._talents = options.talents
  }

  attack() {
    return this._attack
  }

  rangedAttack() {
    return this._rangedAttack
  }

  parry() {
    return this._parry
  }

  dodge() {
    return this._dodge
  }

  talent(talent) {
    return this._talents[talent]
  }
}

type FakeWeaponData = {
  talent?: any
}

export class FakeWeapon {
  _talent: any

  constructor(options: FakeWeaponData = {}) {
    this._talent = options.talent
  }

  get talent() {
    return this._talent
  }
}
