import type {
  BaseCharacter,
  CharacterDataAccessor,
} from '../../../src/module/model/character.js'
import { createTestRuleset } from './helpers.js'
import {
  BasicRestingComputationRule,
  ComputeAstralRestingModifier,
  ComputeRestingParameters,
  ComputeVitalityRestingModifier,
} from '../../../src/module/ruleset/rules/basic-resting-computation.js'

describe('BasicRestringComputation', function () {
  let character: BaseCharacter

  beforeEach(() => {
    character = {
      data: {
        system: {
          base: {
            resources: {
              astralEnergy: {
                value: 1,
                max: 10,
              },
              vitality: {
                value: 1,
                max: 10,
              },
            },
          },
          settings: {
            hasAstralEnergy: true,
          }
        },
      } as CharacterDataAccessor,
    } as BaseCharacter
    character.vitalityRestingModifier = vi.fn().mockImplementation((x) => x)
    character.astralEnergyRestingModifier = vi.fn().mockImplementation((x) => x)
  })

  const ruleset = createTestRuleset()

  ruleset.add(BasicRestingComputationRule)
  ruleset.compileRules()

  test.each([
    {
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
      },
      expect: { vitality: 3, astralEnergy: 4 },
    },
    {
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: -2,
      },
      expect: { vitality: 1, astralEnergy: 2 },
    },
    {
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
        restInterrupted: true,
      },
      expect: { vitality: 2, astralEnergy: 3 },
    },
    {
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
        keptWatch: true,
      },
      expect: { vitality: 2, astralEnergy: 3 },
    },
    {
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
        badEncampment: true,
      },
      expect: { vitality: 2, astralEnergy: 3 },
    },
    {
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
        restInterrupted: true,
        keptWatch: true,
        badEncampment: true,
      },
      expect: { vitality: 0, astralEnergy: 1 },
    },
  ])('should provide computation for resting', function (testParameters) {
    const result = ruleset.compute(ComputeRestingParameters, {
      character,
      options: testParameters.options,
    })

    expect(result).toEqual({
      vitality: {
        roll: true,
        bonusAttribute: 'constitution',
        modifier: testParameters.expect.vitality,
        bonusModifier: 0,
      },
      astralEnergy: {
        roll: true,
        bonusAttribute: 'intuition',
        modifier: testParameters.expect.astralEnergy,
        bonusModifier: 0,
      },
    })
  })

  it('should provide computation for resting if vitality is full', function () {
    character.data.system.base.resources.vitality.value = 10

    const result = ruleset.compute(ComputeRestingParameters, {
      character,
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
      },
    })

    expect(result).toEqual({
      astralEnergy: {
        roll: true,
        bonusAttribute: 'intuition',
        modifier: 4,
        bonusModifier: 0,
      },
    })
  })
    
  it('should provide computation for resting if astralEnergy is full', function () {
    character.data.system.base.resources.astralEnergy.value = 20

    const result = ruleset.compute(ComputeRestingParameters, {
      character,
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
      },
    })

    expect(result).toEqual({
      vitality: {
        roll: true,
        bonusAttribute: 'constitution',
        modifier: 3,
        bonusModifier: 0,
      },
    })
  })
    
  it('should provide computation for resting if the character has no astralEnergy', function () {
    character.data.system.settings.hasAstralEnergy = false

    const result = ruleset.compute(ComputeRestingParameters, {
      character,
      options: {
        vitalityMod: 1,
        astralEnergyMod: 2,
        comfort: 2,
        weather: 0,
      },
    })

    expect(result).toEqual({
      vitality: {
        roll: true,
        bonusAttribute: 'constitution',
        modifier: 3,
        bonusModifier: 0,
      },
    })
  })

  it('should provide computation for the vitality resting modifier', function () {
    const modifiers = {
      roll: true,
      bonusAttribute: 'constitution',
      modifier: 1,
      bonusModifier: -2,
    }
    const result = ruleset.compute(ComputeVitalityRestingModifier, {
      character,
      modifiers,
    })

    expect(result).toEqual(modifiers)
  })

  it('should provide computation for the astral energy resting modifier', function () {
    const modifiers = {
      roll: false,
      bonusAttribute: 'intuition',
      modifier: 7,
      bonusModifier: -2,
    }

    const result = ruleset.compute(ComputeAstralRestingModifier, {
      character,
      modifiers,
    })

    expect(result).toEqual(modifiers)
  })

  it('should provide computation for the astral energy resting modifier', function () {
    const modifiers = {
      roll: false,
      bonusAttribute: 'intuition',
      modifier: 7,
      bonusModifier: -2,
    }

    const result = ruleset.compute(ComputeAstralRestingModifier, {
      character,
      modifiers,
    })

    expect(result).toEqual(modifiers)
  })
})
