import type { RuleConfigManager } from '../../../src/module/ruleset/rule.js'
import { Ruleset } from '../../../src/module/ruleset/ruleset.js'

export function createTestRuleset(): Ruleset {
  const configManager: vi.Mocked<RuleConfigManager> = {
    register: vi.fn(),
    get: vi.fn(),
  }
  configManager.get.mockReturnValue(true)
  const ruleset: Ruleset = new Ruleset(configManager)
  return ruleset
}

export function createMockRuleset(): vi.Mocked<Ruleset> {
  return {
    execute: vi.fn(),
    compute: vi.fn(),
    registerAction: vi.fn(),
  } as unknown as vi.Mocked<Ruleset>
}
