import { Computation } from '../../../src/module/ruleset/rule-components.js'
import { ComputeRangedAttack } from '../../../src/module/ruleset/rules/derived-combat-attributes.js'
import {
  BasicRangedCombatRule,
  RangedAttackAction,
} from '../../../src/module/ruleset/rules/basic-ranged-combat.js'
import type { RangedCombatActionData } from '../../../src/module/ruleset/rules/basic-ranged-combat.js'
import { when } from 'jest-when'
import type { BaseCharacter } from '../../../src/module/model/character.js'
import type {
  RangeClass,
  SizeClass,
  Weapon,
} from '../../../src/module/model/items.js'
import {
  RollAttributeToChatEffect,
  RollCombatAttribute,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import { createTestRuleset } from './helpers.js'
import { EffectSpy, TestAction } from '../test-classes.js'

describe('BasicRangedCombatRule', function () {
  const character = {} as BaseCharacter

  const ruleset = createTestRuleset()

  const identifier = RangedAttackAction
  const computeHook = vi.fn().mockReturnValue({})
  ruleset.registerComputation(new Computation(ComputeRangedAttack, computeHook))
  ruleset.add(BasicRangedCombatRule)
  ruleset.compileRules()
  const rangedAttackValue = 12
  const executeHook = vi.fn().mockReturnValue({})
  const rollAction = new TestAction(RollCombatAttribute, executeHook)
  ruleset.registerAction(rollAction)
  const effectSpy = new EffectSpy(RollAttributeToChatEffect)
  ruleset.registerEffect(effectSpy)

  const weapon = {} as Weapon

  character.rangedAttackValue = vi.fn()
  when(character.rangedAttackValue)
    .calledWith(expect.objectContaining({ weapon }))
    .mockReturnValue(rangedAttackValue)

  let options: RangedCombatActionData

  beforeEach(function () {
    executeHook.mockClear()
    options = {
      character,
      weapon,
      rangeClass: 'close',
      sizeClass: 'big',
    }
  })

  it('should provide basic ranged attack actions', async function () {
    const result = await ruleset.execute(identifier, options)

    expect(result.options.action).toEqual(identifier)
    expect(executeHook).toHaveBeenCalledWith(
      expect.objectContaining({
        targetValue: rangedAttackValue,
        mod: 0,
      })
    )
  })

  it('should generate a chat message for the associated roll', async function () {
    executeHook.mockReturnValueOnce({
      success: true,
    })

    const weaponDamage = '1d6 + 3'
    const weapon = {} as Weapon
    character.rangedAttackValue = vi.fn()
    when(character.rangedAttackValue)
      .calledWith(expect.objectContaining({ weapon }))
      .mockReturnValue(rangedAttackValue)

    character.damage = vi.fn()
    when(character.damage)
      .calledWith(expect.objectContaining({ weapon }))
      .mockReturnValue(weaponDamage)
    await ruleset.execute(identifier, options)

    // expect(effectSpy.result.damage).toEqual(weaponDamage)
  })

  const rangeModMap = {
    veryClose: -2,
    close: 0,
    medium: 4,
    far: 8,
    veryFar: 12,
  }

  const ranges = Object.keys(rangeModMap)

  test.each(ranges)(
    'should provide a computation for the range modificator to the actor',
    function (range: RangeClass) {
      const result = ruleset.compute(ComputeRangedAttack, {
        ...options,
        rangeClass: range,
      })
      expect(result.modifiers?.get('rangeClass')?.mod || 0).toEqual(
        rangeModMap[range]
      )
    }
  )

  test.each(ranges)(
    'should apply the correct modificator based on range class',
    async function (range: RangeClass) {
      await ruleset.execute(RangedAttackAction, {
        ...options,
        rangeClass: range,
      })

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          targetValue: rangedAttackValue,
          mod: rangeModMap[range],
        })
      )
    }
  )

  const sizeModMap = {
    tiny: 8,
    verySmall: 6,
    small: 4,
    medium: 2,
    big: 0,
    veryBig: -2,
  }

  const sizes = Object.keys(sizeModMap).map((key) => [key])

  test.each(sizes)(
    'should provide a computation for the size modificator to the actor',
    function (size: SizeClass) {
      const result = ruleset.compute(ComputeRangedAttack, {
        ...options,
        sizeClass: size,
      })

      expect(result.modifiers?.get('sizeClass')?.mod || 0).toEqual(
        sizeModMap[size]
      )
    }
  )

  test.each(sizes)(
    'should apply the correct modificator based on target size',
    async function (size: SizeClass) {
      await ruleset.execute(RangedAttackAction, {
        ...options,
        sizeClass: size,
      })

      expect(executeHook).toHaveBeenCalledWith(
        expect.objectContaining({
          targetValue: rangedAttackValue,
          mod: sizeModMap[size],
        })
      )
    }
  )
})
