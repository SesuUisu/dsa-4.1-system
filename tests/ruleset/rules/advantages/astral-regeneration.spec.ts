import { when } from 'jest-when'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeAstralRestingModifier } from '../../../../src/module/ruleset/rules/basic-resting-computation.js'
import { vi } from 'vitest'
import {
  AstralRegeneration,
  AstralRegenerationRule,
} from '../../../../src/module/ruleset/rules/advantages/astral-regeneration.js'

let character
let ruleset

describe('Regeneration rule', function () {
  it('should calculate values if the character has no Astral Regeneration', async function () {
    const options = {
      character,
    }
    when(character.has).calledWith(AstralRegeneration).mockReturnValue(false)

    const result = await ruleset.compute(ComputeAstralRestingModifier, options)

    expect(result).toEqual({
      modifier: 0,
      bonusModifier: 0,
      roll: true,
      bonusAttribute: 'intuition',
    })
  })

  test.each([
    {
      level: 1,
      expected: 1,
      expectedBonus: -1,
    },
    {
      level: 2,
      expected: 2,
      expectedBonus: -2,
    },
    {
      level: 3,
      expected: 3,
      expectedBonus: -3,
    },
  ])(
    'should calculate values for Astral Regeneration',
    async function (testParameters) {
      const options = {
        character,
      }

      when(character.has).calledWith(AstralRegeneration).mockReturnValue(true)
      when(character.data.advantage)
        .calledWith(AstralRegeneration.identifier)
        .mockReturnValue({
          system: { value: testParameters.level },
        })

      const result = await ruleset.compute(
        ComputeAstralRestingModifier,
        options
      )

      expect(result).toEqual({
        modifier: testParameters.expected,
        bonusModifier: testParameters.expectedBonus,
        roll: true,
        bonusAttribute: 'intuition',
      })
    }
  )

  beforeEach(() => {
    character = {
      data: {},
    } as BaseCharacter
    ruleset = createTestRuleset()

    const executeHook = vi.fn().mockReturnValue({
      modifier: 0,
      bonusModifier: 0,
      roll: true,
      bonusAttribute: 'intuition',
    })
    ruleset.registerComputation(
      new Computation(ComputeAstralRestingModifier, executeHook)
    )

    ruleset.add(AstralRegenerationRule)
    ruleset.compileRules()

    character.has = vi.fn()
    character.data.advantage = vi.fn()
  })
})
