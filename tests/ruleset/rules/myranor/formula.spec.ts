import { when } from 'jest-when'
import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { TestAction, TestEffect } from '../../test-classes.js'
import type { TestEffectListenedResult } from '../../test-classes.js'
import {
  RollSkill,
  RollSkillToChatEffect,
} from '../../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import {
  BasicSkillRule,
  FormulaAction,
} from '../../../../src/module/ruleset/rules/basic-skill.js'
import type {
  SkillDescriptor,
  Source,
  TestAttributes,
  NamedAttribute,
  Rollable,
  Formula,
} from '../../../../src/module/model/properties.js'
import { createTestRuleset } from '../helpers.js'
import { AttributeName } from '../../../../src/module/model/character-data.js'
import { GenericFormula } from '../../../../src/module/character/skill.js'
import {
  DataAccessor,
  FormulaData,
} from '../../../../src/module/model/item-data.js'
import {
  BasicMyranorFormulaRule,
  ComputeMyranorFormulaModifier,
  GetAspCosts,
} from '../../../../src/module/ruleset/rules/myranor/formula.js'
import { ModifierDescriptor } from '../../../../src/module/model/modifier.js'

describe('MyranorFormulaRule', function () {
  const character = {
    data: {
      advantage: () => {},
    },
  } as unknown as BaseCharacter

  const ruleset = createTestRuleset()

  ruleset.add(BasicMyranorFormulaRule)
  ruleset.compileRules()

  const executeHook = vi.fn().mockReturnValue({})
  const skillRollAction = new TestAction(RollSkill, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.registerEffect<TestEffectListenedResult, TestEffect>(
    new TestEffect(RollSkillToChatEffect)
  )

  ruleset.add(BasicSkillRule)
  ruleset.compileRules()

  const sourceDescriptor: SkillDescriptor = {
    name: 'Carafei',
    identifier: 'source-carafei',
    skillType: 'source',
  }

  const formulaDescriptor: SkillDescriptor = {
    name: 'Todesstrahl',
    identifier: 'formula-todesstrahl',
    skillType: 'formula',
  }

  const attributeRollMock = vi.fn()
  const courage = {
    name: 'courage',
    value: 10,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const charisma = {
    name: 'charisma',
    value: 12,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const source = {} as Source
  const formula = {} as Formula

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData,
  ] = [
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: charisma.name,
      value: charisma.value,
    },
  ]
  const skillValue = 8
  const testAttributes: TestAttributes = testAttributeData.map(
    (attributeData) => attributeData.name as AttributeName
  ) as TestAttributes

  source.value = skillValue
  source.testAttributes = testAttributes

  formula.testAttributes = source.testAttributes
  formula.value = source.value

  character.source = vi.fn()
  when(character.source)
    .calledWith(sourceDescriptor.identifier)
    .mockReturnValue(source)
  character.formula = vi.fn()
  when(character.formula)
    .calledWith(formulaDescriptor.identifier)
    .mockReturnValue(formula)
  character.attribute = vi.fn()
  when(character.attribute).calledWith(courage.name).mockReturnValue(courage)
  when(character.attribute).calledWith(charisma.name).mockReturnValue(charisma)
  when(character.attribute).calledWith(charisma.name).mockReturnValue(charisma)

  it('should provide basic formula roll action', async function () {
    const mod = 5
    await ruleset.execute(FormulaAction, {
      character,
      mod,
      skill: formulaDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: formulaDescriptor.name,
        skillType: formulaDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })

  it('should provide calculate the formula modifier correct', async function () {
    const character = {
      data: {
        advantage: () => {},
      },
    } as unknown as BaseCharacter
    const formulaName = 'Attributo Mut'
    const quality = 5

    const formulaData = {
      name: formulaName,
      type: 'formula',
      system: {
        sid: 'attributo-formula',
        isUniquelyOwnable: true,
        description: '',
        quality: quality,
        sourceId: 'CarafeiId',
        instructionId: 'BesselungDesGeistesId',
        parameters: {
          castTime: 'fiveActions', // 4
          target: 'oneCreature', // -1
          range: 'sevenSteps', // 3
          duration: 'leftSpellPointsHours', // 2,
          structure: 'veryDifficult', // 3
        },
      } as unknown as FormulaData,
    } as DataAccessor<'formula'>

    const formula = new GenericFormula(formulaData, character, ruleset)

    const modifiers = ruleset.compute(ComputeMyranorFormulaModifier, {
      character,
      formula,
      modifiers: new Map<string, ModifierDescriptor>(),
    })

    expect(modifiers.size).toBe(2)
    expect(modifiers.get('formulaParameterModifier')?.mod).toBe(11)
    expect(modifiers.get('formulaQuality')?.mod).toBe(quality * -1)
  })
})

describe('GetAspCosts', function () {
  const formulaData: FormulaData = {
    sid: 'attributo-formula',
    isUniquelyOwnable: true,
    description: '',
    quality: 5,
    sourceId: 'CarafeiId',
    weight: '1kilogram',
    instructionId: 'BesselungDesGeistesId',
    parameters: {
      castTime: 'fiveActions', // 4
      target: 'oneCreature', // -1
      range: 'sevenSteps', // 3
      duration: 'leftSpellPointsHours', // 2,
      structure: 'veryDifficult', // 3
    },
  }

  it('should return undefined if there is no instruction', async function () {
    const instructions = []
    const costs = GetAspCosts(formulaData, instructions)
    expect(costs).toBe(undefined)
  })

  it('should return undefined if there is no costs associated with the structure', async function () {
    const instructions = [
      {
        id: 'BesselungDesGeistesId',
        system: {
          costs: {
            veryEasy: {
              base: 4,
            },
          },
        },
      },
    ]
    const costs = GetAspCosts(formulaData, instructions)
    expect(costs).toBe(undefined)
  })

  it('should return the base cost if there is a cost associated with the structure', async function () {
    const instructions = [
      {
        id: 'BesselungDesGeistesId',
        system: {
          costs: {
            veryDifficult: {
              base: 4,
            },
          },
        },
      },
    ]
    const costs = GetAspCosts(formulaData, instructions)
    expect(costs).toStrictEqual({
      total: 4,
      base: 4,
      additional: 0,
      weight: 0,
      duration: 0,
      remarks: undefined,
    })
  })

  it('should return the base cost if there is a generic cost', async function () {
    const instructions = [
      {
        id: 'BesselungDesGeistesId',
        system: {
          costs: {
            generic: {
              base: 2,
            },
          },
        },
      },
    ]
    const costs = GetAspCosts(formulaData, instructions)

    expect(costs).toStrictEqual({
      total: 2,
      base: 2,
      additional: 0,
      weight: 0,
      duration: 0,
      remarks: undefined,
    })
  })

  it('should return the additional cost', async function () {
    const expectedRemark = '1/4 des Astralvolumens'

    const instructions = [
      {
        id: 'BesselungDesGeistesId',
        system: {
          costs: {
            generic: {
              base: 2,
              remarks: expectedRemark,
            },
          },
        },
      },
    ]

    const costs = GetAspCosts(formulaData, instructions)

    expect(costs?.remarks).toBe(expectedRemark)
  })

  it('should return the total cost even without base costs', async function () {
    const instructions = [
      {
        id: 'BesselungDesGeistesId',
        system: {
          costs: {
            generic: {},
          },
        },
      },
    ]

    const formulaDataForTesting = Object.assign({}, formulaData, {
      additionalAsp: 12,
    })

    const costs = GetAspCosts(formulaDataForTesting, instructions)

    expect(costs).toStrictEqual({
      total: 12,
      base: 0,
      additional: 12,
      weight: 0,
      duration: 0,
      remarks: undefined,
    })
  })

  test.each([
    ['leftSpellPointsActions', 1],
    ['fiftyActions', 1],
    ['oneGameRound', 1],
    ['leftSpellPointsGameRounds', 2],
    ['leftSpellPointsHours', 4],
    ['leftSpellPointsTimes8hours', 8],
    ['oneWeek', 16],
    ['oneMonth', 32],
    ['oneYear', 64],
    ['instantaneousPermanent', 128],
  ])(
    'should calculate the duration cost accordingly',
    function (duration, expectedDurationCost) {
      const instructions = [
        {
          id: 'BesselungDesGeistesId',
          system: {
            costs: {
              generic: {
                base: 0,
                duration: 1,
                durationBase: 'oneGameRound',
              },
            },
          },
        },
      ]

      const formulaDataForTesting = Object.assign({}, formulaData)

      formulaDataForTesting.parameters = Object.assign(
        {},
        formulaData.parameters,
        { duration }
      )

      const costs = GetAspCosts(formulaDataForTesting, instructions)

      expect(costs?.duration).toBe(expectedDurationCost)
    }
  )

  test.each([
    ['40milligram', 1],
    ['1gram', 1],
    ['25gram', 1],
    ['1kilogram', 2],
    ['5kilogram', 4],
    ['25kilogram', 8],
    ['125kilogram', 16],
    ['1ton', 32],
    ['10ton', 64],
  ])(
    'should calculate the weight cost accordingly',
    function (weight, expectedWeightCost) {
      const instructions = [
        {
          id: 'BesselungDesGeistesId',
          system: {
            costs: {
              generic: {
                base: 0,
                weight: 2,
                weightBase: '1kilogram',
              },
            },
          },
        },
      ]
      const formulaDataForTesting = Object.assign({}, formulaData)
      formulaDataForTesting.weight = weight

      const costs = GetAspCosts(formulaDataForTesting, instructions)

      expect(costs?.weight).toBe(expectedWeightCost)
    }
  )

  test.each([
    {
      base: 4,
      durationCost: 2,
      additionalAsp: 3,
      expectedTotal: 15,
    },
    {
      base: 1,
      weightCost: 5,
      additionalAsp: 10,
      expectedTotal: 21,
    },
    {
      base: 1,
      weightCost: 5,
      expectedTotal: 11,
    },
    {
      base: 1,
      durationCost: 2,
      weightCost: 5,
      additionalAsp: 10,
      expectedTotal: 29,
    },
    {
      base: 1,
      additionalAsp: 7,
      expectedTotal: 8,
    },
  ])(
    'should return the total costs with different parameters set',
    function (parameters) {
      const instruction = {
        id: 'BesselungDesGeistesId',
        system: {
          costs: {
            generic: {
              base: parameters.base,
              durationBase: 'oneGameRound',
              weightBase: '25gram',
            },
          },
        },
      }

      if (parameters.durationCost) {
        instruction.system.costs.generic.duration = parameters.durationCost
      }
      if (parameters.weightCost) {
        instruction.system.costs.generic.weight = parameters.weightCost
      }

      const instructions = [instruction]
      const formulaDataForTesting = Object.assign({}, formulaData)

      if (parameters.additionalAsp) {
        formulaDataForTesting.additionalAsp = parameters.additionalAsp
      }

      const costs = GetAspCosts(formulaDataForTesting, instructions)

      expect(costs?.total).toBe(parameters.expectedTotal)
    }
  )
})
