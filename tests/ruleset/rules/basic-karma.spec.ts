import { when } from 'jest-when'
import type { BaseCharacter } from '../../../src/module/model/character.js'
import { TestAction, TestEffect } from '../test-classes.js'
import type { TestEffectListenedResult } from '../test-classes.js'
import {
  RollSkill,
  RollSkillToChatEffect,
} from '../../../src/module/ruleset/rules/basic-roll-mechanic.js'
import {
  BasicSkillRule,
  LiturgyAction,
} from '../../../src/module/ruleset/rules/basic-skill.js'
import type {
  SkillDescriptor,
  Talent,
  Liturgy,
  TestAttributes,
  NamedAttribute,
  Rollable,
} from '../../../src/module/model/properties.js'
import { createTestRuleset } from './helpers.js'
import { AttributeName } from '../../../src/module/model/character-data.js'

describe('BasicKarmaRule', function () {
  const character = {} as BaseCharacter

  const ruleset = createTestRuleset()

  const executeHook = vi.fn().mockReturnValue({})
  const skillRollAction = new TestAction(RollSkill, executeHook)
  ruleset.registerAction(skillRollAction)

  ruleset.registerEffect<TestEffectListenedResult, TestEffect>(
    new TestEffect(RollSkillToChatEffect)
  )

  ruleset.add(BasicSkillRule)
  ruleset.compileRules()

  const talentDescriptor: SkillDescriptor = {
    name: 'Liturgiekenntnis',
    identifier: 'talent-liturgiekenntnis',
    skillType: 'talent',
  }

  const liturgyDescriptor: SkillDescriptor = {
    name: 'Heilungssegen',
    identifier: 'liturgy-heilungssegen',
    skillType: 'liturgy',
  }

  const attributeRollMock = vi.fn()
  const courage = {
    name: 'courage',
    value: 10,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const intuition = {
    name: 'intuition',
    value: 11,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const charisma = {
    name: 'charisma',
    value: 12,
    roll: attributeRollMock,
  } as Rollable<NamedAttribute>

  const talent = {} as Talent
  const liturgy = {} as Liturgy

  type TestAttributeData = { name: AttributeName; value: number }
  const testAttributeData: [
    TestAttributeData,
    TestAttributeData,
    TestAttributeData
  ] = [
    {
      name: courage.name,
      value: courage.value,
    },
    {
      name: intuition.name,
      value: intuition.value,
    },
    {
      name: charisma.name,
      value: charisma.value,
    },
  ]
  const skillValue = 8
  const testAttributes: TestAttributes = testAttributeData.map(
    (attributeData) => attributeData.name as AttributeName
  ) as TestAttributes

  talent.value = skillValue
  talent.testAttributes = testAttributes

  liturgy.degree = 'I'
  liturgy.testAttributes = talent.testAttributes
  liturgy.value = talent.value

  character.karmaTalent = vi.fn()
  when(character.karmaTalent)
    .calledWith(talentDescriptor.identifier)
    .mockReturnValue(talent)
  character.liturgy = vi.fn()
  when(character.liturgy)
    .calledWith(liturgyDescriptor.identifier)
    .mockReturnValue(liturgy)
  character.attribute = vi.fn()
  when(character.attribute).calledWith(courage.name).mockReturnValue(courage)
  when(character.attribute)
    .calledWith(intuition.name)
    .mockReturnValue(intuition)
  when(character.attribute).calledWith(charisma.name).mockReturnValue(charisma)

  it('should provide basic liturgy roll action', async function () {
    const mod = 5
    await ruleset.execute(LiturgyAction, {
      character,
      mod,
      skill: liturgyDescriptor,
    })

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        skillName: liturgyDescriptor.name,
        skillType: liturgyDescriptor.skillType,
        skillValue,
        mod,
        testAttributeData,
      })
    )
  })
})
