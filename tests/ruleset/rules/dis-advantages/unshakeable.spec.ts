import { when } from 'jest-when'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeWoundThresholds } from '../../../../src/module/ruleset/rules/derived-attributes.js'
import {
  Unshakeable,
  UnshakeableRule,
} from '../../../../src/module/ruleset/rules/dis-advantages/unshakeable.js'

describe('Unshakeable', function () {
  const ruleset = createTestRuleset()

  const executeHook = vi.fn()
  ruleset.registerComputation(
    new Computation(ComputeWoundThresholds, executeHook)
  )

  ruleset.add(UnshakeableRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should add 2 to the modifier of wound thresholds if unshakeable', async function () {
    character.has = vi.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(Unshakeable).mockReturnValue(true)

    ruleset.compute(ComputeWoundThresholds, options)

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        mod: 2,
      })
    )
  })

  it('should add 2 to the modifier of wound thresholds if unshakeable with provided mod', async function () {
    character.has = vi.fn()

    const options = {
      character,
      mod: 1,
    }

    when(character.has).calledWith(Unshakeable).mockReturnValue(true)

    ruleset.compute(ComputeWoundThresholds, options)

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        mod: 3,
      })
    )
  })

  it('should not add 2 to the modifier of wound thresholds if unshakeable', async function () {
    character.has = vi.fn()

    const options = {
      character,
    }

    when(character.has).calledWith(Unshakeable).mockReturnValue(false)

    ruleset.compute(ComputeWoundThresholds, options)

    expect(executeHook).toBeCalledWith(
      expect.not.objectContaining({
        mod: 2,
      })
    )
  })

  it('should not add 2 to the modifier of wound thresholds if unshakeable with provided mod', async function () {
    character.has = vi.fn()

    const options = {
      character,
      mod: 1,
    }

    when(character.has).calledWith(Unshakeable).mockReturnValue(false)

    ruleset.compute(ComputeWoundThresholds, options)

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        mod: 1,
      })
    )
  })
})
