import { when } from 'jest-when'
import {
  BladeDancer,
  BladeDancerRule,
} from '../../../../src/module/ruleset/rules/special-abilities/blade-dancer.js'

import type { BaseCharacter } from '../../../../src/module/model/character.js'
import {} from '../../../../src/module/ruleset/rules/derived-combat-attributes.js'
import { Computation } from '../../../../src/module/ruleset/rule-components.js'
import { createTestRuleset } from '../helpers.js'
import { ComputeInitiativeFormula } from '../../../../src/module/ruleset/rules/basic-combat.js'

describe('Blade dancer', function () {
  const ruleset = createTestRuleset()

  const executeHook = vi.fn()
  ruleset.registerComputation(
    new Computation(ComputeInitiativeFormula, executeHook)
  )

  ruleset.add(BladeDancerRule)
  ruleset.compileRules()

  const character = {} as BaseCharacter

  it('should add an additional dice to the initiative formula', async function () {
    character.has = vi.fn()

    const expectedDices = 2

    const options = {
      character,
    }

    when(character.has).calledWith(BladeDancer).mockReturnValue(true)

    ruleset.compute(ComputeInitiativeFormula, options)

    expect(executeHook).toBeCalledWith(
      expect.objectContaining({
        dices: expectedDices,
      })
    )
  })
})
