import {
  generateModificationFromBoolean,
  generateModificationFromMultiplier,
  generateModificationFromTable,
  generateModificationMapFromTable,
} from '../../../src/module/ruleset/rules/modification-generator.js'

describe('generateModificationFromBoolean', function () {
  test.each([true, false, undefined])(
    'should generate a modification from a boolean',
    function (optionIsActive: boolean | undefined) {
      const modification = generateModificationFromBoolean('test', 3)
      const modifiers = new Map()
      const mod = 5
      const result = modification(
        { test: optionIsActive, modifiers, mod },
        { modifiers, mod }
      )

      let expectedModifier
      if (optionIsActive)
        expectedModifier = {
          name: 'test',
          mod: 3,
          class: 'test',
          modifierType: 'other',
          multiplier: 1,
        }
      expect(result.modifiers.get('test')).toEqual(expectedModifier)
      expect(result.modifiers.size).toBe(optionIsActive ? 1 : 0)
    }
  )
})

describe('generateModificationFromMultiplier', function () {
  test.each([0, 1, 2, 3])(
    'should generate a modification from a multiplier',
    function (multiplier: number) {
      const modification = generateModificationFromMultiplier('test', 3)
      const modifiers = new Map()
      const mod = 5
      const result = modification(
        { test: multiplier, modifiers, mod },
        { modifiers, mod }
      )

      const expectedModifier = {
        name: 'test',
        mod: 3 * multiplier,
        class: 'test',
        modifierType: 'other',
        multiplier: 1,
      }
      expect(result.modifiers.get('test')).toEqual(expectedModifier)
      expect(result.modifiers.size).toBe(1)
    }
  )

  test.each([1, 2, 3])(
    'should generate a modification from a multiplier with a min cap',
    function (multiplier: number) {
      const modification = generateModificationFromMultiplier('test', 3, 5)
      const modifiers = new Map()
      const mod = 5
      const result = modification(
        { test: multiplier, modifiers, mod },
        { modifiers, mod }
      )

      const expectedModifier = {
        name: 'test',
        mod: Math.max(3 * multiplier, 5),
        class: 'test',
        modifierType: 'other',
        multiplier: 1,
      }
      expect(result.modifiers.get('test')).toEqual(expectedModifier)
      expect(result.modifiers.size).toBe(1)
    }
  )
})

describe('generateModificationFromTable', function () {
  test.each([
    ['test1', 1],
    ['test2', 2],
    ['test3', 3],
  ])(
    'should generate a modification from a table',
    function (value: string, mod: number) {
      const table = {
        test1: 1,
        test2: 2,
        test3: 3,
      }
      const modification = generateModificationFromTable(table, 'test')
      const modifiers = new Map()
      const result = modification(
        { test: value as 'test1' | 'test2' | 'test3', modifiers, mod: 0 },
        { modifiers, mod: 0 }
      )

      const expectedModifier = {
        name: value,
        mod,
        class: 'test',
        modifierType: 'other',
        multiplier: 1,
      }
      expect(result.modifiers.get('test')).toEqual(expectedModifier)
      expect(result.modifiers.size).toBe(1)
    }
  )
})

describe('generateModificationMapFromTable', function () {
  test('should generate a map of modifications from a table', function () {
    const table = {
      test1: 1,
      test2: 2,
      test3: 3,
    }
    const modificationMap = generateModificationMapFromTable(table, 'test')
    const modifiers = new Map()
    const modifierConfigs = new Map()
    modifierConfigs.set('test1', { count: 1, name: 'test1' })
    modifierConfigs.set('test2', { count: 3, name: 'test2' })
    const result = modificationMap(
      { test: modifierConfigs, modifiers, mod: 0 },
      { modifiers, mod: 0 }
    )

    const expectedModifier1 = {
      name: 'test1',
      mod: 1,
      class: 'test',
      modifierType: 'other',
      multiplier: 1,
    }
    const expectedModifier2 = {
      name: 'test2',
      mod: 2,
      class: 'test',
      modifierType: 'other',
      multiplier: 3,
    }
    expect(result.modifiers.get('test1')).toEqual(expectedModifier1)
    expect(result.modifiers.get('test2')).toEqual(expectedModifier2)
    expect(result.modifiers.size).toBe(2)
  })
})
