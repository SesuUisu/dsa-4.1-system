import { GenericShield } from '../../src/module/character/shield.js'
import type { DataAccessor } from '../../src/module/model/item-data.js'

describe('GenericShield', function () {
  const name = 'Wooden Shield'
  const attackMod = -2
  const parryMod = 3
  const initiativeMod = 2
  const itemData = {
    name,
    system: {
      initiativeMod,
      weaponMod: {
        attack: attackMod,
        parry: parryMod,
      },
    },
  } as DataAccessor<'shield'>

  it('should provide the name of the underlying shield', function () {
    const weapon = new GenericShield(itemData)
    expect(weapon.name).toEqual(name)
  })

  it('should provide the initiative mod of the underlying shield data', function () {
    const weapon = new GenericShield(itemData)
    expect(weapon.initiativeMod).toEqual(initiativeMod)
  })

  it('should provide the attack mod of the underlying shield data', function () {
    const weapon = new GenericShield(itemData)
    expect(weapon.weaponMod.attack).toEqual(attackMod)
  })

  it('should provide the parry mod of the underlying shield data', function () {
    const weapon = new GenericShield(itemData)
    expect(weapon.weaponMod.parry).toEqual(parryMod)
  })
})
