import { Attribute } from '../../src/module/character/attribute.js'
import { Character } from '../../src/module/character/character.js'

import { RollAttribute } from '../../src/module/ruleset/rules/basic-roll-mechanic.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('Attribute', function () {
  const attributeValue = 12
  const character = {
    data: {
      system: {
        base: {
          basicAttributes: {
            courage: {
              value: attributeValue,
            },
          },
        },
      },
    },
  } as Character
  const attributeName = 'courage'
  const ruleset = createMockRuleset()

  const attribute = new Attribute(character, attributeName, ruleset)

  it('should be have its given name', function () {
    expect(attribute.name).toEqual(attributeName)
  })

  it('should return the correct value of its related data attribute', function () {
    expect(attribute.value).toEqual(attributeValue)
  })

  it('should execute the roll attribute action on its given ruleset', function () {
    const rollOptions = {
      targetValue: attributeValue,
    }
    attribute.roll()
    expect(ruleset.execute).toHaveBeenCalledWith(
      RollAttribute,
      expect.objectContaining(rollOptions)
    )
  })
})
