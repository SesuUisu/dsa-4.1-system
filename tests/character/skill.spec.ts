import {
  GenericCombatTalent,
  GenericFormula,
  GenericLiturgy,
  GenericSource,
  GenericSpell,
  GenericTalent,
  RollableSkill,
  Skill,
  TestableSkill,
} from '../../src/module/character/skill.js'
import type { BaseCharacter } from '../../src/module/model/character.js'
import type {
  DataAccessor,
  FormulaData,
  LiturgyData,
  SourceData,
} from '../../src/module/model/item-data.js'
import { SpellModificationCategory } from '../../src/module/model/magic.js'
import type { TestAttributes } from '../../src/module/model/properties.js'
import { createMockRuleset } from '../ruleset/rules/helpers.js'

describe('Skill', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'spell'>

    const spell = Skill.create(spellData, character, ruleset)
    expect(spell?.skillType).toEqual('spell')
    expect(spell?.value).toEqual(spellValue)
    expect(spell?.name).toEqual(spellName)
  })

  it('should have a static creator for can take talent data', function () {
    const talentValue = 12
    const talentName = 'Verstecken'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const talentData = {
      name: talentName,
      type: 'talent',
      system: {
        value: talentValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'talent'>

    const talent = Skill.create(talentData, character, ruleset)
    expect(talent?.skillType).toEqual('talent')
    expect(talent?.value).toEqual(talentValue)
    expect(talent?.name).toEqual(talentName)
  })

  it('should have a static creator for can take combat talent data', function () {
    const talentValue = 12
    const talentName = 'Dolche'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        value: talentValue,
      },
    } as DataAccessor<'combatTalent'>

    const talent = Skill.create(talentData, character, ruleset)
    expect(talent?.skillType).toEqual('talent')
    expect(talent?.value).toEqual(talentValue)
    expect(talent?.name).toEqual(talentName)
  })

  it('should have a static creator for can take liturgy data', function () {
    const liturgyName = 'Beten'
    const liturgyDegree = 'II'
    const liturgyData = {
      name: liturgyName,
      type: 'liturgy',
      system: {
        degree: liturgyDegree,
      } as LiturgyData,
    } as DataAccessor<'liturgy'>

    character.karmaTalent = vi.fn()

    const liturgy = Skill.create(liturgyData, character, ruleset)
    expect(liturgy?.skillType).toEqual('liturgy')
    expect(liturgy?.name).toEqual(liturgyName)
  })
})

describe('RollableSkill', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'spell'>

    const spell = RollableSkill.create(spellData, character, ruleset)
    spell?.roll()
    expect(ruleset.execute).toHaveBeenCalled()
  })

  it('should have a static creator for can take source data', function () {
    const sourceName = 'Carafei'
    const sphere = 'demonic'
    const sourceData = {
      name: sourceName,
      type: 'source',
      system: {
        value: 12,
        sphere: sphere,
        description: '',
      } as SourceData,
    } as DataAccessor<'source'>

    const source = RollableSkill.create(sourceData, character, ruleset)
    source?.roll()
    expect(ruleset.execute).toHaveBeenCalled()
  })

  it('should have a static creator for can take formula data', function () {
    const formulaName = 'Attributo'
    const formulaData = {
      name: formulaName,
      type: 'formula',
      system: {
        description: '',
      } as FormulaData,
    } as DataAccessor<'formula'>

    const formula = RollableSkill.create(formulaData, character, ruleset)
    formula?.roll()
    expect(ruleset.execute).toHaveBeenCalled()
  })
})

describe('TestableSkill', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should have a static creator for can take spell data', function () {
    const spellValue = 12
    const spellName = 'Ignifaxius'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        value: spellValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
      },
    } as DataAccessor<'spell'>

    const spell = TestableSkill.create(spellData, character, ruleset)
    expect(spell?.testAttributes).toEqual(testAttributes)
  })
})

describe('GenericTalent', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide the effective encumbarance', function () {
    const talentValue = 12
    const talentName = 'Verstecken'
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']
    const talentData = {
      name: talentName,
      type: 'talent',
      system: {
        value: talentValue,
        test: {
          firstAttribute: testAttributes[0],
          secondAttribute: testAttributes[1],
          thirdAttribute: testAttributes[2],
        },
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'talent'>

    const talent = new GenericTalent(talentData, character, ruleset)
    expect(talent.effectiveEncumbarance).toEqual(
      talentData.system.effectiveEncumbarance
    )
  })
})

describe('GenericCombatTalent', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide the effective encumbarance', function () {
    const talentValue = 12
    const talentName = 'Verstecken'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        value: talentValue,
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'combatTalent'>

    const talent = new GenericCombatTalent(talentData, character, ruleset)
    expect(talent.effectiveEncumbarance).toEqual(
      talentData.system.effectiveEncumbarance
    )
  })

  it('should provide be unarmed for raufen and ringen', function () {
    const talentValue = 12
    const talentName = 'Raufen'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        sid: 'talent-raufen',
        value: talentValue,
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'combatTalent'>

    const talent = new GenericCombatTalent(talentData, character, ruleset)
    expect(talent.isUnarmed).toBeTruthy()
  })

  it('should provide be armed if not raufen and ringen', function () {
    const talentValue = 12
    const talentName = 'Dolch'
    const talentData = {
      name: talentName,
      type: 'combatTalent',
      system: {
        sid: 'talent-dolche',
        value: talentValue,
        effectiveEncumbarance: {
          type: 'formula',
          formula: 'BE-2',
        },
      },
    } as DataAccessor<'combatTalent'>

    const talent = new GenericCombatTalent(talentData, character, ruleset)
    expect(talent.isUnarmed).toBeFalsy()
  })
})

describe('GenericSpell', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide modifications. computed modifiers and target classes', function () {
    const allowedModifications: SpellModificationCategory[] = [
      'duration',
      'range',
    ]
    const spellName = 'Ignifaxius'

    const spellData = {
      name: spellName,
      type: 'spell',
      system: {
        modifications: allowedModifications,
        targetClasses: ['person', 'object'],
        castTime: {
          duration: 1,
          unit: 'action',
        },
        magicResistance: 3,
        astralCost: '1 AsP',
        range: '3 Schritt',
        effectTime: '1 Minute',
        requiresUphold: true,
        spellVariants: ['testVariant'],
      },
    } as unknown as DataAccessor<'spell'>

    const spell = new GenericSpell(spellData, character, ruleset)
    expect(spell.modifications).toEqual(allowedModifications)
    ruleset.compute = vi
      .fn()
      .mockReturnValue({ spontaneousModifications: ['duration'] })
    expect(spell.spellOptions.spontaneousModifications).toEqual(['duration'])

    expect(spell.targetClasses).toEqual(['person', 'object'])
    expect(spell.castTime).toEqual({ duration: 1, unit: 'action' })
    expect(spell.magicResistance).toEqual(3)
    expect(spell.astralCost).toEqual('1 AsP')
    expect(spell.range).toEqual('3 Schritt')
    expect(spell.effectTime).toEqual('1 Minute')
    expect(spell.requiresUphold).toBeTruthy()
    expect(spell.spellVariants).toEqual(['testVariant'])
    expect(spell.spellOptions).toEqual({
      spontaneousModifications: ['duration'],
    })

    const modifiers = new Map()
    modifiers.set('duration', 2)
    ruleset.compute = vi.fn().mockReturnValue({ modifiers })
    expect(spell.spellOutcome({})).toEqual({ modifiers })
  })
})

describe('GenericLiturgy', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide the degree, value and test attributes', function () {
    const liturgyDegree = 'II'
    const liturgyName = 'Beten'
    const karmaTalentValue = 4
    const testAttributes: TestAttributes = ['courage', 'cleverness', 'agility']

    const liturgyData = {
      name: liturgyName,
      type: 'liturgy',
      system: {
        degree: liturgyDegree,
      },
    } as DataAccessor<'liturgy'>

    character.karmaTalent = vi.fn().mockReturnValue({
      value: karmaTalentValue,
      testAttributes,
    })

    const liturgy = new GenericLiturgy(liturgyData, character, ruleset)
    expect(liturgy.degree).toEqual(liturgyDegree)
    expect(liturgy.value).toEqual(karmaTalentValue)
    expect(liturgy.testAttributes).toEqual(testAttributes)
  })
})

describe('GenericSource', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide value, sphere and determines the test attributes correct', function () {
    const sourceName = 'Carafei'
    const spellValue = 12
    const sphere = 'demonic'
    const testAttributes = ['courage', 'courage', 'charisma']

    const sourceData = {
      name: sourceName,
      type: 'source',
      system: {
        value: spellValue,
        sphere: sphere,
      } as SourceData,
    } as DataAccessor<'source'>

    const source = new GenericSource(sourceData, character, ruleset)
    expect(source.value).toEqual(spellValue)
    expect(source.testAttributes).toEqual(testAttributes)
  })
})

describe('GenericFormula', function () {
  const character = {} as BaseCharacter
  const ruleset = createMockRuleset()

  it('should provide value, sphere and determines the test attributes correct', function () {
    const formulaName = 'Attributo Mut'
    const quality = 5
    const testAttributes = ['courage', 'courage', 'charisma']

    const formulaData = {
      name: formulaName,
      type: 'formula',
      system: {
        quality: quality,
        sourceId: 'CarafeiId',
        instructionId: 'BesselungDesGeistesId',
      } as FormulaData,
    } as DataAccessor<'formula'>

    const sourceData = {
      name: 'Carafei',
      type: 'source',
      system: {
        value: 10,
        sphere: 'demonic',
      } as SourceData,
    } as DataAccessor<'source'>

    const source = Skill.create(sourceData, character, ruleset)

    character.source = vi.fn().mockReturnValue(source)

    const formula = new GenericFormula(formulaData, character, ruleset)
    expect(formula.quality).toEqual(quality)
    expect(formula.source).toEqual(source)
    expect(formula.testAttributes).toEqual(testAttributes)
  })
})
