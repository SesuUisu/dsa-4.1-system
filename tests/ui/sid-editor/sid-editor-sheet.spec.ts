import SidEditorSheet from '../../../src/ui/sid-editor/SidEditor.svelte'
import { fireEvent, render } from '@testing-library/svelte'
import userEvent from '@testing-library/user-event'
import { writable } from 'svelte/store'

describe('SidEditorSheet', () => {
  test('Change SID', async () => {
    const update = vi.fn()
    const doc = {
      system: {
        sid: 'test',
      },
      update,
    }
    const store = writable(doc)
    const { getByLabelText } = render(SidEditorSheet, {
      doc: store,
      localize: (l) => l,
    })
    const sidInput = getByLabelText('SID:')
    await userEvent.type(sidInput, 'newSid')
    await fireEvent.change(sidInput)

    expect(update).toHaveBeenCalledWith(
      expect.objectContaining({
        system: { sid: 'testnewSid' },
      })
    )
  })
})
