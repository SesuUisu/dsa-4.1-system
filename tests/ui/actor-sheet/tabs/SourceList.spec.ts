import { render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Dialog.svelte',
  async () => await import('../../MockDialog.svelte')
)
import { getGame } from '../../../../src/module/utils.js'

vi.mock('../../../../src/module/pdf-integration.ts', () => {
  const openPDFPage = vi.fn()
  class EnhancedJournalPDFPageSheet {}
  return {
    EnhancedJournalPDFPageSheet: EnhancedJournalPDFPageSheet,
    openPDFPage: openPDFPage,
  }
})

import SourceList from '../../../../src/ui/actor-sheet/tabs/SourceList.svelte'
import { writable } from 'svelte/store'
import { Mock } from 'vitest'
import { createMockRuleset } from '../../../ruleset/rules/helpers.js'

describe('SourceList', () => {
  test('Is rendered correctly', async () => {
    const ruleset = createMockRuleset()

    ;(getGame as Mock<any>).mockImplementation(() => ({
      ruleset,
    }))

    const source = {
      name: 'Carafei',
      system: {
        value: 11,
        sphere: 'demonic',
      },
      type: 'source',
      sheet: {
        render: vi.fn(),
      },
      update: vi.fn(),
    }

    const sources = [source, source, source]
    const context = {
      '#external': {},
      isEditMode: writable(true),
      doc: writable({
        system: {
          base: {},
        },
        sources,
      }),

      openDialogMock: vi.fn(),
    }
    const { findAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${SourceList}  />
      </$>
      `
    )

    const sourceList = await findAllByTestId('source-value')
    expect(sourceList.length).toEqual(3)
  })
})
