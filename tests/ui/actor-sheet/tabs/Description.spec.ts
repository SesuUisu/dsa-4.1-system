import { render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

vi.mock(
  '../../../../src/ui/Editor.svelte',
  async () => await import('../../MockEditor.svelte')
)

import Description from '../../../../src/ui/actor-sheet/tabs/Description.svelte'
import { writable } from 'svelte/store'

describe('Description', () => {
  test('Is rendered correctly', async () => {
    const description = 'testContent'
    const context = {
      doc: writable({
        system: {
          background: { description },
        },
      }),
    }

    const { getByTestId } = render(
      html`<${Fragment} context=${context}>
      <${Description} /> 
      </$>`
    )

    const label = getByTestId('editor')
    expect(label).toHaveValue('testContent')
  })
})
