import { fireEvent, render } from '@testing-library/svelte'

import SourceRoll from '../../../src/ui/roll-dialog/SourceRoll.svelte'
import { BaseCharacter } from '../../../src/module/model/character.js'
import { FormulaParameterClassNames } from '../../../src/module/model/myranor-magic.js'
import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'
import { Formula, Testable } from '../../../src/module/model/properties.js'
import { ModifierDescriptor } from '../../../src/module/model/modifier.js'

const settings = { get: vi.fn() }

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

vi.mock('../../../src/module/utils.js', async () => {
  const actual = (await vi.importActual(
    '../../../src/module/utils.js'
  )) as object
  return {
    ...actual,
    getSettings: () => settings,
  }
})

async function checkAndSetSelectOption(option, value, getByTestId) {
  const radioButton = getByTestId(
    `source-mod-${option}-${value}`
  ) as HTMLButtonElement
  await fireEvent.click(radioButton)
}

/*
- renders correct
- reacts on change of properties
- calculates advantage correct (none, other sphere, matching sphere)
- calculates effectiveEncumbarance correct
 */

describe('SourceRoll', () => {
  const effectiveEncumbarance = 2

  const encumbaranceModifier = {
    name: 'effectiveEncumbarance',
    mod: effectiveEncumbarance,
    modifierType: 'other',
  }

  const sourceModifiers = new Map([
    ['effectiveEncumbarance', encumbaranceModifier],
  ])

  FormulaParameterClassNames.forEach((category) => {
    sourceModifiers.set(category, {
      name: `formula-${category}`,
      mod: 0,
      modifierType: 'formulaModifier',
      hidden: true,
    })
  })

  const character = {
    data: {
      advantage: vi.fn().mockReturnValue(undefined),
    },
    sourceModifiers: vi.fn().mockReturnValue(sourceModifiers),
  } as unknown as BaseCharacter

  const testAttributes = ['courage', 'courage', 'charisma']
  const skill = {
    testAttributes,
    effectiveEncumbarance: {
      formula: 'BE-2',
      type: 'formula',
    },
    sphere: 'demonic',
  } as unknown as Testable<Formula>

  test('Is rendered correctly and submits correctly', async () => {
    const callback = vi.fn()

    settings.get.mockReturnValue(false)

    const localize = (a) => a
    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }

    render(
      html`
        <${Fragment} context=${context}>
          <${SourceRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
        </$>
      `
    )

    mockRoll.submit()

    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbarance,
      modifierType: 'other',
    }
    const rangeModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-range',
    }
    const structureModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-structure',
    }
    const castTimeModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-castTime',
    }
    const targetModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-target',
    }
    const durationModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-duration',
    }

    expect(callback).toHaveBeenCalledWith(
      new Map([
        ['effectiveEncumbarance', encumbaranceModifier],
        ['castTime', castTimeModifier],
        ['target', targetModifier],
        ['range', rangeModifier],
        ['duration', durationModifier],
        ['structure', structureModifier],
      ]),
      { testAttributes }
    )
  })

  test('Is rendered correctly and submits correctly when selecting a parameter', async () => {
    const callback = vi.fn()

    settings.get.mockReturnValue(false)

    const localize = (a) => a
    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }

    const { getByTestId } = render(
      html`
        <${Fragment} context=${context}>
          <${SourceRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
        </$>
      `
    )

    await checkAndSetSelectOption('range', 1, getByTestId)

    mockRoll.submit()

    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbarance,
      modifierType: 'other',
    }
    const rangeModifier = {
      hidden: true,
      mod: 1,
      modifierType: 'formulaModifier',
      name: 'formula-range',
    }
    const structureModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-structure',
    }
    const castTimeModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-castTime',
    }
    const targetModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-target',
    }
    const durationModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-duration',
    }

    expect(callback).toHaveBeenCalledWith(
      new Map([
        ['effectiveEncumbarance', encumbaranceModifier],
        ['castTime', castTimeModifier],
        ['target', targetModifier],
        ['range', rangeModifier],
        ['duration', durationModifier],
        ['structure', structureModifier],
      ]),
      { testAttributes }
    )
  })

  test('Is rendered correctly and submits correctly when selecting a parameter twice', async () => {
    const callback = vi.fn()

    settings.get.mockReturnValue(false)

    const localize = (a) => a
    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }

    const { getByTestId } = render(
      html`
        <${Fragment} context=${context}>
          <${SourceRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
        </$>
      `
    )

    await checkAndSetSelectOption('range', 1, getByTestId)
    await checkAndSetSelectOption('range', 3, getByTestId)

    mockRoll.submit()

    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbarance,
      modifierType: 'other',
    }
    const rangeModifier = {
      hidden: true,
      mod: 3,
      modifierType: 'formulaModifier',
      name: 'formula-range',
    }
    const structureModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-structure',
    }
    const castTimeModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-castTime',
    }
    const targetModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-target',
    }
    const durationModifier = {
      hidden: true,
      mod: 0,
      modifierType: 'formulaModifier',
      name: 'formula-duration',
    }

    expect(callback).toHaveBeenCalledWith(
      new Map([
        ['effectiveEncumbarance', encumbaranceModifier],
        ['castTime', castTimeModifier],
        ['target', targetModifier],
        ['range', rangeModifier],
        ['duration', durationModifier],
        ['structure', structureModifier],
      ]),
      { testAttributes }
    )
  })

  test('Is rendered correctly and submits correctly when selecting multiple parameters', async () => {
    const callback = vi.fn()

    settings.get.mockReturnValue(false)

    const localize = (a) => a
    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }

    const { getByTestId } = render(
      html`
        <${Fragment} context=${context}>
          <${SourceRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
        </$>
      `
    )

    const options = {
      range: 1,
      castTime: 4,
      structure: 7,
      duration: -1,
      target: 3,
    }

    for (const key in options) {
      await checkAndSetSelectOption(key, options[key], getByTestId)
    }

    mockRoll.submit()

    const encumbaranceModifier = {
      name: 'effectiveEncumbarance',
      mod: effectiveEncumbarance,
      modifierType: 'other',
    }
    const rangeModifier = {
      hidden: true,
      mod: options.range,
      modifierType: 'formulaModifier',
      name: 'formula-range',
    }
    const structureModifier = {
      hidden: true,
      mod: options.structure,
      modifierType: 'formulaModifier',
      name: 'formula-structure',
    }
    const castTimeModifier = {
      hidden: true,
      mod: options.castTime,
      modifierType: 'formulaModifier',
      name: 'formula-castTime',
    }
    const targetModifier = {
      hidden: true,
      mod: options.target,
      modifierType: 'formulaModifier',
      name: 'formula-target',
    }
    const durationModifier = {
      hidden: true,
      mod: options.duration,
      modifierType: 'formulaModifier',
      name: 'formula-duration',
    }

    expect(callback).toHaveBeenCalledWith(
      new Map([
        ['effectiveEncumbarance', encumbaranceModifier],
        ['castTime', castTimeModifier],
        ['target', targetModifier],
        ['range', rangeModifier],
        ['duration', durationModifier],
        ['structure', structureModifier],
      ]),
      { testAttributes }
    )
  })
})

describe('SourceRoll with advantage', () => {
  test.each([
    { value: 1, expectModifier: false },
    { value: 2, expectModifier: true },
    { value: 3, expectModifier: true },
  ])(
    'Is rendered correctly and submits correctly when the character has a aura conjunction with level 1/2/3',
    async function (conjunction) {
      const callback = vi.fn()

      settings.get.mockReturnValue(false)

      const localize = (a) => a
      const mockRoll = { submit: () => undefined }
      const context = {
        mock: writable(mockRoll),
      }

      const sourceModifiers = new Map<string, ModifierDescriptor>()
      if (conjunction.expectModifier) {
        sourceModifiers.set('auraConjunction', {
          name: 'auraConjunction',
          mod: -3,
          modifierType: 'other',
        })
      }
      sourceModifiers.set('effectiveEncumbarance', {
        name: 'effectiveEncumbarance',
        mod: 0,
        modifierType: 'other',
      })

      const character = {
        data: {
          advantage: vi.fn().mockReturnValue({
            system: {
              value: conjunction.value,
            },
          }),
        },
        effectiveEncumbarance: vi.fn().mockReturnValue(0),
        sourceModifiers: vi.fn().mockReturnValue(sourceModifiers),
      } as unknown as BaseCharacter

      const testAttributes = ['courage', 'courage', 'charisma']
      const skill = {
        testAttributes,
        effectiveEncumbarance: {
          formula: 'BE-2',
          type: 'formula',
        },
        sphere: 'demonic',
      } as unknown as Testable<Formula>

      render(
        html`
        <${Fragment} context=${context}>
          <${SourceRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
        </$>
      `
      )

      const encumbaranceModifier = {
        name: 'effectiveEncumbarance',
        mod: 0,
        modifierType: 'other',
      }

      const expectedMap = new Map([
        ['effectiveEncumbarance', encumbaranceModifier],
      ])

      if (conjunction.expectModifier) {
        expectedMap.set('auraConjunction', {
          name: 'auraConjunction',
          mod: -3,
          modifierType: 'other',
        })
      }

      mockRoll.submit()

      expect(callback).toHaveBeenCalledWith(expectedMap, { testAttributes })
    }
  )
})
