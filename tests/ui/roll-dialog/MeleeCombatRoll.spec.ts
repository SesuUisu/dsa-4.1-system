import { fireEvent, render } from '@testing-library/svelte'

import { when } from 'jest-when'

import MeleeCombatRoll from '../../../src/ui/roll-dialog/MeleeCombatRoll.svelte'
import { BaseCharacter } from '../../../src/module/model/character.js'

import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'

const settings = { get: vi.fn() }

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

vi.mock('../../../src/module/utils.js', async () => {
  const actual = (await vi.importActual(
    '../../../src/module/utils.js'
  )) as object
  return {
    ...actual,
    getSettings: () => settings,
  }
})

describe('MeleeCombatRoll', () => {
  const character = {
    rangedAttackModifiers: vi.fn(),
    rangedAttackDuration: vi.fn(),
  } as unknown as BaseCharacter

  test('Is rendered correctly and submits correctly', async () => {
    const callback = vi.fn()

    const actionModifierCalculator = vi.fn().mockReturnValue(new Map())

    const availableManeuvers = [
      {
        name: 'testManeuver',
        minMod: 4,
      },
    ]

    settings.get.mockReturnValue(false)

    const localize = (a) => a
    const mockRoll = { submit: () => undefined }
    const context = {
      mock: writable(mockRoll),
    }

    const { getByText } = render(
      html`
    <${Fragment} context=${context}>
    <${MeleeCombatRoll} callback=${callback} localize=${localize} character=${character} actionModifierCalculator=${actionModifierCalculator} availableManeuvers=${availableManeuvers} />
    </$>
    `
    )

    const testManeuver = getByText('testManeuver')
    expect(testManeuver).toBeInTheDocument()
    await fireEvent.click(testManeuver)

    for (let i = 4; i < 15; i++) {
      const input = getByText(i.toString())
      expect(input).toBeInTheDocument()
    }

    const testMod = 6
    const expectedModifier = new Map([
      [
        'testManeuver',
        {
          name: 'testManeuver',
          minMod: 4,
          mod: testMod,
        },
      ],
    ])
    when(actionModifierCalculator)
      .calledWith(
        expect.objectContaining({
          maneuvers: expect.arrayContaining([
            {
              name: 'testManeuver',
              minMod: 4,
              mod: testMod,
            },
          ]),
        })
      )
      .mockReturnValue(expectedModifier)
    const input = getByText(testMod.toString())
    await fireEvent.click(input)

    mockRoll.submit()

    expect(actionModifierCalculator).toHaveBeenCalledWith(
      expect.objectContaining({
        maneuvers: expect.arrayContaining([
          {
            name: 'testManeuver',
            minMod: 4,
            mod: testMod,
          },
        ]),
      })
    )
    expect(callback).toHaveBeenCalledWith(expectedModifier, undefined)
  })
})
