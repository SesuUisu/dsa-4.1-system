import { fireEvent, render } from '@testing-library/svelte'
import { get, writable } from 'svelte/store'
import Roll from '../../../src/ui/roll-dialog/Roll.svelte'
import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'

describe('Roll', () => {
  test('Is rendered correctly and submits correctly', async () => {
    const localize = (a) => a
    const callback = vi.fn()
    const closePromise = new Promise((resolve) => resolve(true))
    const context = {
      '#managedPromise': { get: () => closePromise },
    }

    const options = writable({
      sizeClass: 'small',
      test: '',
    })

    const modifiers = writable(new Map())

    const { getByRole, getByTestId, queryAllByTestId } = render(html`
      <${Fragment} context=${context}>
        <${Roll} callback=${callback} localize=${localize} bind:options=${options} bind:modifiers=${modifiers}>
          <div slot="options" data-testid="options-slot">options</div>
          <div slot="options-bottom" data-testid="options-bottom-slot">options-bottom</div>
        </${Roll}>
      </$>
    `)

    const optionsSlot = getByTestId('options-slot')
    expect(optionsSlot).toBeInTheDocument()
    expect(optionsSlot).toHaveTextContent('options')

    const optionsBottomSlot = getByTestId('options-bottom-slot')
    expect(optionsBottomSlot).toBeInTheDocument()
    expect(optionsBottomSlot).toHaveTextContent('options-bottom')

    const input = getByRole('spinbutton')
    expect(input).toBeInTheDocument()
    expect(input.parentElement).toHaveTextContent('modifier')

    options.set({
      sizeClass: 'large',
      test: 'test',
    })

    const additionalModifier = {
      mod: 4,
      modifierType: 'offensive',
      name: 'test',
    }
    const additionalModifier2 = {
      mod: 4,
      multiplier: 2,
      modifierType: 'offensive',
      name: 'test2',
    }

    let modifierEntries = queryAllByTestId('modifier-list-entry')
    expect(modifierEntries).toHaveLength(0)

    let totalModifier = getByTestId('total-modifier')
    expect(totalModifier).toHaveTextContent('0')

    get(modifiers).set('test', additionalModifier)
    get(modifiers).set('test2', additionalModifier2)

    const inputValue = 23

    await fireEvent.input(input, { target: { value: inputValue } })

    modifierEntries = queryAllByTestId('modifier-list-entry')
    expect(modifierEntries).toHaveLength(2)
    expect(modifierEntries[0]).toHaveTextContent('test : 4')
    expect(modifierEntries[1]).toHaveTextContent('2 x test2 : 8')

    totalModifier = getByTestId('total-modifier')
    expect(totalModifier).toHaveTextContent('35')

    await closePromise
    const expectedModifiers = new Map()
    expectedModifiers.set('custom', {
      mod: inputValue,
      modifierType: 'other',
      name: 'custom',
    })
    expectedModifiers.set('test', additionalModifier)
    expectedModifiers.set('test2', additionalModifier2)
    expect(callback).toHaveBeenCalledWith(expectedModifiers, get(options))
  })
})
