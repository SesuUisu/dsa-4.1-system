import SpellRoll from '../../../src/ui/roll-dialog/SpellRoll.svelte'
import { render } from '@testing-library/svelte'

import { BaseCharacter } from '../../../src/module/model/character.js'
import { writable } from 'svelte/store'
import Fragment from '@playpilot/svelte-fragment-component'
import html from '@playpilot/svelte-htm'

vi.mock(
  '../../../src/ui/roll-dialog/Roll.svelte',
  async () => await import('./MockRoll.svelte')
)

describe('SpellRoll', () => {
  const callback = vi.fn()
  const localize = (a) => a
  const mockRoll = { submit: () => undefined }
  const context = {
    mock: writable(mockRoll),
  }
  const testAttributes = ['courage', 'courage', 'courage']

  beforeEach(() => {
    callback.mockReset()
  })

  test('Render correctly without spomods', async () => {
    const skill = {
      skillType: 'spell',
      modifications: [],
      spellOutcome: vi.fn().mockReturnValue({
        castTime: {
          duration: 1,
          unit: 'action',
        },
        range: {
          value: '3 Schritt',
          stepModifier: 0,
        },
        effectTime: {
          value: '1 Minute',
          multiplier: 0,
        },
        astralCost: '3 AsP',
      }),
      spellOptions: {
        spellVariants: [],
        spontaneousModifications: [],
      },
      testAttributes,
    }
    const character = {} as unknown as BaseCharacter

    const { queryAllByTestId, getByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${SpellRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )
    const spellInfo = getByTestId('spell-info')
    expect(spellInfo).toHaveTextContent('castTime 1 action')

    const astralCostInfo = getByTestId('astral-cost')
    expect(astralCostInfo).toHaveTextContent('cost 3 AsP')

    const rangeInfo = getByTestId('range')
    expect(rangeInfo).toHaveTextContent('range 3 Schritt')

    const effectTimeInfo = getByTestId('effect-time')
    expect(effectTimeInfo).toHaveTextContent('effectTime 1 Minute')

    const spontaneousModifications = queryAllByTestId(
      'spontaneous-modification'
    )
    expect(spontaneousModifications).toHaveLength(0)
  })

  test.each([
    [0, false],
    [undefined, false],
    [1, true],
    [2, true],
    ['max', true],
  ])(
    'Should show magic resistance if available',
    async (magicResistance: number | string | undefined, showMR: boolean) => {
      const skill = {
        skillType: 'spell',
        modifications: [],
        spellOutcome: vi.fn().mockReturnValue({
          castTime: {
            duration: 1,
            unit: 'action',
          },
          range: {
            value: '3 Schritt',
            stepModifier: 0,
          },
          effectTime: {
            value: '1 Minute',
            multiplier: 0,
          },
          magicResistance,
        }),
        spellOptions: {
          spellVariants: [],
          spontaneousModifications: [],
        },
        testAttributes,
      }
      const character = {} as unknown as BaseCharacter

      const { queryByTestId } = render(
        html`
      <${Fragment} context=${context}>
      <${SpellRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
      )
      const MRInfo = queryByTestId('magic-resistance')
      if (showMR) {
        expect(MRInfo).toHaveTextContent(
          'magicResistance ' + magicResistance + ' magicResistanceAbbr'
        )
      } else {
        expect(MRInfo).toBeNull()
      }
    }
  )

  test.each([
    [0.5, '/'],
    [2, 'x'],
  ])(
    'Render correctly with spomods',
    async (effectTimeMultiplier: number, effectTimeMulOrDivide: string) => {
      const skill = {
        skillType: 'spell',
        modifications: [],
        spellOutcome: vi.fn().mockReturnValue({
          targetClasses: ['test'],
          castTime: {
            duration: 1,
            unit: 'action',
            additionalCastTime: {
              duration: 1,
              unit: 'action',
              info: '',
            },
          },
          range: {
            value: '3 Schritt',
            stepModifier: 2,
          },
          effectTime: {
            value: '1 Minute',
            multiplier: effectTimeMultiplier,
          },
        }),
        spellOptions: {
          spellVariants: [
            {
              name: 'testVariant',
              modificator: 4,
              minimalValue: 2,
              astralCost: '2 AsP',
              description: 'testVariantDescription',
            },
            {
              name: 'testVariant2',
              modificator: 4,
              minimalValue: 2,
              astralCost: '2 AsP',
              description: 'testVariantDescription',
            },
          ],
          spontaneousModifications: ['test', 'test2'],
        },
        testAttributes,
      }
      const character = {} as unknown as BaseCharacter

      const { queryAllByTestId, getByTestId } = render(
        html`
      <${Fragment} context=${context}>
      <${SpellRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
      )

      const targetClasses = queryAllByTestId('target-class')
      expect(targetClasses).toHaveLength(1)
      expect(targetClasses[0]).toHaveTextContent('test')

      const spellInfo = getByTestId('spell-info')
      expect(spellInfo).toHaveTextContent('castTime 1 action + 1 action')

      const rangeInfo = getByTestId('range')
      expect(rangeInfo).toHaveTextContent('range 3 Schritt (+ 2 step)')

      const effectTimeInfo = getByTestId('effect-time')
      expect(effectTimeInfo).toHaveTextContent(
        'effectTime 1 Minute (' + effectTimeMulOrDivide + ' 2)'
      )

      const spontaneousModifications = queryAllByTestId(
        'spontaneous-modification'
      )
      expect(spontaneousModifications).toHaveLength(2)
      expect(spontaneousModifications[0]).toHaveTextContent('test')
      expect(spontaneousModifications[1]).toHaveTextContent('test2')

      const spellVariants = queryAllByTestId('spell-variant')
      expect(spellVariants).toHaveLength(2)
      expect(spellVariants[0]).toHaveTextContent('testVariant')
      expect(spellVariants[1]).toHaveTextContent('testVariant2')
    }
  )

  test('Add and remove spomods should add modifiers', async () => {
    const skill = {
      skillType: 'spell',
      modifications: [],
      spellOutcome: vi.fn(),
      spellOptions: {
        spellVariants: [],
        spontaneousModifications: ['test'],
      },
      testAttributes,
    }
    const character = {} as unknown as BaseCharacter

    skill.spellOutcome.mockImplementation((options) => ({
      castTime: {
        duration: 1,
        unit: 'action',
      },
      modifiers: new Map(
        options.spontaneousModifications?.get('test')?.count > 0
          ? [['test', { mod: 1, name: 'test' }]]
          : []
      ),
      range: {
        value: '3 Schritt',
        stepModifier: 0,
      },
      effectTime: {
        value: '1 Minute',
        multiplier: 0,
      },
    }))
    const { getByTestId, queryAllByTestId } = render(
      html`
      <${Fragment} context=${context}>
      <${SpellRoll} callback=${callback} localize=${localize} character=${character} skill=${skill} />
      </$>
      `
    )

    let modifiers = queryAllByTestId('modifier-list-entry')
    expect(modifiers).toHaveLength(0)
    const addSpomod = getByTestId('add-modification')
    await addSpomod.click()
    getByTestId('modifier-list-entry')
    modifiers = queryAllByTestId('modifier-list-entry')
    expect(modifiers).toHaveLength(1)
    expect(modifiers[0]).toHaveTextContent('test')
    const removeSpomod = getByTestId('remove-modification')
    await removeSpomod.click()
    modifiers = queryAllByTestId('modifier-list-entry')
    expect(modifiers).toHaveLength(0)
  })
})
