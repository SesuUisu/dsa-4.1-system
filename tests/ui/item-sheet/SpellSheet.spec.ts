import { render } from '@testing-library/svelte'

import html from '@playpilot/svelte-htm'
import Fragment from '@playpilot/svelte-fragment-component'
import { writable } from 'svelte/store'

const Hooks = {
  once: vi.fn(),
}
global.Hooks = Hooks
import SpellSheet from '../../../src/ui/item-sheet/SpellSheet.svelte'
import {
  SpellModificationCategories,
  SpellTargetClasses,
} from '../../../src/module/model/magic.js'
import { fireEvent } from '@testing-library/dom'

describe('SpellSheet', () => {
  test('Is rendered correctly', async () => {
    const doc = {
      name: 'testName',
      system: {
        test: ['courage', 'courage', 'courage'],
        castTime: {
          duration: 1,
          unit: 'action',
        },
        effectTime: '5',
        range: '3 Schritt',
        value: 5,
        magicResistance: 3,
        astralCost: '3 AsP',
        requiresUphold: false,
        lcdPage: 1,
        targetClasses: ['object', 'person'],
        modifications: ['duration'],
        description: 'testDescription',
        spellVariants: [
          {
            name: 'testVariant',
            modificator: 4,
            minimalValue: 2,
            astralCost: '2 AsP',
            description: 'testVariantDescription',
          },
        ],
      },
    }
    const context = {
      doc: writable(doc),
    }
    const { getByTestId, queryAllByTestId } = render(
      html`
              <${Fragment} context=${context}>
                <${SpellSheet} />
              </$>
              `
    )

    const testAttributes = getByTestId('test-attributes')
    const testAttributSelection = testAttributes.querySelectorAll('select')
    expect(testAttributSelection.length).toBe(3)
    expect(testAttributSelection[0].value).toBe('courage')
    expect(testAttributSelection[1].value).toBe('courage')
    expect(testAttributSelection[2].value).toBe('courage')

    const spellValue = getByTestId('spell-value').querySelector('input')
    expect(spellValue?.value).toBe('5')

    const magicResistance =
      getByTestId('magic-resistance').querySelector('input')
    expect(magicResistance?.value).toBe('3')

    const castTime = getByTestId('cast-time')
    expect(castTime.querySelector('input')?.value).toBe('1')
    expect(castTime.querySelector('select')?.value).toBe('action')

    const astralCost = getByTestId('astral-cost').querySelector('input')
    expect(astralCost?.value).toBe('3 AsP')

    const range = getByTestId('range').querySelector('input')
    expect(range?.value).toBe('3 Schritt')

    const effectTime = getByTestId('effect-time').querySelector('input')
    expect(effectTime?.value).toBe('5')

    const requiresUphold = getByTestId('requires-uphold').querySelector('input')
    expect(requiresUphold?.checked).toBe(false)

    const lcdPage = getByTestId('lcd-page').querySelector('input')
    expect(lcdPage?.value).toBe('1')

    const targetClasses =
      getByTestId('target-classes').querySelectorAll('input')
    expect(targetClasses.length).toEqual(SpellTargetClasses.length)
    expect(targetClasses[0].checked).toBe(true)
    expect(targetClasses[1].checked).toBe(false)
    expect(targetClasses[2].checked).toBe(true)
    expect(targetClasses[3].checked).toBe(false)

    const modifications = getByTestId('modifications').querySelectorAll('input')
    expect(modifications.length).toEqual(SpellModificationCategories.length - 2)
    expect(modifications[0].checked).toBe(false)
    expect(modifications[5].checked).toBe(true)

    const description = getByTestId('description').querySelector('textarea')
    expect(description?.value).toBe('testDescription')

    const spellVariants = queryAllByTestId('variants')
    expect(spellVariants.length).toBe(1)

    const addSpellVariant = getByTestId('add-variant')
    await fireEvent.click(addSpellVariant)
    expect(doc.system.spellVariants.length).toBe(2)
  })
})
