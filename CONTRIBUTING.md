# Intro
Code and content contributions are welcome. Note that our issue tracker is currenlty poorly maintained. Therefore it is currently better to contact us on our discord server. If you want to provide contributions please follow the guidelines from this document.

#### What can I contribute besides new features?
- Update/Enhance user documentation
    - You can even create an in-game documentation with Journal Entries
- Test the system and report bugs
- Add missing translations and fix typos
- Add missing unit tests
- Refactor and clean up the unit tests
- Refactor and clean up the code base
- Fix issues reported by code climate
- Fix the foundry typings (https://github.com/League-of-Foundry-Developers/foundry-vtt-types)
    - There is a dev version that should somehow work (at least in SWADE it seams to be working)
- Enhance UI/UX

# Development Setup

After cloning this repository you have to install the dependencies with `npm install`. You should also run `npm run prepare` to make the pre-commit hooks work in your environment.

### Working with VS Code

If you are working with Visual Studio Code it is recommended to use the following extensions:

- Svelte for VS Code
- ESLint
- Prettier - Code formatter
- Prettier ESLint
- Vitest
- Vitest Runner

### Running the build for development

On a first time build (and after each compemdium update) you need to generate the databases for the compendium packs. The compendium packs are generated with `npm run build-packs`.

You can run the build with `npm run build`. The resulting distribution is located in the `dist` directory.

You can start a development environment with hot reloading via `npm run dev`. This enables you to make code changes without the need of reloading the foundry page. By default the Foundry server is expected to run on `localhost:30001`. If you run the above command you will be provided with a new local URL which acts as a proxy and injects your changes.

### Configuring your local dev environment

If you want to use your build directly in Foundry you have two options:

- Link the `dist` directory to the `systems/dsa-41` path in your Foundry installation.
- Change `FVTTDEV_DEPLOY_PATH` in `env/build-dev.env` to point to the system path in your Foundry installation

You can configure your Foundry server in `env/foundry.env`. Just copy the content of `env/foundry.env.example` and adjust it to your needs. This is needed for the `npm run dev` command.

### Testing Code

#### Unit Tests
To execute the unit tests you can run `npm run test` or `npm run test:watch` if you want to automatically rerun the tests on changes. You can also execute `npm run testWithCoverage` if you want the get the coverage results.

#### ES Lint
To execute ES Lint you can run `npm run lint`. This will provide you an overview of errors and warnings regarding possible problems in the code. If you have activated the commit hooks you will not be able to commit if the linter throws any errors.

#### Prettier
To enforce a certain code style we use Prettier. You can check the code with `npm run prettier`. To automatically fix the code based on the prettier rules you can run `npm run prettier:fix`. If you have activated the commit hooks you will not be able to commit if prettier throws any errors.

# Contributing Code
### Merge Requests
When you want to contribute something to the codebase please create a merge request. It is a best practice to keep merge requests small (around 200-400 lines of code changes). The reason for that is, that it is easier to comprehend changes of that size. This also means that you can get better feedback faster.

If you are working on a bigger feature this means that you have to split up your changes into meaningful smaller merge requests.

Every merge requests needs a description that outline the changes you have done.

Please also note that after you created a merge request it can take some time until you get feedback.

### Unit Tests
Please always try to add or update unit tests in a meaningful way for all your code changes. It is ok to leave them out if you interact directly with the Foundry API as it can be hard in these places. (But it would be appreciated)

### Code Style
Currently there are still some things badly organized in the code, but the goal should be to obtain a good separation of concerns. Mainly, rules, UI and Foundry-integration need to be separated in a clean way.

Here are some guidelines for the current state:
- **Datamodel and typings**: Foundry datamodel is described in `template.json` and the related types can be found in `model`. All types that are needed in more than one module should always be described in `model`.
- **UI**: The Svelte based UI components are located in `ui`. The handbars templates in `templates`. Note that all new UI development should be done with Svelte.
- **Foundry Integration**: This is a little bit scattered currently. Actor related stuff can be found in `actor`. Item releated stuff can be found in `item`. But there are also some other modules in the base `module` directory.
- **Rules**: Rules should always be desribed via the ruleset in `ruleset`.
- **Integrating Rules, Foundry and UI**: We need a clean way to interact with the Foundry based classes and the rules. This is mainly done in `character`.

