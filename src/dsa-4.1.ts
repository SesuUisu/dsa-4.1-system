import './styles/dsa-4.1.css'

// Import Modules
import { DsaActor } from './module/actor/actor.js'
import { DsaActorSheet } from './module/actor/actor-sheet.js'
import { DsaSvelteActorSheet } from './module/actor/actor-sheet-svelte.js'
import { preloadHandlebarsTemplates } from './module/preload-templates.js'
import './module/item/item.js'
import { DsaItemSheet } from './module/item/item-sheet.js'
import { LiturgySheet } from './module/item/liturgy-sheet.js'
import { SourceSheet } from './module/item/source-sheet.js'
import { FormulaSheet } from './module/item/formula-sheet.js'
import { FormulaInstructionSheet } from './module/item/formulaInstruction-sheet.js'
import {
  TalentSheet,
  LinguisticTalentSheet,
  CombatTalentSheet,
} from './module/item/talent-sheet.js'
import './module/item/talent.js'
import { DsaCombatant } from './module/combat.js'
import { DsaActiveEffectConfig } from './module/active-effect.js'

import './module/bar-brawl-integration.js'
import './module/quick-roll.js'
import './module/nicer-dsa-roll.js'

import { Ruleset } from './module/ruleset/ruleset.js'
import { BasicCombatRule } from './module/ruleset/rules/basic-combat.js'
import { BasicRangedCombatRule } from './module/ruleset/rules/basic-ranged-combat.js'
import { DerivedAttributesRule } from './module/ruleset/rules/derived-attributes.js'
import { DerivedCombatAttributesRule } from './module/ruleset/rules/derived-combat-attributes.js'
import { BasicShieldCombatRule } from './module/ruleset/rules/basic-shield-combat.js'
import { WeaponModifierRule } from './module/ruleset/rules/weapon-modifier.js'
import { WeaponStrengthModifierRule } from './module/ruleset/rules/weapon-strength-modifier.js'
import { BasicRollMechanicRule } from './module/ruleset/rules/basic-roll-mechanic.js'
import { BasicManeuverRule } from './module/ruleset/rules/maneuvers/basic-maneuver.js'
import { ManeuverRules } from './module/ruleset/rules/maneuvers/maneuvers.js'
import { SpecialAbilityRules } from './module/ruleset/rules/special-abilities/special-abilities.js'
import { EnhancedRangedCombatRule } from './module/ruleset/rules/enhanced-ranged-combat.js'
import { AdvantageRules } from './module/ruleset/rules/advantages/advantages.js'
import { DisAdvantageRules } from './module/ruleset/rules/dis-advantages/dis-advantages.js'

import { DSADie } from './module/dsa-die.js'

import { BasicSkillRule } from './module/ruleset/rules/basic-skill.js'
import { FulminictusRule } from './module/ruleset/rules/spells/fulminictus.js'
import { getGame, getLocalizer } from './module/utils.js'
import { migrateSystem } from './migrations.js'
import { getSystemSetting, registerSettings } from './module/settings.js'
import { BasicKarmaRule } from './module/ruleset/rules/basic-karma.js'

import { EnhancedJournalPDFPageSheet } from './module/pdf-integration.js'
import { SidEditorSheet } from './module/item/sid-editor-sheet.js'
import { registerMacros } from './macros/macros.js'
import { Character } from './module/character/character.js'
import {
  BasicMyranorFormulaRule,
  BasicMyranorSourceRule,
} from './module/ruleset/rules/myranor/formula.js'
import { modifierLabel } from './module/i18n.js'
import { BasicSpellRule } from './module/ruleset/rules/basic-spell.js'
import { SvelteItemSheet } from './module/item/item-sheet-svelte.js'
import { MyranorItemType, MyranorItemTypes } from './module/model/item-data.js'
import { registerDataModels } from './module/data/data-models.js'
import { BasicRestingComputationRule } from './module/ruleset/rules/basic-resting-computation.js'
import { BasicRestingActionRule } from './module/ruleset/rules/basic-resting-action.js'

Hooks.once('init', async function () {
  console.log('Initialisiere DSA 4.1 System')

  registerDataModels()

  CONFIG.Actor.documentClass = DsaActor
  CONFIG.Combatant.documentClass = DsaCombatant

  // CONFIG.ActiveEffect.sheetClass = DsaActiveEffectConfig
  DocumentSheetConfig.registerSheet(
    ActiveEffect,
    'dsa-41',
    DsaActiveEffectConfig,
    {
      makeDefault: true,
    }
  )

  DocumentSheetConfig.registerSheet(
    JournalEntryPage,
    'core',
    EnhancedJournalPDFPageSheet,
    {
      types: ['pdf'],
      makeDefault: true,
      label: () =>
        game.i18n.format('JOURNALENTRYPAGE.DefaultPageSheet', {
          page: game.i18n.localize('JOURNALENTRYPAGE.TypePDF'),
        }),
    }
  )

  Actors.unregisterSheet('core', ActorSheet)
  Actors.registerSheet('dsa-41', DsaActorSheet, { makeDefault: true })
  Actors.registerSheet('dsa-41', DsaSvelteActorSheet)

  Items.registerSheet('dsa-41', DsaItemSheet, {
    types: [
      'meleeWeapon',
      'rangedWeapon',
      'armor',
      'shield',
      'advantage',
      'disadvantage',
      'specialAbility',
      'skill',
      'spellVariant',
      'liturgyVariant',
      'genericItem',
      'source',
      'formulaInstruction',
      'formula',
    ],
    makeDefault: true,
  })
  Items.registerSheet('dsa-41', SvelteItemSheet, {
    types: ['spell'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-41', LiturgySheet, {
    types: ['liturgy'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-41', TalentSheet, {
    types: ['talent'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-41', CombatTalentSheet, {
    types: ['combatTalent'],
    makeDefault: true,
  })
  Items.registerSheet('dsa-41', LinguisticTalentSheet, {
    types: ['language', 'scripture'],
    makeDefault: true,
  })

  Handlebars.registerHelper('concat', function (...args) {
    let outStr = ''

    for (const arg of args) {
      if (typeof arg !== 'object') {
        outStr += arg
      }
    }

    return outStr
  })

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase()
  })

  Handlebars.registerHelper('getSystemSettings', getSystemSetting)
  Handlebars.registerHelper('modifierLabel', (key) =>
    modifierLabel(key, getLocalizer())
  )

  await preloadHandlebarsTemplates()
})

Hooks.once('init', async function () {
  const game = getGame()
  game.ruleset = new Ruleset(game.settings)
  // game.ruleset.add(BasicRollMechanicRule)
  BasicRollMechanicRule(game.ruleset)
  game.ruleset.add(BasicSkillRule)
  game.ruleset.add(BasicCombatRule)
  game.ruleset.add(DerivedAttributesRule)
  game.ruleset.add(DerivedCombatAttributesRule)
  game.ruleset.add(BasicRangedCombatRule)
  game.ruleset.add(BasicShieldCombatRule)
  game.ruleset.add(BasicSpellRule)
  game.ruleset.add(BasicKarmaRule)
  game.ruleset.add(BasicRestingComputationRule)
  game.ruleset.add(BasicRestingActionRule)
  game.ruleset.add(BasicMyranorFormulaRule)
  game.ruleset.add(BasicMyranorSourceRule)

  SpecialAbilityRules.forEach((specialAbility) =>
    game.ruleset.add(specialAbility)
  )
  AdvantageRules.forEach((advantage) => game.ruleset.add(advantage))
  DisAdvantageRules.forEach((disAdvantage) => game.ruleset.add(disAdvantage))

  game.ruleset.compileRules()
})

Hooks.once('setup', async function () {
  const game = getGame()
  game.ruleset.add(WeaponModifierRule)
  game.ruleset.add(WeaponStrengthModifierRule)

  game.ruleset.add(BasicManeuverRule)
  ManeuverRules.forEach((maneuverRule) => game.ruleset.add(maneuverRule))

  game.ruleset.add(FulminictusRule)
  game.ruleset.add(EnhancedRangedCombatRule)

  game.ruleset.compileRules()
})

function registerMyranorSheets(
  myranoSheets: Record<MyranorItemType, DsaItemSheet>
) {
  Items.registerSheet('dsa-41', DsaItemSheet, {
    types: [...MyranorItemTypes],
  })

  for (const [type, sheet] of Object.entries(myranoSheets)) {
    if (sheet) {
      Items.registerSheet('dsa-41', sheet, {
        types: [type],
        makeDefault: true,
      })
    }
  }
}

function deregisterMyranorItems() {
  // item types have to be available in the init in case instances of the items are present
  game.system.documentTypes.Item = Object.fromEntries(
    Object.entries(game.system.documentTypes.Item).filter(
      ([type, _]) => !(MyranorItemTypes as ReadonlyArray<string>).includes(type)
    )
  )
}

Hooks.once('ready', async function () {
  await migrateSystem()

  const game = getGame()
  game.ruleset.compileRules()

  registerSettings()

  const myranorSheets: Record<MyranorItemType, DsaItemSheet> = {
    source: SourceSheet,
    formulaInstruction: FormulaInstructionSheet,
    formula: FormulaSheet,
  }

  getSystemSetting('myranor')
    ? registerMyranorSheets(myranorSheets)
    : deregisterMyranorItems()

  registerMacros()
})

Hooks.once('init', async function () {
  CONFIG.Dice.terms['z'] = DSADie
})

Hooks.once('ready', async function () {
  game.sidEditor = () => {
    new SidEditorSheet().render(true)
  }

  game.testApplyDamage = (actor, damage, wounds) => {
    const character = new Character(actor)
    character.takeDamage(damage, wounds)
  }
})
