import { baseItem, buyable, physical } from './common.js'

const fields = foundry.data.fields

export class ShieldDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof ShieldDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...buyable(),
      ...physical(),
      type: new fields.StringField({ initial: '', required: true }),
      sizeClass: new fields.StringField({ initial: '', required: true }),
      weaponMod: new fields.SchemaField({
        attack: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        parry: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
      }),
      initiativeMod: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      breakingFactor: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
    }
  }
}
