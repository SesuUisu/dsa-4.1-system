import { baseItem, buyable, physical } from './common.js'

const fields = foundry.data.fields

export class GenericItemDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof GenericItemDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...buyable(),
      ...physical(),
      isConsumable: new fields.BooleanField({ initial: false }),
      quantity: new fields.NumberField({
        initial: 1,
        integer: true,
        required: true,
        nullable: false,
      }),
    }
  }
}
