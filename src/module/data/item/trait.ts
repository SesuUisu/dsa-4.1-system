import { baseItem, uniquelyIdentifiable } from './common.js'

const fields = foundry.data.fields

const makeDisAdvantage = () => ({
  ...baseItem(),
  ...uniquelyIdentifiable(),
  value: new fields.NumberField({
    initial: null,
    integer: true,
    nullable: true,
    required: true,
  }),
})

export class AdvantageDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof AdvantageDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...makeDisAdvantage(),
    }
  }
}

export class DisadvantageDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof DisadvantageDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...makeDisAdvantage(),
      negativeAttribute: new fields.BooleanField({ initial: false }),
    }
  }
}

export class SpecialAbilityDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof SpecialAbilityDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...uniquelyIdentifiable(),
      type: new fields.StringField({
        initial: '',
        required: true,
        nullable: false,
      }),
    }
  }
}
