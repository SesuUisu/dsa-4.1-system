import { FormulaCategories } from '../../../model/item-data.js'
import {
  FormulaCastTimeClasses,
  FormulaDurationClasses,
  FormulaRangeClasses,
  FormulaStructureClasses,
  FormulaTargetClasses,
  FormulaWeightClasses,
} from '../../../model/myranor-magic.js'
import {
  baseItem,
  baseTalent,
  makeChoiceStringField,
  testable,
  uniquelyIdentifiable,
} from '../common.js'

const fields = foundry.data.fields

type Enumerate<
  N extends number,
  Acc extends number[] = [],
> = Acc['length'] extends N
  ? Acc[number]
  : Enumerate<N, [...Acc, Acc['length']]>

type IntRange<F extends number, T extends number> = Exclude<
  Enumerate<T>,
  Enumerate<F>
>

type QualityOptions = {
  initial: 0
  integer: true
  min: 0
  max: 7
  required: true
  nullable: false
}

export class FormulaDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof FormulaDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...baseTalent(),
      ...uniquelyIdentifiable(),
      ...testable(),
      instructionId: new fields.StringField({ initial: '', required: true }),
      sourceId: new fields.StringField({ initial: '', required: true }),
      parameters: new fields.SchemaField({
        castTime: makeChoiceStringField(FormulaCastTimeClasses),
        target: makeChoiceStringField(FormulaTargetClasses),
        range: makeChoiceStringField(FormulaRangeClasses),
        duration: makeChoiceStringField(FormulaDurationClasses),
        structure: makeChoiceStringField(FormulaStructureClasses),
      }),
      quality: new fields.NumberField<
        QualityOptions,
        IntRange<0, 7>,
        IntRange<0, 7>,
        IntRange<0, 7>
      >({
        initial: 0,
        integer: true,
        min: 0,
        max: 7,
        required: true,
        nullable: false,
      }),
      asp: new fields.NumberField({
        initial: 0,
        integer: true,
        min: 0,
        required: true,
        nullable: false,
      }),
      pasp: new fields.NumberField({
        initial: 0,
        integer: true,
        min: 0,
        required: true,
        nullable: false,
      }),
      additionalAsp: new fields.NumberField({
        initial: 0,
        integer: true,
        min: 0,
        required: true,
        nullable: false,
      }),
      weight: makeChoiceStringField(FormulaWeightClasses),
      category: makeChoiceStringField(FormulaCategories),
    }
  }
}
