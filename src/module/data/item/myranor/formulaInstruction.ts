import {
  FormulaDurationCategories,
  FormulaDurationClasses,
  FormulaTargetCategories,
  FormulaWeightClasses,
} from '../../../model/myranor-magic.js'
import {
  baseItem,
  makeChoiceStringField,
  uniquelyIdentifiable,
} from '../common.js'

const fields = foundry.data.fields

function makeStructureField() {
  return new fields.SchemaField({
    active: new fields.BooleanField({ initial: false }),
    name: new fields.StringField({ initial: '', required: true }),
  })
}

function makeCostField() {
  return new fields.SchemaField(
    {
      base: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      duration: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      durationBase: makeChoiceStringField(FormulaDurationClasses, false),
      weight: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      weightBase: makeChoiceStringField(FormulaWeightClasses, false),
      additional: new fields.StringField({ initial: '', required: true }),
      remarks: new fields.StringField({ initial: '', required: true }),
    },
    { required: false }
  )
}

export class FormulaInstructionDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof FormulaInstructionDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...uniquelyIdentifiable(),
      structures: new fields.SchemaField({
        extremeEasy: makeStructureField(),
        veryEasy: makeStructureField(),
        easy: makeStructureField(),
        difficult: makeStructureField(),
        veryDifficult: makeStructureField(),
        extremeDifficult: makeStructureField(),
        complex: makeStructureField(),
        veryComplex: makeStructureField(),
        extremeComplex: makeStructureField(),
      }),
      durations: new fields.SetField(
        makeChoiceStringField(FormulaDurationCategories)
      ),
      targetCategories: new fields.SetField(
        makeChoiceStringField(FormulaTargetCategories)
      ),
      costs: new fields.SchemaField({
        extremeEasy: makeCostField(),
        veryEasy: makeCostField(),
        easy: makeCostField(),
        difficult: makeCostField(),
        veryDifficult: makeCostField(),
        extremeDifficult: makeCostField(),
        complex: makeCostField(),
        veryComplex: makeCostField(),
        extremeComplex: makeCostField(),
      }),
    }
  }
}
