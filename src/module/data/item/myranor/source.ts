import { SphereTypes } from '../../../model/myranor-magic.js'
import {
  baseItem,
  makeChoiceStringField,
  uniquelyIdentifiable,
} from '../common.js'

const fields = foundry.data.fields

export class SourceDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof SourceDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...uniquelyIdentifiable(),
      value: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      sphere: makeChoiceStringField(SphereTypes),
    }
  }
}
