import {
  baseItem,
  castable,
  migrateTimeUnits,
  testable,
  uniquelyIdentifiable,
} from './common.js'

const fields = foundry.data.fields

export class SpellDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof SpellDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...uniquelyIdentifiable(),
      ...testable(),
      ...castable(),
      value: new fields.NumberField({
        initial: 0,
        integer: true,
        nullable: false,
        required: true,
      }),
      magicResistance: new fields.NumberField({
        initial: 0,
        integer: true,
        nullable: false,
        required: true,
      }),
      astralCost: new fields.StringField({ initial: '', required: true }),
      modifications: new fields.ArrayField(
        new fields.StringField({ initial: '', required: true })
      ),
      lcdPage: new fields.NumberField({
        initial: null,
        nullable: true,
        required: true,
      }),
      reversalis: new fields.StringField({ initial: '', required: true }),
      antimagic: new fields.StringField({ initial: '', required: true }),
      properties: new fields.ArrayField(
        new fields.StringField({ initial: '', required: true })
      ),
      complexity: new fields.StringField({ initial: '', required: true }),
      representation: new fields.StringField({ initial: '', required: true }),
      spread: new fields.StringField({ initial: '', required: true }),
      requiresUphold: new fields.BooleanField({ initial: false }),
      spellVariants: new fields.ArrayField(
        new fields.SchemaField({
          name: new fields.StringField({ initial: '', required: true }),
          description: new fields.StringField({ initial: '', required: true }),
          modificator: new fields.NumberField({
            initial: 0,
            nullable: false,
            required: true,
          }),
          minimalValue: new fields.NumberField({
            initial: 0,
            required: false,
            nullable: false,
          }),
          astralCost: new fields.StringField({ initial: '', required: false }),
        })
      ),
    }
  }

  static migrateData(source: object): object {
    if ('lcdPage' in source && typeof source.lcdPage === 'string') {
      source.lcdPage = parseInt(source.lcdPage)
      if (isNaN(source.lcdPage as number)) {
        source.lcdPage = null
      }
    }

    source = migrateTimeUnits(source)
    return super.migrateData(source)
  }
}
