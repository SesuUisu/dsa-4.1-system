import { baseItem, baseWeapon, buyable, physical } from './common.js'

const fields = foundry.data.fields

export class MeleeWeaponDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof MeleeWeaponDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...baseWeapon(),
      ...buyable(),
      ...physical(),
      length: new fields.StringField({ initial: '', required: true }),
      strengthMod: new fields.SchemaField({
        threshold: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        hitPointStep: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
      }),
      breakingFactor: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      initiativeMod: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      weaponMod: new fields.SchemaField({
        attack: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        parry: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
      }),
      distanceClass: new fields.StringField({ initial: '', required: true }),
      twoHanded: new fields.BooleanField({ initial: false }),
      improvised: new fields.BooleanField({ initial: false }),
      privileged: new fields.BooleanField({ initial: false }),
    }
  }
}
