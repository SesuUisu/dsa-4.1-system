import { baseItem, baseWeapon, buyable, physical } from './common.js'

const fields = foundry.data.fields

export class RangedWeaponDataModel extends foundry.abstract.TypeDataModel<
  ReturnType<(typeof RangedWeaponDataModel)['defineSchema']>,
  Item.ConfiguredInstance
> {
  static defineSchema() {
    return {
      ...baseItem(),
      ...baseWeapon(),
      ...buyable(),
      ...physical(),
      ranges: new fields.SchemaField({
        veryClose: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        close: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        medium: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        far: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        veryFar: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
      }),
      bonusDamages: new fields.SchemaField({
        veryClose: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        close: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        medium: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        far: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
        veryFar: new fields.NumberField({
          initial: 0,
          integer: true,
          required: true,
          nullable: false,
        }),
      }),
      loadtime: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      projectilePrice: new fields.NumberField({
        initial: 0,
        integer: true,
        required: true,
        nullable: false,
      }),
      loweredWoundThreshold: new fields.BooleanField({ initial: false }),
      improvised: new fields.BooleanField({ initial: false }),
      entangles: new fields.BooleanField({ initial: false }),
    }
  }
}
