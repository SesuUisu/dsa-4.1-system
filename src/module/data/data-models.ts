import { CharacterDataModel } from './actor/character.js'
import { ArmorDataModel } from './item/armor.js'
import { GenericItemDataModel } from './item/genericItem.js'
import { LiturgyDataModel } from './item/liturgy.js'
import { MeleeWeaponDataModel } from './item/meleeWeapon.js'
import { FormulaDataModel } from './item/myranor/formula.js'
import { FormulaInstructionDataModel } from './item/myranor/formulaInstruction.js'
import { SourceDataModel } from './item/myranor/source.js'
import { RangedWeaponDataModel } from './item/rangedWeapon.js'
import { ShieldDataModel } from './item/shield.js'
import { SpellDataModel } from './item/spell.js'
import {
  CombatTalentDataModel,
  LinguisticTalentDataModel,
  TalentDataModel,
} from './item/talent.js'
import {
  AdvantageDataModel,
  DisadvantageDataModel,
  SpecialAbilityDataModel,
} from './item/trait.js'

export const itemDataModels = {
  talent: TalentDataModel,
  language: LinguisticTalentDataModel,
  scripture: LinguisticTalentDataModel,
  combatTalent: CombatTalentDataModel,
  advantage: AdvantageDataModel,
  disadvantage: DisadvantageDataModel,
  specialAbility: SpecialAbilityDataModel,
  spell: SpellDataModel,
  liturgy: LiturgyDataModel,
  source: SourceDataModel,
  formulaInstruction: FormulaInstructionDataModel,
  formula: FormulaDataModel,
  meleeWeapon: MeleeWeaponDataModel,
  rangedWeapon: RangedWeaponDataModel,
  armor: ArmorDataModel,
  shield: ShieldDataModel,
  genericItem: GenericItemDataModel,
} as const

export function registerDataModels() {
  CONFIG.Actor.dataModels.character = CharacterDataModel

  Object.entries(itemDataModels).forEach(([type, model]) => {
    CONFIG.Item.dataModels[type] = model
  })
}
