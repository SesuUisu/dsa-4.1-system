import { AttributeName } from './character-data.js'
import { SpellVariantData, TimeData } from './item-data.js'
import {
  SpellComputationResult,
  SpellModification,
  SpellModificationCategory,
  SpellOptions,
  SpellTargetClass,
} from './magic.js'
import { SphereType } from './myranor-magic.js'

export interface GeneralNamedAttribute {
  value: number
  name: string
}

export interface NamedAttribute extends GeneralNamedAttribute {
  name: AttributeName
}

export type Rollable<Type> = Type & {
  roll(options?: any): void
}

export function isRollable<Type>(item: Type): item is Rollable<Type> {
  return (item as Rollable<Type>).roll !== undefined
}

export interface BaseProperty {
  name: string
  identifier: string
}

export type TestAttributes = [AttributeName, AttributeName, AttributeName?]

export type SkillType = 'talent' | 'spell' | 'liturgy' | 'source' | 'formula'
export type SkillDescriptor = BaseProperty & { skillType: SkillType }

export interface BaseSkill extends BaseProperty, SkillDescriptor {
  value: number
}

export type Testable<SkillType extends BaseSkill> = Rollable<SkillType> & {
  testAttributes: TestAttributes
}

type TalentType = 'normal' | 'combat' | 'language' | 'scripture'

type EffectiveEncumbaranceType = 'none' | 'special' | 'formula'
export interface EffectiveEncumbarance {
  type: EffectiveEncumbaranceType
  formula?: string
}

export interface BaseTalent extends BaseSkill {
  skillType: 'talent'
  talentType: TalentType
  effectiveEncumbarance: EffectiveEncumbarance
}

export type SpellModificationConfig = {
  count: number
}

export type SpellOutcomeOptions = {
  spontaneousModifications?: Map<SpellModification, SpellModificationConfig>
  spellVariants?: Map<string, SpellVariantData>
}

export interface Spell extends Testable<BaseSkill> {
  skillType: 'spell'
  modifications: SpellModificationCategory[]
  targetClasses: SpellTargetClass[]
  castTime: TimeData
  magicResistance: number
  astralCost: string
  range: string
  effectTime: string
  requiresUphold: boolean
  spellVariants: SpellVariantData[]
  spellOptions: SpellOptions
  spellOutcome(options: SpellOutcomeOptions): SpellComputationResult
}

export interface Talent extends Testable<BaseTalent> {
  talentType: 'normal'
}
export interface Liturgy extends Testable<BaseSkill> {
  skillType: 'liturgy'
  degree: string
}

export interface Source extends Testable<BaseSkill> {
  skillType: 'source'
  sphere: SphereType
}

export interface Formula extends Testable<BaseSkill> {
  skillType: 'formula'
  effectiveEncumbarance: EffectiveEncumbarance
  source: Source
}

export interface CombatTalent extends BaseTalent {
  talentType: 'combat'
  isUnarmed: boolean
  attack: number
  parry: number
  rangedAttack: number
  attackMod: number
  parryMod: number
  rangedAttackMod: number
}

export interface SpecialAbility extends BaseProperty {
  value?: string | number
}

export interface Advantage extends BaseProperty {
  value: number
}

export interface Disadvantage extends BaseProperty {
  negativeAttribute: boolean
  value: number
}

export const liturgyDegrees = [
  '0',
  'I',
  'II',
  'III',
  'IV',
  'V',
  'VI',
  'VII',
  'VIII',
  'IX',
  'X',
]
