import type {
  BaseProperty,
  BaseTalent,
  CombatTalent,
  NamedAttribute,
  Rollable,
  SpecialAbility,
  Spell,
  Liturgy,
  Talent,
  Source,
  Formula,
} from './properties.js'
import type {
  Armor,
  RollFormula,
  Shield,
  SizeClass,
  RangeClass,
  Weapon,
} from './items.js'
import type {
  ManeuverDescriptor,
  ManeuverModifier,
  ManeuverType,
  ModifierDescriptor,
} from './modifier.js'
import {
  AttributeName,
  CharacterData,
  WoundThresholds,
} from './character-data.js'
import {
  DataAccessor,
  DataAccessorsByType,
  PropertyItemType,
  TalentItemType,
  UpdateData,
  WeaponItemType,
} from './item-data.js'
import type { InitiativeFormula } from '../ruleset/rules/basic-combat.js'
import {
  ResourceRestingModifiers,
  RestingOptions,
} from '../ruleset/rules/basic-resting-computation.js'
import {
  RestingActionData,
  RestingActionParameters,
} from '../ruleset/rules/basic-resting-action.js'

export interface BaseCombatOptions {
  modifiers?: Map<string, ModifierDescriptor>
  maneuvers?: ManeuverDescriptor[]
}

type Armed<CombatOptions extends BaseCombatOptions> = CombatOptions & {
  weapon: Weapon
}

type Unarmed<CombatOptions extends BaseCombatOptions> = CombatOptions & {
  talent: CombatTalent
}

export interface ArmedMeleeCombatOptions extends Armed<BaseCombatOptions> {
  shield?: Shield
  secondaryWeapon?: Weapon
  useSecondaryHand?: boolean
}

export type MeleeCombatOptions =
  | ArmedMeleeCombatOptions
  | Unarmed<BaseCombatOptions>

export interface RangedCombatOptions extends Armed<BaseCombatOptions> {
  sizeClass?: SizeClass
  rangeClass?: RangeClass
}

export interface CombatState {
  isArmed: boolean
  primaryHand: DataAccessor<WeaponItemType> | undefined
  secondaryHand: DataAccessor<WeaponItemType | 'shield'> | undefined
  unarmedTalent: DataAccessor<'combatTalent'> | undefined
}

export interface CharacterDataAccessor {
  name: string | null
  system: CharacterData

  update(data: UpdateData<CharacterData>): Promise<this | undefined>

  baseAttack: number
  baseParry: number
  baseRangedAttack: number
  baseInitiative: number
  dodge: number
  bonusDamage: number

  talent(
    sid: string
  ):
    | DataAccessor<'talent' | 'language' | 'scripture' | 'combatTalent'>
    | undefined
  spell(sid: string): DataAccessor<'spell'> | undefined
  source(sid: string): DataAccessor<'source'> | undefined
  formula(sid: string): DataAccessor<'formula'> | undefined
  liturgy(sid: string): DataAccessor<'liturgy'> | undefined
  specialAbility(sid: string): DataAccessor<'specialAbility'> | undefined
  advantage(sid: string): DataAccessor<'advantage'> | undefined
  disadvantage(sid: string): DataAccessor<'disadvantage'> | undefined

  talents: DataAccessor<TalentItemType>[]
  spells: DataAccessor<'spell'>[]
  sources: DataAccessor<'source'>[]
  formulas: DataAccessor<'formula'>[]
  liturgies: DataAccessor<'liturgy'>[]
  properties: DataAccessor<PropertyItemType>[]
  specialAbilities: DataAccessor<'specialAbility'>[]
  advantages: DataAccessor<'advantage'>[]
  disadvantages: DataAccessor<'disadvantage'>[]

  weapons: DataAccessor<WeaponItemType>[]
  shields: DataAccessor<'shield'>[]
  armorItems: DataAccessor<'armor'>[]

  itemTypes: DataAccessorsByType

  combatState: CombatState

  addWound(count?: number): Promise<void>
  removeWound(): Promise<void>
  countWounds(): number
}

export interface DamageConsequences {
  damage: number
  wounds: number
}

export interface BaseCharacter {
  data: CharacterDataAccessor

  attribute(name: AttributeName): Rollable<NamedAttribute>

  baseAttack: number
  baseParry: number
  baseRangedAttack: number
  baseDodge: number
  attackValue(useSecondaryHand?: boolean): number
  attackValue(options?: MeleeCombatOptions): number
  parryValue(useSecondaryHand?: boolean): number
  parryValue(options?: MeleeCombatOptions): number
  rangedAttackValue(options?: RangedCombatOptions): number
  rangedAttackModifiers(
    options?: RangedCombatOptions | Record<string, unknown>
  ): Map<string, ModifierDescriptor>
  rangedAttackModifier(
    options?: RangedCombatOptions | Record<string, unknown>
  ): number
  rangedAttackDuration(
    options?: RangedCombatOptions | Record<string, unknown>
  ): number
  dodgeValue(options?: BaseCombatOptions): number
  attackModifiers(useSecondaryHand?: boolean): Map<string, ModifierDescriptor>
  attackModifiers(options?: MeleeCombatOptions): Map<string, ModifierDescriptor>
  parryModifiers(useSecondaryHand?: boolean): Map<string, ModifierDescriptor>
  parryModifiers(options?: MeleeCombatOptions): Map<string, ModifierDescriptor>
  rangedAttackModifiers(
    options?: RangedCombatOptions
  ): Map<string, ModifierDescriptor>
  sourceModifiers(formula: Formula): Map<string, ModifierDescriptor>
  damage(useSecondaryHand?: boolean): RollFormula
  damage(options?: BaseCombatOptions): RollFormula
  initiative(): InitiativeFormula
  has(property: BaseProperty): boolean
  attack(options?: BaseCombatOptions): void
  rangedAttack(options?: BaseCombatOptions): void
  parry(options?: BaseCombatOptions): void
  dodge(options?: BaseCombatOptions): void
  rollDamage(options?: BaseCombatOptions): void
  maneuverList(
    maneuverType: ManeuverType,
    armed?: boolean
  ): ManeuverDescriptor[]
  maneuverModifiers: ManeuverModifier[]
  armorClass: number
  encumbarance: number
  woundThresholds: WoundThresholds
  determineDamage(hitPoints: number): DamageConsequences
  takeDamage(damageConsequences: DamageConsequences): void
  applyDamage(damage: number, wounds: number): void
  determineResting(options: RestingOptions): RestingActionParameters
  rest(options: RestingActionData): void
  applyResting(vitality: number, astralEnergy?: number): void

  talent(sid: string): BaseTalent | undefined
  spell(sid: string): Spell | undefined
  source(sid: string): Source | undefined
  formula(sid: string): Formula | undefined
  liturgy(sid: string): Liturgy | undefined

  armorItems: Armor[]
  specialAbilities: SpecialAbility[]
  properties: BaseProperty[]
  baseInitiative: number
  effectiveEncumbarance(formula: string): number
  degreeModifier(degree: string): number
  karmaTalent(): Talent | undefined

  astralEnergyRestingModifier(
    modifiers: ResourceRestingModifiers
  ): ResourceRestingModifiers
  vitalityRestingModifier(
    modifiers: ResourceRestingModifiers
  ): ResourceRestingModifiers
}
