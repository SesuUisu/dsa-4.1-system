import type { BaseCharacter } from './character.js'

export type Func<OptionsData, ResultType> = (options: OptionsData) => ResultType
export type PreHook<OptionData = any> = (options: OptionData) => OptionData
export type PostHook<OptionData = any, ResultType = any> = (
  options: OptionData,
  result: ResultType
) => ResultType

export type RuleComponentType = 'computation' | 'action' | 'effect'
export interface RuleComponentIdentifier<
  Type extends RuleComponentType = RuleComponentType,
> {
  name: string
  type: Type
}

export type BaseOptionData<Type extends RuleComponentType> =
  Type extends 'computation'
    ? BaseComputationOptionData
    : Type extends 'action'
    ? BaseActionOptionData
    : Type extends 'effect'
    ? BaseEffectOptionData<any>
    : never

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface AmendedRuleComponentIdentifier<
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  OptionData extends BaseOptionData<Type>,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  ResultType,
  Type extends RuleComponentType,
> extends RuleComponentIdentifier<Type> {}

export type BaseComputationOptionData = Record<string, unknown> & {
  character: BaseCharacter
  mod?: number
}

export interface ComputationIdentifier<
  OptionData extends BaseComputationOptionData,
  ResultType,
> extends AmendedRuleComponentIdentifier<
    OptionData,
    ResultType,
    'computation'
  > {
  type: 'computation'
}

export type BaseActionResultType<OptionData> = {
  options: OptionData
}

export type BaseActionOptionData = {
  action?: RuleComponentIdentifier
}

export interface ActionIdentifier<
  OptionData extends BaseActionOptionData,
  ResultType extends BaseActionResultType<OptionData>,
> extends AmendedRuleComponentIdentifier<OptionData, ResultType, 'action'> {
  type: 'action'
}

export type BaseEffectOptionData<ListenedResultType> = {
  result: ListenedResultType
}

export type ListenedResultTypeOf<OptionData> =
  OptionData extends BaseEffectOptionData<infer ListenedResultType>
    ? ListenedResultType
    : never

export interface EffectIdentifier<ListenedResultType>
  extends AmendedRuleComponentIdentifier<
    BaseEffectOptionData<ListenedResultType>,
    Promise<boolean>,
    'effect'
  > {
  type: 'effect'
}
