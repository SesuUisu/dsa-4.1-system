import type { BaseComputationOptionData } from '../ruleset.js'
import type { CombatTalent } from '../properties.js'
import type { RangeClass, SizeClass, Shield, Weapon } from '../items.js'
import type { ManeuverDescriptor, ModifierDescriptor } from '../modifier.js'

export interface CombatComputationData extends BaseComputationOptionData {
  talent?: CombatTalent
  weapon?: Weapon
  shield?: Shield
  modifiers?: Map<string, ModifierDescriptor>
  maneuvers?: ManeuverDescriptor[]
}

export interface CombatComputationResult extends Record<string, unknown> {
  value: number
  mod?: number
  modifiers: Map<string, ModifierDescriptor>
}

export interface CombatDamageComputationData extends CombatComputationData {
  bonusDamage?: number
  damageMultiplier?: number
}

export interface RangedCombatOptionData {
  rangeClass?: RangeClass
  sizeClass?: SizeClass
}

export interface RangedCombatComputationData
  extends CombatComputationData,
    RangedCombatOptionData {
  mod?: number
}

export interface DamageFormula {
  baseDamage: string
  multiplier: number
  bonusDamage: number
  formula: string
}
