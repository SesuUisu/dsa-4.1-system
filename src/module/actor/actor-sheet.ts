import { RangeI18nMap, SizeI18nMap, labeledValues } from '../i18n.js'
import { createOpenPDFPageListener } from '../pdf-integration.js'
import { RuleBook } from '../enums.js'

import { DsaActor } from './actor.js'
import { Character } from '../character/character.js'
import { getGame, getLocalizer, getSettings } from '../utils.js'
import type { ManeuverDescriptor } from '../model/modifier.js'
import {
  RangeClass,
  RangeClasses,
  SizeClass,
  SizeClasses,
} from '../model/items.js'
import { Skill, testAttributesBySphere } from '../character/skill.js'
import {
  DataAccessor,
  SkillItemType,
  SourceData,
  WeaponItemType,
} from '../model/item-data.js'
import { BaseSkill, Testable } from '../model/properties.js'
import { ActorSettingsSheet } from './actor-settings-sheet.js'
import { CharacterDataAccessor } from '../model/character.js'
import Talent from '../../ui/actor-sheet/tabs/Talent.svelte'
import { getSystemSetting } from '../settings.js'
import { rollDialog } from '../../ui/Dialog.svelte'
import RangedCombatRoll from '../../ui/roll-dialog/RangedCombatRoll.svelte'
import MeleeCombatRoll from '../../ui/roll-dialog/MeleeCombatRoll.svelte'
import SkillRoll from '../../ui/roll-dialog/SkillRoll.svelte'

interface RangedAttackConfig {
  modifier: number
  rangeClass?: RangeClass
  sizeClass?: SizeClass
}

const SheetItemTypes = [
  'talent',
  'combatTalent',
  'language',
  'scripture',
  'spell',
  'spellVariant',
  'liturgy',
  'advantage',
  'disadvantage',
  'specialAbility',
  'meleeWeapon',
  'rangedWeapon',
  'armor',
  'shield',
  'genericItem',
] as const

const MyranorSheetItemTypes = [...SheetItemTypes, 'source'] as const

function appendTestAttributesToSources(sources: SourceData[]): SourceData[] {
  return sources.map((source) => ({
    ...source,
    testAttributes: testAttributesBySphere[source.system.sphere],
  }))
}

export class DsaActorSheet extends ActorSheet {
  private rangedAttackConfig: RangedAttackConfig | undefined

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      classes: ['dsa-41', 'sheet', 'actor'],
      template: 'systems/dsa-41/templates/actor-sheet.html',
      width: 950,
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'character',
        },
        {
          navSelector: '.talents-tabs',
          contentSelector: '.talents-body',
          initial: 'talents-combat',
        },
        {
          navSelector: '.inventory-tabs',
          contentSelector: '.inventory-body',
          initial: 'inventory-generic-item',
        },
      ],
    })
  }

  async getData(): Promise<any> {
    await this.actor.tempItemPromise
    const data: any = await super.getData()

    const isMyranor = getSystemSetting('myranor')
    const itemTypes = isMyranor ? MyranorSheetItemTypes : SheetItemTypes

    data.owned = {}

    itemTypes.forEach((type) => {
      data.owned[`${type}s`] = data.items.filter(
        (item: any) => item.type === type
      )
    })

    this.appendDerivedItemData(data, isMyranor)

    const character = new Character(this.actor as CharacterDataAccessor)

    data.armorClass = character.armorClass
    data.encumbarance = character.encumbarance

    data.itemsById = {}
    data.items.forEach((item: any) => {
      data.itemsById[item._id] = item
    })

    this.appendRangedCombatConfigData(data)

    return data
  }

  private appendRangedCombatConfigData(data: any) {
    data.useBasicRangedCombat = !getSettings().get(
      'dsa-41',
      'enhanced-ranged-combat-rule'
    )
    data.rangeClasses = labeledValues(
      RangeI18nMap,
      RangeClasses,
      getLocalizer()
    )
    data.sizeClasses = labeledValues(SizeI18nMap, SizeClasses, getLocalizer())
    data.rangedAttackConfig = this.rangedAttackConfig
      ? this.rangedAttackConfig
      : {
          modifier: 0,
          rangeClass: 'close',
          sizeClass: 'big',
        }
  }

  private appendDerivedItemData(data: any, isMyranor: boolean) {
    data.owned.allMeleeWeapons = data.owned.meleeWeapons
    data.owned.meleeWeapons = data.owned.meleeWeapons.filter(
      (weapon: DataAccessor<WeaponItemType>) => this.actor.isEquipable(weapon)
    )

    data.owned.unarmedTalents = data.owned.combatTalents.filter(
      (talent: Talent) => talent.system.combat.category === 'unarmed'
    )

    data.owned.allRangedWeapons = data.owned.rangedWeapons
    data.owned.rangedWeapons = data.owned.rangedWeapons.filter(
      (weapon: DataAccessor<WeaponItemType>) => this.actor.isEquipable(weapon)
    )

    const character = new Character(this.actor as CharacterDataAccessor)

    data.attack = character.attackValue()
    data.parry = character.parryValue()
    data.initiative = character.initiative().formula
    data.damage = character.damage()
    data.rangedAttack = character.rangedAttackValue()

    data.secondaryAttack = character.attackValue(true)
    data.secondaryParry = character.parryValue(true)
    data.secondaryDamage = character.damage(true)

    data.armorClass = character.armorClass
    data.encumbarance = character.encumbarance

    data.itemsById = {}
    data.items.forEach((item: Item) => {
      data.itemsById[item._id] = item
    })

    data.isArmed = data.data.system.base.combatState.isArmed

    if (isMyranor) {
      data.owned.sources = appendTestAttributesToSources(data.owned.sources)
    }

    return data
  }

  /** @override */
  async _updateObject(
    event: Event,
    formData: object
  ): Promise<undefined | DsaActor> {
    await super._updateObject(event, formData)

    let items = expandObject(formData).owned || []
    items = Object.entries(items).map(([id, data]) => ({
      _id: id,
      system: (data as any).system,
    }))
    await this.actor.updateEmbeddedDocuments('Item', items)

    return
  }

  _switchArmedEventListener(_event: Event) {
    const combatModeData = _event.currentTarget?.dataset?.combatMode
    const isArmed = combatModeData
      ? combatModeData === 'armed'
      : !this.actor.system.base.combatState.isArmed

    if (isArmed !== this.actor.system.base.combatState.isArmed) {
      this.actor.update({
        system: { base: { combatState: { isArmed } } },
      })
      this.render(true)
    }
  }

  activateListeners(html: JQuery<HTMLElement>) {
    super.activateListeners(html)

    html
      .find('a.combat-mode')
      .on('click', (e: Event) => this._switchArmedEventListener(e))

    html
      .find('.attribute-actions .item-roll')
      .click(createAttributeRollEventListener(this.actor, false))

    html
      .find('.attribute-label a')
      .click(createAttributeRollEventListener(this.actor, false))

    html
      .find('.trait-label a')
      .click(createAttributeRollEventListener(this.actor, true))

    html
      .find('a.combat-attribute-action:not([data-action=damage])')
      .click(createCombatActionRollEventListener(this.actor))

    html
      .find('a.combat-attribute-action[data-action=damage]')
      .on('click', createDamageRollEventListener(this.actor))

    html.find('.talent-label a').click(createSkillRollEventListener(this.actor))
    html.find('.roll-spell').click(createSkillRollEventListener(this.actor))
    html.find('.roll-source').click(createSkillRollEventListener(this.actor))
    html.find('.roll-liturgy').click(createSkillRollEventListener(this.actor))

    html
      .find('.open-lcd-page')
      .click(createOpenPDFPageListener(RuleBook.LiberCantionesDeluxe))

    html
      .find('.open-ll-page')
      .click(createOpenPDFPageListener(RuleBook.LiberLiturgium))

    html
      .find('.talent-label a')
      .contextmenu(createRenderItemSheetEventListener(this.actor))
    html
      .find('.spell-label a')
      .contextmenu(createRenderItemSheetEventListener(this.actor))
    html
      .find('.liturgy-label a')
      .contextmenu(createRenderItemSheetEventListener(this.actor))
    html
      .find('.item-edit')
      .click(createRenderItemSheetEventListener(this.actor))

    html.find('.trait .mode').click(createCollapseEventListener('trait'))
    html
      .find('.special-ability-label .mode')
      .click(createCollapseEventListener('special-ability'))

    html.find('select.range_class, select.size_class').on('change', (event) => {
      const weaponId = $(event.currentTarget)
        .parents('.weapon')
        ?.data('attribute')
      const weapon = this.actor.items.get(weaponId)
      const configElement = $(event.currentTarget).parents('.weapon')
      const options = {
        rangeClass: configElement
          .find('select.range_class')
          .val() as RangeClass,
        sizeClass: configElement.find('select.size_class').val() as SizeClass,
      }
      const modificator = this.actor.rangedAttackModifier(weapon, options)
      this.rangedAttackConfig = {
        modifier: modificator || 0,
        rangeClass: options.rangeClass,
        sizeClass: options.sizeClass,
      }
      this.render()
    })

    html
      .find('.effect-edit')
      .click(createRenderActiveEffectConfigEventListener(this.actor))

    html
      .find('.effect-add')
      .click(createAddActiveEffectEventListener(this.actor))

    html
      .find('.effect-disable')
      .click(createDisableActiveEffectEventListener(this.actor))

    html
      .find('.effect-delete')
      .click(createDeleteActiveEffectEventListener(this.actor))
    html.find('.item-add').click(createAddItemEventListener(this.actor))
    html.find('.item-delete').click(createDeleteItemEventListener(this.actor))
    html.find('.item-name').click(createCollapseEventListener('item'))
  }

  _getHeaderButtons() {
    const buttons = super._getHeaderButtons()
    const game = getGame()

    if (
      game.user?.isGM ||
      (this.document.isOwner && game.user?.can('TOKEN_CONFIGURE'))
    ) {
      buttons.unshift({
        class: 'actor-settings',
        icon: 'fas fa-cog',
        label: getGame().i18n.localize('DSA.actorSettings'),
        onclick: (ev) => {
          if (ev) {
            ev.preventDefault()
          }
          new ActorSettingsSheet(this.document).render(true)
        },
      })
    }

    return buttons
  }

  async _onDropItem(event: DragEvent, data: object) {
    if (!this.actor.isOwner) return false
    const item = await Item.implementation.fromDropData(data)
    const itemData = item.toObject()

    // Handle item sorting within the same Actor
    if (this.actor.uuid === item.parent?.uuid)
      return this._onSortItem(event, itemData)
    else if (item.parent?.isOwner) {
      // previousOwner.deleteEmbeddedDocuments('Item', [item.uuid])
      item.delete()
    }

    // Create the owned item
    return this._onDropItemCreate(itemData)
  }
}

function createCollapseEventListener(type: string) {
  return (event: Event) => {
    const typeElement = $(event.currentTarget)
      .parents(`.${type}`)
      .find(`.${type}-description`)

    const description = typeElement
    description.toggleClass('collapsed')

    if (description.hasClass('collapsed')) {
      description.slideUp()
    } else {
      description.slideDown()
    }
  }
}

function createRenderItemSheetEventListener(actor: DsaActor) {
  return (event: Event) => {
    const item = actor.items.get(event.currentTarget?.name)
    item?.sheet?.render(true)
  }
}

abstract class RollEventListener {
  protected _actor: DsaActor

  constructor(actor: DsaActor) {
    this._actor = actor
  }

  _rollTarget(event: Event) {
    return event.currentTarget?.name
  }

  abstract _rollMethod(target, isFastForward, options, event?)

  abstract _getOptions(action, event)

  async _renderDialog() {
    const template = 'systems/dsa-41/templates/roll-dialog.html'
    const dialogData = {}

    return renderTemplate(template, dialogData)
  }

  async _showDialog(name) {
    const html = this._renderDialog()

    return new Promise((resolve) => {
      const game = getGame()
      html.then((html) =>
        new Dialog({
          title: game.i18n.localize(`DSA.${name}`),
          content: html,
          buttons: {
            normal: {
              label: 'Roll',
              callback: (html) =>
                resolve(
                  parseInt(html[0].querySelector('form').modificator.value)
                ),
            },
          },
          default: 'normal',
          close: () => resolve(0),
        }).render(true)
      )
    })
  }

  handleEvent(event: Event) {
    const target = this._rollTarget(event)
    let options = this._getOptions(target, event) || {}
    const localize = getLocalizer()
    const isArmed = this._actor.system.base.combatState.isArmed
    const maneuverType = {
      attack: 'offensive',
      parry: 'defensive',
      rangedAttack: '',
    }[target]
    const character = new Character(this._actor)

    const dialogOptions = {
      character,
      callback: (modifiers, combatOptions) => {
        options.modifiers = modifiers
        options = { ...options, ...combatOptions }
        this._rollMethod(target, true, options, event)
      },
    } as Record<string, unknown>

    if (maneuverType === 'offensive' || maneuverType === 'defensive') {
      const lastManeuver = this._actor.getFlag(
        'dsa-41',
        `last-${maneuverType}-maneuver`
      ) as ManeuverDescriptor[]
      dialogOptions.availableManeuvers = character.maneuverList(
        maneuverType,
        isArmed
      )
      dialogOptions.lastManeuver = lastManeuver
      const actionModifierCalculator = {
        attack: character.attackModifiers.bind(character),
        parry: character.parryModifiers.bind(character),
      }[target]
      dialogOptions.actionModifierCalculator = actionModifierCalculator
    }

    let component: any
    if (target === 'rangedAttack') {
      component = RangedCombatRoll
    } else {
      component = MeleeCombatRoll
    }

    rollDialog(localize(target), component, dialogOptions, localize)
  }

  _isFastForward(event: KeyboardEvent) {
    return (
      event &&
      (event.shiftKey || event.altKey || event.ctrlKey || event.metaKey)
    )
  }
}

class AttributeRollEventListener extends RollEventListener {
  private _isNegative: boolean

  constructor(actor: DsaActor, isNegative: boolean) {
    super(actor)
    this._isNegative = isNegative
  }

  _getOptions() {
    return
  }

  _rollMethod(target, isFastForward, options) {
    this._actor.rollAttribute(target, isFastForward, this._isNegative, options)
  }
}

function createAttributeRollEventListener(actor, isNegative) {
  return (event) => {
    const eventListener = new AttributeRollEventListener(actor, isNegative)
    eventListener.handleEvent(event)
  }
}

export class SkillRollEventListener extends RollEventListener {
  _rollMethod(target, _isFastForward, options) {
    this._actor.rollSkill(target, options)
  }

  _getOptions() {
    return undefined
  }

  handleEvent(event) {
    const target = this._rollTarget(event)

    const item = this._actor.items.find((i) => i.id === target)
    const options = this._getOptions(target, event) || ({} as any)
    const character = new Character(this._actor)
    const skill = item
      ? (Skill.create(
          item as DataAccessor<SkillItemType>,
          character
        ) as Testable<BaseSkill>)
      : undefined
    if (skill) {
      const localize = getLocalizer()
      const dialogOptions = {
        character,
        skill,
        callback: (modifiers, skillOptions) => {
          this._rollMethod(
            target,
            true,
            { ...options, ...skillOptions, modifiers },
            event
          )
        },
      } as Record<string, unknown>
      rollDialog(localize('skillRoll'), SkillRoll, dialogOptions, localize)
    }
  }
}

function createSkillRollEventListener(actor) {
  return (event) => {
    const eventListener = new SkillRollEventListener(actor)
    eventListener.handleEvent(event)
  }
}

class CombatRollEventListener extends RollEventListener {
  getWeaponOrTalent(event) {
    const itemId = $(event.currentTarget)
      .parents('.weapon, .talent')
      ?.data('attribute')
    return this._actor.items.get(itemId)
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _getOptions(action, event) {
    return
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _rollMethod(target, isFastForward, options, event) {
    return
  }
}

class DamageRollEventListener extends CombatRollEventListener {
  _rollMethod(target, isFastForward, options, event) {
    const weaponOrTalent = this.getWeaponOrTalent(event)
    this._actor.rollDamage(weaponOrTalent, isFastForward, options)
  }
}

function createDamageRollEventListener(actor) {
  return (event) => {
    const eventListener = new DamageRollEventListener(actor)
    eventListener.handleEvent(event)
  }
}

class CombatActionRollEventListener extends CombatRollEventListener {
  _getOptions(action, event) {
    if (action == 'rangedAttack') {
      const configElement = $(event.currentTarget).parents('.weapon')
      return {
        rangeClass: configElement.find('select.range_class').val(),
        sizeClass: configElement.find('select.size_class').val(),
      }
    }
  }

  _rollMethod(action, isFastForward, options, event) {
    const weaponOrTalent = this.getWeaponOrTalent(event)
    // const actionDict = {
    //   attack: AttackAction,
    //   parry: ParryAction,
    //   rangedAttack: RangedAttackAction,
    //   dodge: DodgeAction,
    // }
    this._actor.rollCombatAction(action, weaponOrTalent, options)
  }
}

function createCombatActionRollEventListener(actor) {
  return (event) => {
    const eventListener = new CombatActionRollEventListener(actor)
    eventListener.handleEvent(event)
  }
}

function createRenderActiveEffectConfigEventListener(actor) {
  return (event) => {
    const effect = actor.effects.get(event.currentTarget.name)
    effect.sheet.render(true)
  }
}

function createAddActiveEffectEventListener(actor) {
  return async () => {
    const effectData = {
      label: 'New Effect',
      icon: 'icons/svg/blood.svg',
    }

    await actor.createEmbeddedDocuments('ActiveEffect', [effectData], {
      renderSheet: true,
    })
  }
}

function createDisableActiveEffectEventListener(actor) {
  return async (event) => {
    const effect = actor.effects.get(event.currentTarget.name)
    const updateData = {
      disabled: !effect.disabled,
    }
    await effect.update(updateData)
  }
}

function createDeleteActiveEffectEventListener(actor) {
  return async (event) => {
    await actor.deleteEmbeddedDocuments('ActiveEffect', [
      event.currentTarget.name,
    ])
  }
}

function createAddItemEventListener(actor) {
  return async (event) => {
    const itemData = {
      name: getGame().i18n.localize('DSA.newItem'),
      img: 'icons/svg/item-bag.svg',
      type: $(event.currentTarget).data('itemType'),
    }

    await actor.createEmbeddedDocuments('Item', [itemData], {
      renderSheet: true,
    })
  }
}

function createDeleteItemEventListener(actor) {
  return async (event) => {
    await actor.deleteEmbeddedDocuments('Item', [event.currentTarget.name])
  }
}
