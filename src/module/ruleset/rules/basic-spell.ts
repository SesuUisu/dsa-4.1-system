import type { Ruleset } from '../ruleset.js'
import { DescribeRule } from '../rule.js'
import { generateModificationMapFromTable } from './modification-generator.js'
import type { ModifierDescriptor, ModifierTable } from '../../model/modifier.js'
import {
  SpellComputationResult,
  SpellModification,
  SpellModificationCategories,
  SpellModificationsByCategory,
  SpellOptions,
  SpellTargetClass,
} from '../../model/magic.js'
import { SpellAction } from './basic-skill.js'
import { BaseComputationOptionData } from '../../model/ruleset.js'
import { CreateComputationIdentifier } from '../rule-components.js'
import {
  SkillDescriptor,
  SpellModificationConfig,
} from '../../model/properties.js'
import { BaseCharacter } from '../../model/character.js'
import { SpellVariantData } from '../../model/item-data.js'

const SpellPointModifiers: ModifierTable<SpellModification> = {
  changeTechnique: 7,
  changeCentralTechnique: 12,
  halfCastTime: 5,
  doubleCastTime: -3,
  forceEffect: 1,
  reduceCost: 3,
  unvoluntarily: 5,
  voluntarily: 2,
  multipleCompanions: 3,
  multipleEnemies: 0,
  increaseRange: 5,
  reduceRange: 3,
  doubleDuration: 7,
  halfDuration: 3,
  changeToFixedDuration: 7,
}

const ApplySpontaneousModifications = generateModificationMapFromTable(
  SpellPointModifiers,
  'spontaneousModifications'
)

interface ComputeSpellOptionsData extends BaseComputationOptionData {
  skill: SkillDescriptor
  character: BaseCharacter
}

interface ComputeSpellData extends BaseComputationOptionData {
  skill: SkillDescriptor
  character: BaseCharacter
  spontaneousModifications?: Map<SpellModification, SpellModificationConfig>
  spellVariants?: Map<string, SpellVariantData>
  astralCost?: string
}

export const ComputeSpell = CreateComputationIdentifier<
  ComputeSpellData,
  SpellComputationResult
>('spell')

export const ComputeSpellOptions = CreateComputationIdentifier<
  ComputeSpellOptionsData,
  SpellOptions
>('spell-options')

function PrepareSpellOptions(options: ComputeSpellOptionsData) {
  return {
    options,
    spontaneousModifications: [],
  }
}

function ApplyVariantsToSpellOptions(
  options: ComputeSpellData
): ComputeSpellData {
  options.spellVariants?.forEach((variant) => {
    if (variant.astralCost !== undefined) {
      options.astralCost = variant.astralCost
    }
  })
  return options
}

function PrepareSpellResult(options: ComputeSpellData) {
  const spell = options.character.spell(options.skill.identifier)
  return {
    options,
    modifiers: new Map<string, ModifierDescriptor>(),
    targetClasses: [...(spell?.targetClasses ?? [])],
    castTime: { ...spell?.castTime },
    magicResistance: spell?.magicResistance ?? 0,
    astralCost: options.astralCost ?? spell?.astralCost ?? '',
    range: {
      value: spell?.range ?? '',
      stepModifier: 0,
    },
    effectTime: {
      value: spell?.effectTime ?? '',
      multiplier: 0,
    },
    requiresUphold: spell?.requiresUphold ?? false,
  }
}

function hasPersonOrCreature(targetClasses: SpellTargetClass[]): boolean {
  return targetClasses.includes('person') || targetClasses.includes('creature')
}

function DeterminePossibleSpontaneousModifications(
  options: ComputeSpellOptionsData,
  result: SpellOptions
): SpellOptions {
  const spell = options.character.spell(options.skill.identifier)
  let spontaneousModifications: SpellModification[] = [
    ...result.spontaneousModifications,
    ...SpellModificationsByCategory['general'],
  ]
  SpellModificationCategories.forEach((category) => {
    if (
      spell?.modifications.includes(category) &&
      category !== 'multipleTargets'
    ) {
      spontaneousModifications = [
        ...spontaneousModifications,
        ...SpellModificationsByCategory[category],
      ]
    }
  })
  const targetClasses = spell?.targetClasses ?? []
  if (spell?.modifications.includes('multipleTargets')) {
    if (hasPersonOrCreature(targetClasses)) {
      if (targetClasses.includes('voluntarily')) {
        spontaneousModifications.push('multipleCompanions')
      } else {
        spontaneousModifications.push('multipleEnemies')
      }
    }
  }
  if (hasPersonOrCreature(targetClasses)) {
    if (targetClasses.includes('voluntarily')) {
      spontaneousModifications.push('unvoluntarily')
    } else {
      spontaneousModifications.push('voluntarily')
    }
  }

  return {
    ...result,
    spontaneousModifications,
  }
}

function DeterminePossibleVariants(
  options: ComputeSpellOptionsData,
  result: SpellOptions
): SpellOptions {
  const spell = options.character.spell(options.skill.identifier)
  const spellVariants = (spell?.spellVariants ?? []).filter(
    (variant) =>
      variant.minimalValue === undefined ||
      (spell?.value ?? 0) >= variant.minimalValue
  )

  return {
    ...result,
    spellVariants,
  }
}

function ApplyModificationsToTargetClasses(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const targetClasses = result?.targetClasses ?? []
  const spontaneousModifications =
    options.spontaneousModifications ??
    new Map<SpellModification, SpellModificationConfig>()
  if (hasPersonOrCreature(targetClasses)) {
    const singleTarget =
      targetClasses.find(
        (targetClass) => targetClass === 'person' || targetClass === 'creature'
      ) ?? 'person'
    const multiTarget =
      singleTarget === 'person' ? 'multiplePersons' : 'multipleCreatures'

    if (targetClasses.includes('voluntarily')) {
      if ((spontaneousModifications.get('unvoluntarily')?.count ?? 0) > 0) {
        targetClasses.splice(targetClasses.indexOf('voluntarily'), 1)
      }
      if (
        (spontaneousModifications.get('multipleCompanions')?.count ?? 0) > 0
      ) {
        targetClasses.push(multiTarget)
        targetClasses.splice(targetClasses.indexOf(singleTarget), 1)
      }
    } else {
      if ((spontaneousModifications.get('voluntarily')?.count ?? 0) > 0) {
        targetClasses.push('voluntarily')
      }
      if ((spontaneousModifications.get('multipleEnemies')?.count ?? 0) > 0) {
        targetClasses.push(multiTarget)
        targetClasses.splice(targetClasses.indexOf(singleTarget), 1)
      }
    }
  }
  return {
    ...result,
    targetClasses,
  }
}

function ApplyModificationsToCastTime(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const castTime = result.castTime
  const spontaneousModifications =
    options.spontaneousModifications ??
    new Map<SpellModification, SpellModificationConfig>()

  const halfCount = spontaneousModifications.get('halfCastTime')?.count ?? 0
  if (castTime.unit === 'action') {
    for (let i = 0; i < halfCount; i++) {
      castTime.duration = Math.max(1, Math.ceil(castTime.duration / 2))
    }
  } else {
    castTime.duration = castTime.duration / Math.pow(2, halfCount)
  }

  const doubleCount = spontaneousModifications.get('doubleCastTime')?.count ?? 0
  castTime.duration = castTime.duration * Math.pow(2, doubleCount)

  const CastTimeModfiers: ModifierTable<SpellModification> = {
    changeTechnique: 3,
    changeCentralTechnique: 3,
    halfCastTime: 0,
    doubleCastTime: 0,
    forceEffect: 1,
    reduceCost: 1,
    unvoluntarily: 1,
    voluntarily: 1,
    multipleCompanions: 1,
    multipleEnemies: 1,
    increaseRange: 1,
    reduceRange: 1,
    doubleDuration: 1,
    halfDuration: 1,
    changeToFixedDuration: 1,
  }

  spontaneousModifications.forEach((modification, key) => {
    const mod = CastTimeModfiers[key]
    castTime.additionalCastTime =
      castTime.additionalCastTime === undefined
        ? {
            duration: 0,
            unit: 'action',
            info: '',
          }
        : castTime.additionalCastTime

    if (mod > 0) {
      castTime.additionalCastTime.duration += mod * modification.count
    }
  })

  return {
    ...result,
    castTime,
  }
}

function ApplyModificationsToAstralCost(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const spontaneousModifications =
    options.spontaneousModifications ??
    new Map<SpellModification, SpellModificationConfig>()
  let astralCost = result.astralCost
  const forceEffectCount =
    spontaneousModifications.get('forceEffect')?.count ?? 0
  if (forceEffectCount > 0) {
    astralCost += ` + ${Math.pow(2, forceEffectCount - 1)} AsP`
  }
  const reduceCostCount = spontaneousModifications.get('reduceCost')?.count ?? 0
  if (reduceCostCount > 0) {
    astralCost += ` - ${reduceCostCount * 10}% AsP`
  }

  return {
    ...result,
    astralCost,
  }
}

function ApplyModificationsToRange(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const spontaneousModifications =
    options.spontaneousModifications ??
    new Map<SpellModification, SpellModificationConfig>()
  const range = result.range
  const increaseRangeCount =
    spontaneousModifications.get('increaseRange')?.count ?? 0

  const reduceRangeCount =
    spontaneousModifications.get('reduceRange')?.count ?? 0

  range.stepModifier = increaseRangeCount - reduceRangeCount

  return {
    ...result,
    range,
  }
}

function ApplyModificationsToEffectTime(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const spontaneousModifications =
    options.spontaneousModifications ??
    new Map<SpellModification, SpellModificationConfig>()
  const effectTime = result.effectTime
  const doubleDurationCount =
    spontaneousModifications.get('doubleDuration')?.count ?? 0
  const halfDurationCount =
    spontaneousModifications.get('halfDuration')?.count ?? 0

  effectTime.multiplier = Math.pow(2, doubleDurationCount - halfDurationCount)

  if ((spontaneousModifications.get('changeToFixedDuration')?.count ?? 0) > 0) {
    result.requiresUphold = false
  }

  return {
    ...result,
    effectTime,
  }
}

function ApplyModificationsToMagicResistanceModifier(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const spell = options.character.spell(options.skill.identifier)
  result.magicResistance = spell?.magicResistance ?? 0
  const magicResistanceModifiers: Partial<
    ModifierTable<SpellModification, number | 'max'>
  > = {
    unvoluntarily: 1,
    voluntarily: -0.5,
    multipleEnemies: 'max',
  }
  const spontaneousModifications =
    options.spontaneousModifications ??
    new Map<SpellModification, SpellModificationConfig>()
  spontaneousModifications.forEach((modification, key) => {
    if (magicResistanceModifiers[key] !== undefined) {
      const mod = magicResistanceModifiers[key]
      if (
        modification.count > 0 &&
        (mod === 'max' || result.magicResistance === 'max')
      ) {
        result.magicResistance = 'max'
      } else if (
        mod !== undefined &&
        mod !== 'max' &&
        result.magicResistance !== 'max'
      ) {
        result.magicResistance += mod * modification.count
      }
    }
  })

  return result
}

function ApplyVariants(
  options: ComputeSpellData,
  result: SpellComputationResult
): SpellComputationResult {
  const spellVariants =
    options.spellVariants ?? new Map<string, SpellVariantData>()
  spellVariants.forEach((variant) =>
    result.modifiers.set(variant.name, {
      name: variant.name,
      mod: variant.modificator,
      modifierType: 'other',
      class: 'variant',
      nameIsLocalized: true,
    })
  )
  return result
}

export const BasicSpellRule = DescribeRule(
  'basic-spell-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.on(ComputeSpellOptions).do(PrepareSpellOptions)
    ruleset
      .after(ComputeSpellOptions)
      .do(DeterminePossibleSpontaneousModifications)
    ruleset.after(ComputeSpellOptions).do(DeterminePossibleVariants)

    ruleset.on(ComputeSpell).do(PrepareSpellResult)

    ruleset.after(ComputeSpell).do(ApplySpontaneousModifications)
    ruleset.after(ComputeSpell).do(ApplyVariants)
    ruleset.after(ComputeSpell).do(ApplyModificationsToTargetClasses)
    ruleset.after(ComputeSpell).do(ApplyModificationsToCastTime)
    ruleset.after(ComputeSpell).do(ApplyModificationsToAstralCost)
    ruleset.after(ComputeSpell).do(ApplyModificationsToRange)
    ruleset.after(ComputeSpell).do(ApplyModificationsToEffectTime)
    ruleset.after(ComputeSpell).do(ApplyModificationsToMagicResistanceModifier)
    ruleset.before(ComputeSpell).do(ApplyVariantsToSpellOptions)

    ruleset.before(SpellAction).do(ApplySpontaneousModifications)
  }
)
