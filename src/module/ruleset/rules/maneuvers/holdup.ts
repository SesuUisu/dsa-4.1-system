import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Holdup: SpecialAbility = {
  name: 'Gegenhalten',
  identifier: 'ability-gegenhalten',
}

export const HoldupManeuver = new Maneuver('holdup', 'defensive')
export const HoldupRule = createManeuverRule(Holdup, HoldupManeuver)
