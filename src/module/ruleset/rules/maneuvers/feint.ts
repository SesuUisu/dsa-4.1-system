import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Feint: SpecialAbility = {
  name: 'Finte',
  identifier: 'ability-finte',
}

export const FeintManeuver = new Maneuver('feint', 'offensive')
export const FeintRule = createManeuverRule(Feint, FeintManeuver, false)
