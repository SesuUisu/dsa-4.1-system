import { Maneuver } from '../../../character/maneuver.js'
import type { SpecialAbility } from '../../../model/properties.js'

import { createManeuverRule } from './maneuver-creator.js'

export const Clearance: SpecialAbility = {
  name: 'Befreiungsschlag',
  identifier: 'ability-befreiungsschlag',
}

export const ClearanceManeuver = new Maneuver('clearance', 'offensive', {
  minMod: 4,
})
export const ClearanceRule = createManeuverRule(Clearance, ClearanceManeuver)
