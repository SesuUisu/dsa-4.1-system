import type { BaseProperty } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import { ComputeAstralRestingModifier } from '../basic-resting-computation.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'

export const AstralRegeneration: BaseProperty = {
  name: 'Astrale Regeneration',
  identifier: 'advantage-astrale-regeneration',
}
export const AstralRegenerationRule = DescribeRule(
  'astral-regeneration',
  {
    changeable: true,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset
      .after(ComputeAstralRestingModifier)
      .when(CharacterHas(AstralRegeneration))
      .do((options, result) => {
        const regenerationBonus =
          options.character.data.advantage('advantage-astrale-regeneration')
            ?.system.value ?? 1
        result.modifier += regenerationBonus
        result.bonusModifier -= regenerationBonus
        return result
      })
  }
)
