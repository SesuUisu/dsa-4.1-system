import { DescribeRule } from '../rule.js'
import type { Ruleset } from '../ruleset.js'
import { ComputeAttack, ComputeParry } from './derived-combat-attributes.js'

export const BasicShieldCombatRule = DescribeRule(
  'basic-shield-combat-rule',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ruleset.after(ComputeAttack).do((options, result) => {
      if (options.shield) {
        result.value += options.shield.weaponMod.attack
      }
      return result
    })

    ruleset.after(ComputeParry).do((options, result) => {
      if (options.shield) {
        result.value =
          options.character.baseParry + options.shield.weaponMod.parry
      }
      return result
    })
  }
)
