import type { SpecialAbility } from '../../../model/properties.js'
import { DescribeRule } from '../../rule.js'
import { Ruleset } from '../../ruleset.js'
import { CharacterHas } from '../maneuvers/basic-maneuver.js'
import { Attribute } from '../../../enums.js'
import { ComputeAstralRestingModifier } from '../basic-resting-computation.js'

export const RegenerationI: SpecialAbility = {
  name: 'Regneration I',
  identifier: 'ability-regeneration-i',
}
export const RegenerationII: SpecialAbility = {
  name: 'Regneration II',
  identifier: 'ability-regeneration-ii',
}
export const MasterRegeneration: SpecialAbility = {
  name: 'Meisterliche Regeneration',
  identifier: 'ability-meisterliche-regeneration',
}
export const RegenerationRule = DescribeRule(
  'regneration',
  {
    changeable: false,
    enabled: true,
  },
  (ruleset: Ruleset): void => {
    ;[RegenerationI, RegenerationII, MasterRegeneration].forEach(
      (specialAbility: SpecialAbility) => {
        ruleset
          .after(ComputeAstralRestingModifier)
          .when(CharacterHas(specialAbility))
          .do((options, result) => {
            result.modifier++
            result.bonusModifier--
            return result
          })
      }
    )

    ruleset
      .after(ComputeAstralRestingModifier)
      .when(CharacterHas(MasterRegeneration))
      .do((options, result) => {
        result.modifier += Math.round(
          options.character.attribute(Attribute.Intuition).value / 3
        )
        result.roll = false
        return result
      })
  }
)
