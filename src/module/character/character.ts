import {
  BaseProperty,
  BaseTalent,
  CombatTalent,
  NamedAttribute,
  Rollable,
  SpecialAbility,
  Spell,
  Source,
  Liturgy,
  Talent,
  Advantage,
  Disadvantage,
  BaseSkill,
  Formula,
} from '../model/properties.js'
import type { Armor, RollFormula, Shield, Weapon } from '../model/items.js'
import type {
  ArmedMeleeCombatOptions,
  BaseCharacter,
  BaseCombatOptions,
  CharacterDataAccessor,
  DamageConsequences,
  MeleeCombatOptions,
  RangedCombatOptions,
} from '../model/character.js'
import type { Ruleset } from '../ruleset/ruleset.js'
import {
  ComputeArmorClass,
  ComputeAttack,
  ComputeDamageFormula,
  ComputeDodge,
  ComputeEffectiveEncumbarance,
  ComputeEncumbarance,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes.js'

import {
  AttackAction,
  CombatActionData,
  CombatActionResult,
  ComputeDamageResult,
  ComputeInitiativeFormula,
  DamageAction,
  DodgeAction,
  InitiativeFormula,
  ParryAction,
  TakeDamageAction,
} from '../ruleset/rules/basic-combat.js'

import { ComputeDegreeModifier } from '../ruleset/rules/basic-karma.js'
import { RangedAttackAction } from '../ruleset/rules/basic-ranged-combat.js'
import { ComputeManeuverList } from '../ruleset/rules/maneuvers/basic-maneuver.js'
import type {
  ManeuverDescriptor,
  ManeuverModifier,
  ManeuverType,
  ModifierDescriptor,
} from '../model/modifier.js'
import { getGame } from '../utils.js'
import { ComputeRangedAttackDuration } from '../ruleset/rules/enhanced-ranged-combat.js'

import {
  GenericLiturgy,
  GenericSource,
  GenericFormula,
  GenericSpell,
  isBaseTalent,
  isTalent,
  Skill,
} from './skill.js'
import { GenericWeapon } from './weapon.js'
import { GenericShield } from './shield.js'
import { DataAccessor, WeaponItemType } from '../model/item-data.js'
import { AttributeName, WoundThresholds } from '../model/character-data.js'
import { Attribute } from './attribute.js'
import { ActionIdentifier, ComputationIdentifier } from '../model/ruleset.js'
import {
  CombatComputationData,
  CombatComputationResult,
} from '../model/rules/derived-combat-attributes.js'
import {
  ComputeMyranorFormulaModifier,
  ComputeMyranorSourceModifier,
} from '../ruleset/rules/myranor/formula.js'
import {
  ResourceRestingModifiers,
  ComputeRestingParameters,
  RestingOptions,
  ComputeVitalityRestingModifier,
  ComputeAstralRestingModifier,
} from '../ruleset/rules/basic-resting-computation.js'
import {
  RestingAction,
  RestingActionData,
  RestingActionParameters,
} from '../ruleset/rules/basic-resting-action.js'

type CombatIdentifier =
  | 'attack'
  | 'parry'
  | 'ranged'
  | 'rangedDuration'
  | 'dodge'
  | 'damage'

function actionIdentifierByName(
  action: CombatIdentifier
): ActionIdentifier<CombatActionData, CombatActionResult> {
  return {
    attack: AttackAction,
    parry: ParryAction,
    ranged: RangedAttackAction,
    dodge: DodgeAction,
    damage: DamageAction,
  }[action]
}

function computationIdentifierByName(
  action: CombatIdentifier
):
  | ComputationIdentifier<CombatComputationData, CombatComputationResult>
  | undefined {
  return {
    attack: ComputeAttack,
    parry: ComputeParry,
    ranged: ComputeRangedAttack,
    rangedDuration: ComputeRangedAttackDuration,
    dodge: ComputeDodge,
    damage: undefined,
  }[action]
}

export class Character implements BaseCharacter {
  private ruleset: Ruleset
  data: CharacterDataAccessor

  constructor(data: CharacterDataAccessor, ruleset?: Ruleset) {
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
    this.data = data
  }

  attribute(name: AttributeName): Rollable<NamedAttribute> {
    return new Attribute(this, name, this.ruleset)
  }

  get baseAttack(): number {
    return this.data.system.base.combatAttributes.active.baseAttack.value
  }

  get baseParry(): number {
    return this.data.system.base.combatAttributes.active.baseParry.value
  }

  get baseRangedAttack(): number {
    return this.data.system.base.combatAttributes.active.baseRangedAttack.value
  }

  get baseDodge(): number {
    return this.data.system.base.combatAttributes.active.dodge.value
  }

  get woundThresholds(): WoundThresholds {
    const woundThresholdsWithMod =
      this.data.system.base.combatAttributes.passive.woundThresholds
    const calcWoundThreshold = (name: keyof WoundThresholds): number => {
      return woundThresholdsWithMod[name] + woundThresholdsWithMod.mod
    }
    return {
      first: calcWoundThreshold('first'),
      second: calcWoundThreshold('second'),
      third: calcWoundThreshold('third'),
    }
  }

  determineDamage(hitPoints: number): DamageConsequences {
    const result = this.ruleset.compute(ComputeDamageResult, {
      hitPoints,
      character: this,
    })
    return {
      damage: result.damage,
      wounds: result.wounds,
    }
  }

  takeDamage(damageConsequences: DamageConsequences): void {
    this.ruleset.execute(TakeDamageAction, {
      character: this,
      ...damageConsequences,
    })
  }

  determineResting(options: RestingOptions): RestingActionParameters {
    return this.ruleset.compute(ComputeRestingParameters, {
      character: this,
      options,
    })
  }

  rest(options: RestingActionData): void {
    this.ruleset.execute(RestingAction, {
      character: this,
      options,
    })
  }

  applyResting(vitality: number, astralEnergy?: number): void {
    const update = {
      system: {
        base: {
          resources: {
            endurance: {
              value: this.data.system.base.resources.endurance.max,
            },
          },
        },
      },
    }

    if (vitality > 0) {
      const vitalityCurrent =
        this.data.system.base.resources.vitality.value + vitality
      const vitalityMax = this.data.system.base.resources.vitality.max
      update.system.base.resources.vitality = {
        value: Math.min(vitalityCurrent, vitalityMax),
      }
    }

    if (astralEnergy !== undefined && astralEnergy > 0) {
      const astralEnergyCurrent =
        this.data.system.base.resources.astralEnergy.value + astralEnergy
      const astralEnergyMax = this.data.system.base.resources.astralEnergy.max
      update.system.base.resources.astralEnergy = {
        value: Math.min(astralEnergyCurrent, astralEnergyMax),
      }
    }

    this.data.update(update)
  }

  applyDamage(damage: number, wounds: number): void {
    this.data.addWound(wounds)
    const update = {
      system: {
        base: {
          resources: {
            vitality: {
              value: this.data.system.base.resources.vitality.value - damage,
            },
          },
        },
      },
    }
    this.data.update(update)
  }

  private get combatOptionsFromState(): Partial<
    MeleeCombatOptions | RangedCombatOptions
  > {
    let options: {
      weapon?: Weapon
      shield?: Shield
      secondaryWeapon?: Weapon
      talent?: BaseSkill
    } = {}
    const combatState = this.data.combatState
    if (combatState.isArmed) {
      if (
        combatState.primaryHand !== undefined &&
        ['meleeWeapon', 'rangedWeapon'].includes(combatState.primaryHand.type)
      ) {
        options = {
          weapon: GenericWeapon.create(combatState.primaryHand),
        }
        if (combatState.secondaryHand !== undefined) {
          if (combatState.secondaryHand?.type === 'shield') {
            options.shield = new GenericShield(
              combatState.secondaryHand as DataAccessor<'shield'>
            )
          }
          if (
            ['meleeWeapon', 'rangedWeapon'].includes(
              combatState.secondaryHand.type
            )
          ) {
            options.secondaryWeapon = GenericWeapon.create(
              combatState.secondaryHand as DataAccessor<WeaponItemType>
            )
          }
        }
      }
    } else if (combatState.unarmedTalent) {
      options = {
        talent: Skill.create(combatState.unarmedTalent, this),
      }
    }
    return options
  }

  private prepareCombatOptions(
    param?: BaseCombatOptions | boolean
  ): Partial<MeleeCombatOptions | RangedCombatOptions> {
    let options: Partial<MeleeCombatOptions | RangedCombatOptions>
    let useSecondaryHand = false
    if (param === undefined || typeof param === 'boolean') {
      useSecondaryHand = typeof param === 'boolean' ? param : false
      options = this.combatOptionsFromState
    } else {
      options = {
        ...this.combatOptionsFromState,
        ...param,
      }
    }

    const isArmedMelee = function (
      opt: Partial<MeleeCombatOptions>
    ): opt is ArmedMeleeCombatOptions {
      return (opt as ArmedMeleeCombatOptions)?.weapon !== undefined
    }

    if (
      isArmedMelee(options) &&
      options.secondaryWeapon &&
      (options.useSecondaryHand || useSecondaryHand)
    ) {
      options.weapon = options.secondaryWeapon
    }
    return options
  }

  private computeCombatParameters(
    name: CombatIdentifier,
    param?: BaseCombatOptions | boolean
  ): CombatComputationResult | undefined {
    const options = this.prepareCombatOptions(param)

    const computationIdentifier = computationIdentifierByName(name)
    if (computationIdentifier === undefined) return

    return this.ruleset.compute(computationIdentifier, {
      character: this,
      ...options,
    })
  }

  private computeCombatValue(
    name: CombatIdentifier,
    param?: BaseCombatOptions | boolean
  ): number {
    return this.computeCombatParameters(name, param)?.value || 0
  }

  private computeCombatModifiers(
    name: CombatIdentifier,
    param?: BaseCombatOptions | boolean
  ): Map<string, ModifierDescriptor> {
    return (
      this.computeCombatParameters(name, param)?.modifiers ||
      new Map<string, ModifierDescriptor>()
    )
  }

  attackValue(useSecondaryHand?: boolean): number
  attackValue(options?: MeleeCombatOptions): number
  attackValue(param?: MeleeCombatOptions | boolean): number {
    return this.computeCombatValue('attack', param)
  }

  attackModifiers(useSecondaryHand?: boolean): Map<string, ModifierDescriptor>
  attackModifiers(options?: MeleeCombatOptions): Map<string, ModifierDescriptor>
  attackModifiers(
    param?: MeleeCombatOptions | boolean
  ): Map<string, ModifierDescriptor> {
    return this.computeCombatModifiers('attack', param)
  }

  parryValue(useSecondaryHand?: boolean): number
  parryValue(options?: MeleeCombatOptions): number
  parryValue(options?: MeleeCombatOptions | boolean): number {
    return this.computeCombatValue('parry', options)
  }

  parryModifiers(useSecondaryHand?: boolean): Map<string, ModifierDescriptor>
  parryModifiers(options?: MeleeCombatOptions): Map<string, ModifierDescriptor>
  parryModifiers(
    param?: MeleeCombatOptions | boolean
  ): Map<string, ModifierDescriptor> {
    return this.computeCombatModifiers('parry', param)
  }

  rangedAttackValue(options?: RangedCombatOptions): number {
    return this.computeCombatValue('ranged', options)
  }

  rangedAttackModifier(
    options?: RangedCombatOptions | Record<string, unknown>
  ): number {
    return this.computeCombatParameters('ranged', options)?.mod || 0
  }

  rangedAttackDuration(
    options?: RangedCombatOptions | Record<string, unknown>
  ): number {
    return this.computeCombatParameters('rangedDuration', options)?.value || 0
  }

  rangedAttackModifiers(
    options?: RangedCombatOptions
  ): Map<string, ModifierDescriptor> {
    return this.computeCombatModifiers('ranged', options)
  }

  sourceModifiers(formula: Formula): Map<string, ModifierDescriptor> {
    return this.ruleset.compute(ComputeMyranorSourceModifier, {
      character: this,
      formula,
    })
  }

  dodgeValue(options?: MeleeCombatOptions): number {
    return this.computeCombatValue('dodge', options)
  }

  get encumbarance(): number {
    return this.ruleset.compute(ComputeEncumbarance, {
      character: this,
    })
  }

  effectiveEncumbarance(formula: string): number {
    return this.ruleset.compute(ComputeEffectiveEncumbarance, {
      character: this,
      formula,
    })
  }

  degreeModifier(degree: string): number {
    return this.ruleset.compute(ComputeDegreeModifier, {
      character: this,
      degree,
    })
  }

  astralEnergyRestingModifier(
    modifiers: ResourceRestingModifiers
  ): ResourceRestingModifiers {
    return this.ruleset.compute(ComputeAstralRestingModifier, {
      character: this,
      modifiers: modifiers,
    })
  }

  vitalityRestingModifier(
    modifiers: ResourceRestingModifiers
  ): ResourceRestingModifiers {
    return this.ruleset.compute(ComputeVitalityRestingModifier, {
      character: this,
      modifiers: modifiers,
    })
  }

  formulaModifier(formula: GenericFormula): Map<string, ModifierDescriptor> {
    return this.ruleset.compute(ComputeMyranorFormulaModifier, {
      character: this,
      formula,
    })
  }

  get armorClass(): number {
    return this.ruleset.compute(ComputeArmorClass, {
      character: this,
    })
  }

  damage(useSecondaryHand?: boolean): RollFormula
  damage(options?: MeleeCombatOptions | RangedCombatOptions): RollFormula
  damage(
    param?: MeleeCombatOptions | RangedCombatOptions | boolean
  ): RollFormula {
    const options = this.prepareCombatOptions(param)

    return this.ruleset.compute(ComputeDamageFormula, {
      character: this,
      ...options,
    }).formula
  }

  get baseInitiative(): number {
    return this.data.baseInitiative
  }

  initiative(): InitiativeFormula {
    const combatState = this.data.combatState
    const unarmedTalent = combatState.isArmed
      ? undefined
      : combatState.unarmedTalent
      ? (Skill.create(combatState.unarmedTalent, this) as CombatTalent)
      : undefined
    const weapon =
      combatState.isArmed && combatState.primaryHand
        ? GenericWeapon.create(combatState.primaryHand)
        : undefined
    const shield =
      combatState.isArmed &&
      combatState.secondaryHand &&
      combatState.secondaryHand.type === 'shield'
        ? new GenericShield(combatState.secondaryHand as DataAccessor<'shield'>)
        : undefined
    return this.ruleset.compute(ComputeInitiativeFormula, {
      character: this,
      talent: unarmedTalent,
      weapon,
      shield,
    })
  }

  has(property: BaseProperty): boolean {
    return this.data.properties.some(
      (p) => p.system.sid === property.identifier
    )
  }

  private baseCombatAction(
    action: CombatIdentifier,
    options?: BaseCombatOptions
  ): void {
    const actionIdentifier = actionIdentifierByName(action)

    this.ruleset.execute(actionIdentifier, {
      character: this,
      ...this.prepareCombatOptions(options),
    })
  }

  attack(options?: BaseCombatOptions): void {
    this.baseCombatAction('attack', options)
  }

  rangedAttack(options?: BaseCombatOptions): void {
    this.baseCombatAction('ranged', options)
  }

  parry(options?: BaseCombatOptions): void {
    this.baseCombatAction('parry', options)
  }

  dodge(options?: BaseCombatOptions): void {
    this.baseCombatAction('dodge', options)
  }

  rollDamage(options?: BaseCombatOptions): void {
    this.baseCombatAction('damage', options)
  }

  maneuverList(
    maneuverType: ManeuverType,
    isArmed?: boolean
  ): ManeuverDescriptor[] {
    return this.ruleset.compute(ComputeManeuverList, {
      character: this,
      maneuverType,
      isArmed,
    }).maneuvers
  }

  get maneuverModifiers(): ManeuverModifier[] {
    return this.data.system.modifiers.maneuverModifiers.map((modifier) => ({
      ...modifier,
      value: parseInt(`${modifier.value}`),
    }))
  }

  talent(sid: string): BaseTalent | undefined {
    const talentData = this.data.talent(sid)
    const skill = talentData
      ? Skill.create(talentData, this, this.ruleset)
      : undefined
    return skill !== undefined && isBaseTalent(skill) ? skill : undefined
  }

  spell(sid: string): Spell | undefined {
    const spellData = this.data.spell(sid)
    return spellData !== undefined
      ? new GenericSpell(spellData, this, this.ruleset)
      : undefined
  }

  source(sid: string): Source | undefined {
    const sourceData = this.data.source(sid)
    return sourceData !== undefined
      ? new GenericSource(sourceData, this, this.ruleset)
      : undefined
  }

  formula(sid: string): Formula | undefined {
    const formulaData = this.data.formula(sid)
    return formulaData !== undefined
      ? new GenericFormula(formulaData, this, this.ruleset)
      : undefined
  }

  liturgy(sid: string): Liturgy | undefined {
    const liturgyData = this.data.liturgy(sid)
    return liturgyData !== undefined
      ? new GenericLiturgy(liturgyData, this, this.ruleset)
      : undefined
  }

  karmaTalent(): Talent | undefined {
    let karmaTalent = this.talent('talent-liturgiekenntnis')
    if (karmaTalent !== undefined && isTalent(karmaTalent)) {
      return karmaTalent
    }
    const karmaTalentData = this.data.talents.find(
      (talent) => talent.system.category === 'karma'
    )
    if (karmaTalentData !== undefined) {
      karmaTalent = Skill.create(
        karmaTalentData,
        this,
        this.ruleset
      ) as BaseTalent
      if (karmaTalent !== undefined && isTalent(karmaTalent)) {
        return karmaTalent
      }
    }
    return undefined
  }

  get armorItems(): Armor[] {
    return this.data.armorItems.map((item) => ({
      name: item.name || '',
      armorClass: item.system.armorClass,
      encumbarance: item.system.encumbarance,
    }))
  }

  get specialAbilities(): SpecialAbility[] {
    return this.data.specialAbilities.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
    }))
  }

  get advantages(): Advantage[] {
    return this.data.advantages.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
      value: item.system.value,
    }))
  }

  get disadvantages(): Disadvantage[] {
    return this.data.disadvantages.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
      value: item.system.value,
      negativeAttribute: item.system.negativeAttribute,
    }))
  }

  get properties(): BaseProperty[] {
    return this.data.properties.map((item) => ({
      name: item.name || '',
      identifier: item.system.sid,
    }))
  }
}
