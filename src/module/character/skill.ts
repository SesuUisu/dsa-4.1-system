import type { BaseCharacter } from '../model/character.js'
import type {
  BaseSkill,
  BaseTalent,
  CombatTalent,
  EffectiveEncumbarance,
  Liturgy,
  Rollable,
  SkillDescriptor,
  SkillType,
  Source,
  Formula,
  Spell,
  Talent,
  Testable,
  TestAttributes,
  SpellOutcomeOptions,
} from '../model/properties.js'
import type {
  ActionIdentifier,
  ComputationIdentifier,
} from '../model/ruleset.js'
import type {
  SkillActionData,
  SkillActionResult,
} from '../ruleset/rules/basic-skill.js'
import {
  LiturgyAction,
  SourceAction,
  FormulaAction,
  SpellAction,
  TalentAction,
} from '../ruleset/rules/basic-skill.js'
import type { Ruleset } from '../ruleset/ruleset.js'
import { getGame } from '../utils.js'
import {
  ComputeAttack,
  ComputeParry,
  ComputeRangedAttack,
} from '../ruleset/rules/derived-combat-attributes.js'
import type {
  DataAccessor,
  RollablSkillItemType,
  SkillItemType,
  SpellVariantData,
  TestableSkillItemType,
  TestableTalentItemType,
  TimeData,
} from '../model/item-data.js'
import { Property } from './property.js'
import { Attribute } from '../enums.js'
import {
  CombatComputationData,
  CombatComputationResult,
} from '../model/rules/derived-combat-attributes.js'
import { QualityDegrees, SphereType } from '../model/myranor-magic.js'
import {
  SpellComputationResult,
  SpellModificationCategory,
  SpellOptions,
  SpellTargetClass,
} from '../model/magic.js'
import {
  ComputeSpell,
  ComputeSpellOptions,
} from '../ruleset/rules/basic-spell.js'

interface SkillRollOptions {
  mod?: number
}

type OptionalValue<ItemData> = ItemData & {
  value?: number
}

type OptionalDataValue<T extends { system: any }> = T & {
  system: OptionalValue<T['system']>
}

export abstract class Skill<ItemType extends SkillItemType>
  extends Property<ItemType>
  implements BaseSkill
{
  public declare item: OptionalDataValue<DataAccessor<ItemType>>
  protected declare character: BaseCharacter
  public abstract skillType: SkillType

  get value(): number {
    return this.item.system.value || 0
  }

  static create(
    item: DataAccessor<SkillItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ): BaseSkill | undefined {
    let skill: BaseSkill
    switch (item.type) {
      case 'spell':
        skill = new GenericSpell(
          item as DataAccessor<'spell'>,
          character,
          ruleset
        )
        break
      case 'talent':
      case 'language':
      case 'scripture':
        skill = new GenericTalent(
          item as DataAccessor<TestableTalentItemType>,
          character,
          ruleset
        )
        break
      case 'combatTalent':
        skill = new GenericCombatTalent(
          item as DataAccessor<'combatTalent'>,
          character,
          ruleset
        )
        break
      case 'liturgy':
        skill = new GenericLiturgy(
          item as DataAccessor<'liturgy'>,
          character,
          ruleset
        )
        break
      case 'source':
        skill = new GenericSource(
          item as DataAccessor<'source'>,
          character,
          ruleset
        )
        break
      case 'formula':
        skill = new GenericFormula(
          item as DataAccessor<'formula'>,
          character,
          ruleset
        )
        break
    }
    return skill
  }
}

export abstract class RollableSkill<ItemType extends RollablSkillItemType>
  extends Skill<ItemType>
  implements Testable<BaseSkill>
{
  protected abstract skillAction: ActionIdentifier<
    SkillActionData,
    SkillActionResult
  >
  protected ruleset: Ruleset

  constructor(
    item: DataAccessor<ItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character)

    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
  }

  public abstract readonly testAttributes: TestAttributes

  private get skillDescriptor(): SkillDescriptor {
    return {
      name: this.name,
      identifier: this.identifier,
      skillType: this.skillType,
    }
  }

  roll(options: SkillRollOptions): void {
    this.ruleset.execute(this.skillAction, {
      character: this.character,
      ...options,
      skill: this.skillDescriptor,
    })
  }

  static create(
    item: DataAccessor<RollablSkillItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ): Rollable<BaseSkill> | undefined {
    return Skill.create(item, character, ruleset) as Rollable<BaseSkill>
  }
}

export abstract class TestableSkill<ItemType extends TestableSkillItemType>
  extends RollableSkill<ItemType>
  implements Testable<BaseSkill>
{
  get testAttributes(): TestAttributes {
    return [
      this.item.system.test.firstAttribute,
      this.item.system.test.secondAttribute,
      this.item.system.test.thirdAttribute,
    ]
  }

  static create(
    item: DataAccessor<TestableSkillItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ): Testable<BaseSkill> | undefined {
    return Skill.create(item, character, ruleset) as Testable<BaseSkill>
  }
}

export function isBaseTalent(skill: BaseSkill): skill is BaseTalent {
  return skill.skillType === 'talent'
}

export function isTalent(skill: BaseSkill): skill is Talent {
  return isBaseTalent(skill) && skill.talentType === 'normal'
}

export function isLiturgy(skill: BaseSkill): skill is Liturgy {
  return skill.skillType === 'liturgy'
}

export function isSource(skill: BaseSkill): skill is Source {
  return skill.skillType === 'source'
}

export function isFormula(skill: BaseSkill): skill is Formula {
  return skill.skillType === 'formula'
}

export class GenericTalent
  extends TestableSkill<TestableTalentItemType>
  implements Talent
{
  public skillType = 'talent' as const
  public talentType = 'normal' as const

  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    TalentAction

  constructor(
    item: DataAccessor<TestableTalentItemType>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return this.item.system.effectiveEncumbarance
  }
}

export class GenericSpell extends TestableSkill<'spell'> implements Spell {
  public skillType = 'spell' as const
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    SpellAction

  constructor(
    item: DataAccessor<'spell'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
  }

  get modifications(): SpellModificationCategory[] {
    return this.item.system.modifications as SpellModificationCategory[]
  }

  get targetClasses(): SpellTargetClass[] {
    return this.item.system.targetClasses as SpellTargetClass[]
  }

  get castTime(): TimeData {
    return this.item.system.castTime
  }

  get magicResistance(): number {
    return this.item.system.magicResistance
  }

  get astralCost(): string {
    return this.item.system.astralCost
  }

  get range(): string {
    return this.item.system.range
  }

  get effectTime(): string {
    return this.item.system.effectTime
  }

  get requiresUphold(): boolean {
    return this.item.system.requiresUphold
  }

  get spellVariants(): SpellVariantData[] {
    return this.item.system.spellVariants
  }

  get spellOptions(): SpellOptions {
    return this.ruleset.compute(ComputeSpellOptions, {
      character: this.character,
      skill: this,
    })
  }

  spellOutcome(options: SpellOutcomeOptions): SpellComputationResult {
    return this.ruleset.compute(ComputeSpell, {
      character: this.character,
      skill: this,
      ...options,
    })
  }
}

export const testAttributesBySphere: Record<SphereType, TestAttributes> = {
  death: [Attribute.Courage, Attribute.Courage, Attribute.Charisma],
  demonic: [Attribute.Courage, Attribute.Courage, Attribute.Charisma],
  elemental: [Attribute.Courage, Attribute.Cleverness, Attribute.Charisma],
  nature: [Attribute.Courage, Attribute.Cleverness, Attribute.Intuition],
  stellar: [Attribute.Cleverness, Attribute.Intuition, Attribute.Charisma],
  time: [Attribute.Courage, Attribute.Cleverness, Attribute.Intuition],
}

export class GenericSource extends RollableSkill<'source'> implements Source {
  public skillType = 'source' as const

  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    SourceAction

  constructor(
    item: DataAccessor<'source'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
  }

  get sphere(): SphereType {
    return this.item.system.sphere
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return {
      type: 'formula',
      formula: 'BE-2',
    }
  }

  get testAttributes(): TestAttributes {
    return (
      testAttributesBySphere[this.sphere] || [
        'courage',
        'intuition',
        'charisma',
      ]
    )
  }
}

export class GenericFormula
  extends RollableSkill<'formula'>
  implements Formula
{
  public skillType = 'formula' as const
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    FormulaAction

  constructor(
    item: DataAccessor<'formula'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
  }

  get description(): string {
    return this.item.system.description
  }

  get quality(): QualityDegrees {
    return this.item.system.quality
  }

  get instructionId(): string {
    return this.item.system.instructionId || ''
  }

  get source(): Source | undefined {
    return this.character.source(this.item.system.sourceId)
  }

  get sourceId(): string {
    return this.item.system.sourceId
  }

  get testAttributes(): TestAttributes {
    return this.source?.testAttributes || ['courage', 'intuition', 'charisma']
  }

  get value(): number {
    return this.character.source(this.item.system.sourceId)?.value || 0
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return {
      type: 'formula',
      formula: 'BE-2',
    }
  }
}

export class GenericCombatTalent
  extends Skill<'combatTalent'>
  implements CombatTalent
{
  public skillType = 'talent' as const
  public talentType = 'combat' as const
  private ruleset: Ruleset

  constructor(
    item: DataAccessor<'combatTalent'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character)
    if (ruleset) {
      this.ruleset = ruleset
    } else {
      this.ruleset = getGame().ruleset
    }
  }

  private computeValue(
    identifier: ComputationIdentifier<
      CombatComputationData,
      CombatComputationResult
    >
  ): number {
    return this.ruleset.compute(identifier, {
      character: this.character,
      talent: this,
    }).value
  }

  get attack(): number {
    return this.computeValue(ComputeAttack)
  }

  get parry(): number {
    return this.computeValue(ComputeParry)
  }

  get rangedAttack(): number {
    return this.computeValue(ComputeRangedAttack)
  }

  get attackMod(): number {
    return this.item.system.combat.attack
  }

  get parryMod(): number {
    return this.item.system.combat.parry
  }

  get rangedAttackMod(): number {
    return this.item.system.combat.rangedAttack
  }

  get isUnarmed(): boolean {
    return ['talent-raufen', 'talent-ringen'].includes(this.item.system.sid)
  }

  get effectiveEncumbarance(): EffectiveEncumbarance {
    return this.item.system.effectiveEncumbarance
  }
}
export class GenericLiturgy
  extends RollableSkill<'liturgy'>
  implements Liturgy, Testable<BaseSkill>
{
  public skillType = 'liturgy' as const
  private karmaTalent: Talent | undefined
  protected skillAction: ActionIdentifier<SkillActionData, SkillActionResult> =
    LiturgyAction

  constructor(
    item: DataAccessor<'liturgy'>,
    character: BaseCharacter,
    ruleset?: Ruleset
  ) {
    super(item, character, ruleset)
    this.karmaTalent = character.karmaTalent()
  }

  get degree(): string {
    return this.item.system.degree
  }

  get value(): number {
    if (this.karmaTalent) return this.karmaTalent.value

    return 0
  }

  get testAttributes(): TestAttributes {
    if (this.karmaTalent) return this.karmaTalent.testAttributes

    return ['courage', 'intuition', 'charisma']
  }
}
