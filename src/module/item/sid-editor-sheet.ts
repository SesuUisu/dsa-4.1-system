import { TJSDocument } from '@typhonjs-fvtt/runtime/svelte/store/fvtt/document'
import { SvelteApplication } from '@typhonjs-fvtt/runtime/svelte/application'
import SidEditorShell from '../../ui/sid-editor/SidEditorShell.svelte'
import { getLocalizer } from '../utils.js'

declare module '@typhonjs-fvtt/runtime/svelte/application' {
  interface SvelteApplication extends Application {}
}

const localize = getLocalizer()

export class SidEditorSheet extends SvelteApplication {
  private documentStore = new TJSDocument<Item>(void 0, {
    delete: this.close.bind(this),
  })

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      title: localize('sidEditor'),
      resizable: true,
      width: 500,
      height: 'auto',
      svelte: {
        class: SidEditorShell,
        target: document.body,
        props: function () {
          return {
            localize: (key: string) => localize(key),
            doc: this.documentStore,
          }
        },
      },
    })
  }

  constructor(document: Item) {
    super()
    this.documentStore.set(document)
  }
}
