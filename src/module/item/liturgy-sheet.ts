import {
  LiturgyCastType,
  LiturgyTargetClass,
  LiturgyType,
  RuleBook,
} from '../enums.js'
import { labeledEnumValues } from '../i18n.js'
import { createOpenPDFPageListener } from '../pdf-integration.js'
import { showOptionsDialogFromEnum } from './sheet-helper.js'
import { ItemWithVariantsSheet } from './item-with-variants-sheet.js'
import { liturgyDegrees } from '../model/properties.js'

export class LiturgySheet extends ItemWithVariantsSheet {
  static get variantCompendium() {
    return 'world.liturgyvariants'
  }

  static get variantItemType() {
    return 'liturgyVariant'
  }

  static get variantHTMLSelector() {
    return '.liturgy-variants'
  }

  activateListeners(html) {
    super.activateListeners(html)

    html.find('.target-classes-list .item-create').click(() => {
      showOptionsDialogFromEnum(LiturgyTargetClass, this.item, 'targetClasses')
    })

    html
      .find('.open-ll-page')
      .click(createOpenPDFPageListener(RuleBook.LiberLiturgium))
  }

  async getData() {
    const data = await super.getData()

    data.targetClasses = labeledEnumValues(LiturgyTargetClass).filter(
      (target_class) =>
        data.data.system.targetClasses.includes(target_class.value)
    )

    data.types = labeledEnumValues(LiturgyType)
    data.castTypes = labeledEnumValues(LiturgyCastType)
    data.degrees = liturgyDegrees

    return data
  }
}

ItemWithVariantsSheet.registerSheet('liturgie', LiturgySheet)
