import { genericI18nMap, labeledEnumValues, labeledValues } from '../i18n.js'
import { getGame, getLocalizer } from '../utils.js'

function updateOptions(html, item, attributeName) {
  const options = [...html.find('input')].reduce(
    (options, checkbox) =>
      checkbox.checked ? [...options, checkbox.name] : options,
    []
  )

  item.update({ data: { [attributeName]: options } })
}

export async function showOptionsDialogFromEnum(enumType, item, attributeName) {
  const labeledValues = labeledEnumValues(enumType)

  return showOptionsDialogFromLabeledValues(labeledValues, item, attributeName)
}

export async function showOptionsDialog<T extends string>(
  names: readonly T[],
  item,
  attributeName
) {
  const localize = getLocalizer()

  const labeledNames = labeledValues(genericI18nMap(names), names, localize)

  return showOptionsDialogFromLabeledValues(labeledNames, item, attributeName)
}

async function showOptionsDialogFromLabeledValues(
  labeledValues,
  item,
  attributeName
) {
  const template = 'systems/dsa-41/templates/options-dialog.html'
  const options = labeledValues.map((labeledValue) => ({
    ...labeledValue,
    selected: item.system[attributeName].includes(labeledValue.value),
  }))

  const html = await renderTemplate(template, { options })

  return new Promise((resolve) => {
    const game = getGame()
    new Dialog({
      title: `${game.i18n.localize('DSA.choose')} ${game.i18n.localize(
        'DSA.' + attributeName
      )}`,
      content: html,
      buttons: {
        normal: {
          label: game.i18n.localize('DSA.select'),
          callback: (html) => updateOptions(html, item, attributeName),
        },
      },
      default: 'normal',
      close: () => resolve(false),
    }).render(true)
  })
}
