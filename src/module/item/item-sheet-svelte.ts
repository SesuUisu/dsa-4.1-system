import {
  SvelteApplication,
  SvelteApplicationOptions,
} from '@typhonjs-fvtt/runtime/svelte/application'
import { TJSDocument } from '@typhonjs-fvtt/runtime/svelte/store/fvtt/document'
import { getGame } from '../utils.js'
import { isUniquelyIdentifiable } from '../model/item-data.js'
import { SidEditorSheet } from './sid-editor-sheet.js'
import ItemSheetShell from '../../ui/item-sheet/ItemSheetShell.svelte'

export class SvelteItemSheet extends SvelteApplication {
  #documentStore = new TJSDocument(void 0, { delete: this.close.bind(this) })

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      width: 950,
      resizable: true,
      minimizable: true,
      svelte: {
        class: ItemSheetShell,
        target: document.body,
        props: function () {
          return {
            doc: this.#documentStore,
          }
        },
      },
    })
  }

  public item: Item

  constructor(object: SvelteApplicationOptions) {
    super(object)
    this.item = object as Item

    /**
     * @member {object} document - Adds accessors to SvelteReactive to get / set the document associated with
     *                             Document.
     *
     * @memberof SvelteReactive#
     */
    Object.defineProperty(this.reactive, 'document', {
      get: () => this.#documentStore.get(),
      set: (document) => {
        this.#documentStore.set(document)
      },
    })
    this.reactive.document = object
  }

  _getHeaderButtons() {
    const buttons = super._getHeaderButtons()
    const game = getGame()

    if (isUniquelyIdentifiable(this.item.system) && game.user?.isGM) {
      buttons.unshift({
        class: 'item-tweaks',
        icon: 'fas fa-cog',
        label: getGame().i18n.localize('DSA.tweaks'),
        onclick: (ev) => {
          if (ev) {
            ev.preventDefault()
          }
          new SidEditorSheet(this.item).render(true)
        },
      })
    }

    return buttons
  }
}
