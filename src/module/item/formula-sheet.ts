import { DsaItemSheet } from './item-sheet.js'
import {
  FormulaCastTimeClasses,
  FormulaTargetClasses,
  FormulaRangeClasses,
  FormulaDurationClasses,
  FormulaParameterClassNames,
  FormulaStructureClasses,
  FormulaDurationByType,
  FormulaTargetsByType,
  FormulaWeightClasses,
} from '../model/myranor-magic.js'
import {
  GetAspCosts,
  GetModifiersTotal,
} from '../ruleset/rules/myranor/formula.js'

Handlebars.registerHelper(
  'isStructureCategory',
  (category) => category === 'structure'
)

Handlebars.registerHelper(
  'getFormulaStructureLabelByInstruction',
  (instructions, instructionId, parameterValue) => {
    const instruction = instructions.find(
      (instruction) => instruction._id === instructionId
    )

    return instruction?.system.structures[parameterValue].name
  }
)

Handlebars.registerHelper('getAspCosts', GetAspCosts)
Handlebars.registerHelper('getTotalFormulaModifiers', GetModifiersTotal)

Handlebars.registerHelper(
  'isValidFormulaParameter',
  (instructions, instructionId, parameterClass, parameterValue) => {
    const instruction = instructions.find(
      (instruction) => instruction._id === instructionId
    )

    if (!instruction) {
      return true // show all options in case no instruction could be determined
    }

    let validTargetParameters, validTargetCategoryGroupIds

    function isValidParameter(instructionCategories, categoryByType) {
      validTargetCategoryGroupIds = Object.keys(instructionCategories).map(
        (category) => {
          return instructionCategories[category] ? categoryByType[category] : []
        }
      )
      validTargetParameters = [].concat(...validTargetCategoryGroupIds)
      return validTargetParameters.includes(parameterValue)
    }

    switch (parameterClass) {
      case 'duration':
        return isValidParameter(
          instruction.system.durations,
          FormulaDurationByType
        )
      case 'target':
        return isValidParameter(
          instruction.system.targetCategories,
          FormulaTargetsByType
        )
      case 'structure':
        return instruction.system.structures[parameterValue].active
    }
    return true
  }
)

export class FormulaSheet extends DsaItemSheet {
  activateListeners(html) {
    super.activateListeners(html)
  }

  async getData() {
    const data = await super.getData()

    const systemSources = await game.packs
      .get('dsa-41.myranorsources')
      .getDocuments()
    const customSources = await game.items.find(
      (item) => item.type === 'myranorSource'
    )
    data.sources = []
      .concat(systemSources, customSources)
      .sort((a, b) => (a.name < b.name ? -1 : 1))

    const systemInstructions = await game.packs
      .get('dsa-41.myranorformulainstructions')
      .getDocuments()

    const customInstructions =
      (await game.items.filter((item) => item.type === 'formulaInstruction')) ||
      []
    data.instructions = []
      .concat(systemInstructions, customInstructions)
      .sort((a, b) => (a.name < b.name ? -1 : 1))

    data.weightClasses = FormulaWeightClasses
    data.parameterClasses = FormulaParameterClassNames
    data.parameters = {
      castTime: FormulaCastTimeClasses,
      target: FormulaTargetClasses,
      range: FormulaRangeClasses,
      duration: FormulaDurationClasses,
      structure: FormulaStructureClasses,
    }

    return data
  }
}
